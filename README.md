negocios
========

A Symfony 2.7

Funcionamiento de Cliente, Asistencia, Cuotas y Cobros:

Al tomar una Asistencia la misma utiliza un GymBundle/Service/Cuota con una Query en CuotaRepository que devolvera true or false.
Segun ese resultado se creara o no la Cuota.
Tenemos 2 metodos:
generarCuotaMes
generarCuotaSegunAsistenciasMes (Usa generarCuotaMes)

Generacion de cuotas:
Luego de la inscripción de los clientes, generar las cuotas.

1. Esto lo hacemos yendo a Cuotas en el menu.
2. Luego a la pestaña que dice: Por Mes.
3. Seleccionamos el mes que queremos generar las cuotas por ejemplo Mayo.
4. Debajo nos aparecen botones amarillo presionamos el que dice: Generar Solo Cuotas Mes: 5.
5. Listo nos deberían aparecer las cuotas correspondientes.
6. De esta manera Mes a Mes se pueden ir generando las cuotas.


TECSPRO <contacto@tecspro.com.ar>