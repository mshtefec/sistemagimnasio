<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RutinaCliente
 *
 * @ORM\Table(name="rutina_cliente")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\RutinaClienteRepository")
 */
class RutinaCliente {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Rutina", inversedBy="rutinasClientes")
     * @ORM\JoinColumn(name="rutina_id", referencedColumnName="id")
     */
    private $rutina;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="rutinasClientes")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return RutinaCliente
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set rutina
     *
     * @param \Sistema\GymBundle\Entity\Rutina $rutina
     * @return RutinaCliente
     */
    public function setRutina(\Sistema\GymBundle\Entity\Rutina $rutina = null)
    {
        $rutina->addRutinasCliente($this);
        $this->rutina = $rutina;

        return $this;
    }

    /**
     * Get rutina
     *
     * @return \Sistema\GymBundle\Entity\Rutina 
     */
    public function getRutina()
    {
        return $this->rutina;
    }

    /**
     * Set cliente
     *
     * @param \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return RutinaCliente
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null)
    {
        $cliente->addRutinasCliente($this);
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente 
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
