<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Plan
 *
 * @ORM\Table(name="plan")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\PlanRepository")
 */
class Plan {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\ActividadCobro", mappedBy="plan", cascade={"persist"}, orphanRemoval=true)
     */
    private $actividadCobros;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Actividad", inversedBy="planes")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id", nullable=false)
     */
    private $actividad;

    /**
     * @var string
     *
     * @ORM\Column(name="costo", type="decimal")
     */
    private $costo;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidadDiasSemana", type="integer")
     */
    private $cantidadDiasSemana;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * Constructor
     */
    public function __construct() {
        $this->actividadCobros = new ArrayCollection();
        $this->setActivo(true);
    }

    public function __toString() {
        return 'Actividad: ' . $this->getActividad()->getNombre().' - Plan: '. $this->getNombre() . ' - Dias semanales: ' . $this->getCantidadDiasSemana() . ' - costo: $' . $this->getCosto();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set costo
     *
     * @param  string $costo
     * @return Plan
     */
    public function setCosto($costo) {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return string
     */
    public function getCosto() {
        return $this->costo;
    }

    /**
     * Set cantidadDiasSemana
     *
     * @param  integer $cantidadDiasSemana
     * @return Plan
     */
    public function setCantidadDiasSemana($cantidadDiasSemana) {
        $this->cantidadDiasSemana = $cantidadDiasSemana;

        return $this;
    }

    /**
     * Get cantidadDiasSemana
     *
     * @return integer
     */
    public function getCantidadDiasSemana() {
        return $this->cantidadDiasSemana;
    }

    /**
     * Set activo
     *
     * @param  boolean $activo
     * @return Plan
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Add actividadCobros
     *
     * @param  \Sistema\GymBundle\Entity\ActividadCobro $actividadCobros
     * @return Plan
     */
    public function addActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobros) {
        $actividadCobros->setPlan($this);
        $this->actividadCobros[] = $actividadCobros;

        return $this;
    }

    /**
     * Remove actividadCobros
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobros
     */
    public function removeActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobros) {
        $this->actividadCobros->removeElement($actividadCobros);
    }

    /**
     * Get actividadCobros
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActividadCobros() {
        return $this->actividadCobros;
    }

    /**
     * Set actividad
     *
     * @param  \Sistema\GymBundle\Entity\Actividad $actividad
     * @return Plan
     */
    public function setActividad(\Sistema\GymBundle\Entity\Actividad $actividad = null) {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return \Sistema\GymBundle\Entity\Actividad
     */
    public function getActividad() {
        return $this->actividad;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Plan
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

}
