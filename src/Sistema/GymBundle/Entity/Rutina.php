<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rutina
 *
 * @ORM\Table(name="rutina")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\RutinaRepository")
 */
class Rutina {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidadSemanas", type="integer")
     */
    private $cantidadSemanas;

    /**
     * @ORM\OneToMany(targetEntity="RutinaCliente", mappedBy="rutina")
     * */
    private $rutinasClientes;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="RutinaDias", mappedBy="rutina",cascade={"persist"})
     */
    private $rutinaDias;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="rutinas")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->rutinasClientes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rutinaDias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidadSemanas
     *
     * @param integer $cantidadSemanas
     * @return Rutina
     */
    public function setCantidadSemanas($cantidadSemanas) {
        $this->cantidadSemanas = $cantidadSemanas;

        return $this;
    }

    /**
     * Get cantidadSemanas
     *
     * @return integer 
     */
    public function getCantidadSemanas() {
        return $this->cantidadSemanas;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Rutina
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Add rutinasClientes
     *
     * @param \Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes
     * @return Rutina
     */
    public function addRutinasCliente(\Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes) {
        $this->rutinasClientes[] = $rutinasClientes;

        return $this;
    }

    /**
     * Remove rutinasClientes
     *
     * @param \Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes
     */
    public function removeRutinasCliente(\Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes) {
        $this->rutinasClientes->removeElement($rutinasClientes);
    }

    /**
     * Get rutinasClientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRutinasClientes() {
        return $this->rutinasClientes;
    }

    /**
     * Add rutinaDias
     *
     * @param \Sistema\GymBundle\Entity\RutinaDias $rutinaDias
     * @return Rutina
     */
    public function addRutinaDia(\Sistema\GymBundle\Entity\RutinaDias $rutinaDias) {
        $rutinaDias->setRutina($this);
        $this->rutinaDias[] = $rutinaDias;

        return $this;
    }

    /**
     * Remove rutinaDias
     *
     * @param \Sistema\GymBundle\Entity\RutinaDias $rutinaDias
     */
    public function removeRutinaDia(\Sistema\GymBundle\Entity\RutinaDias $rutinaDias) {
        $this->rutinaDias->removeElement($rutinaDias);
    }

    /**
     * Get rutinaDias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRutinaDias() {
        return $this->rutinaDias;
    }


    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Rutina
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
