<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Asistencia
 *
 * @ORM\Table(name="asistencia")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\AsistenciaRepository")
 */
class Asistencia {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\ActividadCobro", inversedBy="asistencias")
     * @ORM\JoinColumn(name="actividadCobro_id", referencedColumnName="id")
     */
    private $actividadCobro;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Gimnasio", inversedBy="asistencias")
     * @ORM\JoinColumn(name="gimnasio_id", referencedColumnName="id")
     */
    private $gimnasio;

    /**
     * @var integer
     *
     * @ORM\Column(name="anio", type="integer")
     */
    private $anio;

    /**
     * @var integer
     *
     * @ORM\Column(name="mes", type="integer")
     */
    private $mes;

    /**
     * @var integer
     *
     * @ORM\Column(name="dia", type="integer")
     */
    private $dia;

    /**
     * @ORM\Column(name="hora", type="time")
     */
    private $hora;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="asistencias" , cascade={"persist"})
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;


    public function __construct() {
        $fecha = new \DateTime('now');
        $this->anio = $fecha->format('Y');
        $this->mes = $fecha->format('m');
        $this->dia = $fecha->format('d');
        $this->hora = $fecha;
    }

    public function __toString() {
        return $this->getDia() . "/" . $this->getMes() . "/" . $this->getAnio();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return Asistencia
     */
    public function setAnio($anio) {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio() {
        return $this->anio;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     * @return Asistencia
     */
    public function setMes($mes) {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return integer 
     */
    public function getMes() {
        return $this->mes;
    }

    /**
     * Set dia
     *
     * @param integer $dia
     * @return Asistencia
     */
    public function setDia($dia) {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return integer 
     */
    public function getDia() {
        return $this->dia;
    }

    /**
     * Set hora
     *
     * @param \DateTime $hora
     * @return Asistencia
     */
    public function setHora($hora) {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \DateTime 
     */
    public function getHora() {
        return $this->hora;
    }

    /**
     * Set actividadCobro
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobro
     * @return Asistencia
     */
    public function setActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobro = null)
    { 
        $this->actividadCobro = $actividadCobro;

        return $this;
    }

    /**
     * Get actividadCobro
     *
     * @return \Sistema\GymBundle\Entity\ActividadCobro 
     */
    public function getActividadCobro()
    {
        return $this->actividadCobro;
    }

    /**
     * Set gimnasio
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasio
     * @return Asistencia
     */
    public function setGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasio = null) {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get gimnasio
     *
     * @return \Sistema\GymBundle\Entity\Gimnasio 
     */
    public function getGimnasio() {
        return $this->gimnasio;
    }


    /**
     * Set cliente
     *
     * @param \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return Asistencia
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente 
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
