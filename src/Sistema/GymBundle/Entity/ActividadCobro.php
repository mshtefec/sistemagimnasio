<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\GymBundle\Validator\Constraints as GymAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ActividadCobro 
 *
 * @ORM\Table(name="actividad_cobro")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\ActividadCobroRepository")
 * @GymAssert\ContraintActividad()
 * @UniqueEntity(
 *     fields={"plan", "cliente"},
 *     errorPath="plan",
 *     message="El cliente ya se encuentra inscripto en el plan elegido"
 * )
 */
class ActividadCobro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="actividadCobros")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=false)
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Plan", inversedBy="actividadCobros")
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id", nullable=false)
     * @Assert\NotNull()
     */
    private $plan;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Cuota", mappedBy="actividadCobro", cascade={"persist"}, orphanRemoval=true)
     */
    private $cuotas;

    /**
     * @var integer
     *
     * @ORM\Column(name="diaCobro", type="string")
     * @Assert\Range(
     *      min = 1,
     *      max = 31,
     *      minMessage = "Minimo {{ limit }}",
     *      maxMessage = "Maximo {{ limit }}"
     * )
     */
    private $diaCobro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Asistencia", mappedBy="actividadCobro")
     */
    private $asistencias;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Gimnasio", inversedBy="actividadCobros")
     * @ORM\JoinColumn(name="gimnasio_id", referencedColumnName="id")
     */
    private $gimnasio;


    /**
     * Constructor
     */
    public function __construct() {
        $this->cuotas      = new ArrayCollection();
        $this->asistencias = new ArrayCollection();
    }

    public function __toString() {

        return  'Cliente: '     . $this->getCliente() .
                '. Actividad: ' . $this->getPlan()->getActividad()->getNombre() .
                '. Plan: '      . $this->getPlan()->getNombre() .
                '. Costo: $'    . $this->getPlan()->getCosto();
    }

    public function getPlanYActividades(){
          
        return  'Actividad: ' . $this->getPlan()->getActividad()->getNombre() .
                '. Plan: '    . $this->getPlan()->getNombre() .
                '. Costo: $'  . $this->getPlan()->getCosto();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set diaCobro
     *
     * @param  integer        $diaCobro
     * @return ActividadCobro
     */
    public function setDiaCobro($diaCobro) {
        $this->diaCobro = $diaCobro;

        return $this;
    }

    /**
     * Get diaCobro
     *
     * @return integer
     */
    public function getDiaCobro() {
        return $this->diaCobro;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return ActividadCobro
     */
    public function setFecha($fecha)
    {
        $this->fecha    = $fecha;
        $this->diaCobro = $fecha->format('d');

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set cliente
     *
     * @param  \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return ActividadCobro
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * Set plan
     *
     * @param  \Sistema\GymBundle\Entity\Plan $plan
     * @return ActividadCobro
     */
    public function setPlan(\Sistema\GymBundle\Entity\Plan $plan = null) {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \Sistema\GymBundle\Entity\Plan
     */
    public function getPlan() {
        return $this->plan;
    }

    /**
     * Add cuotas
     *
     * @param  \Sistema\GymBundle\Entity\Cuota $cuotas
     * @return ActividadCobro
     */
    public function addCuota(\Sistema\GymBundle\Entity\Cuota $cuotas) {
        $cuotas->setActividadCobros($this);
        $this->cuotas[] = $cuotas;

        return $this;
    }

    /**
     * Remove cuotas
     *
     * @param \Sistema\GymBundle\Entity\Cuota $cuotas
     */
    public function removeCuota(\Sistema\GymBundle\Entity\Cuota $cuotas) {
        $this->cuotas->removeElement($cuotas);
    }

    /**
     * Get cuotas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCuotas() {
        return $this->cuotas;
    }

    /**
     * Add asistencias
     *
     * @param  \Sistema\GymBundle\Entity\Asistencia $asistencias
     * @return ActividadCobro
     */
    public function addAsistencia(\Sistema\GymBundle\Entity\Asistencia $asistencias) {
        $asistencias->setActividadCobro($this);
        $this->asistencias[] = $asistencias;

        return $this;
    }

    /**
     * Remove asistencias
     *
     * @param \Sistema\GymBundle\Entity\Asistencia $asistencias
     */
    public function removeAsistencia(\Sistema\GymBundle\Entity\Asistencia $asistencias) {
        $this->asistencias->removeElement($asistencias);
    }

    /**
     * Get asistencias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsistencias() {
        return $this->asistencias;
    }

    /**
     * Set gimnasio
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasio
     * @return ActividadCobro
     */
    public function setGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasio = null)
    {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get gimnasio
     *
     * @return \Sistema\GymBundle\Entity\Gimnasio 
     */
    public function getGimnasio()
    {
        return $this->gimnasio;
    } 
}