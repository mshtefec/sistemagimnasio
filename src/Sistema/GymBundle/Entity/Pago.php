<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pago
 *
 * @ORM\Table(name="pago")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\PagoRepository")
 */
class Pago {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @ORM\OneToMany(targetEntity="PagoCuota", mappedBy="pago",cascade={"all"})
     */
    private $detalles;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="pagos")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\user", inversedBy="pagos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="monto", type="string", length=255)
     *
     * @Assert\Range(
     *      min = 1,
     *      minMessage = "El pago debe ser mayor a 0.",
     * )
     */
    private $monto;

    /**
     * Constructor
     */
    public function __construct() {
        $this->detalles = new ArrayCollection();
        $this->fecha = new \DateTime();
        $this->monto = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pago
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set monto
     *
     * @param float $monto
     * @return Pago
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float 
     */
    public function getMonto() {

        return $this->monto;
    }

    /**
     * Add detalles
     *
     * @param \Sistema\GymBundle\Entity\PagoDetalle $detalles
     * @return Pago
     */
    public function addDetalle(\Sistema\GymBundle\Entity\PagoCuota $detalles) {
        $detalles->setPago($this);
        $this->detalles[] = $detalles;

        return $this;
    }

    /**
     * Remove detalles
     *
     * @param \Sistema\GymBundle\Entity\PagoDetalle $detalles
     */
    public function removeDetalle(\Sistema\GymBundle\Entity\PagoCuota $detalles) {
        $this->detalles->removeElement($detalles);
    }

    /**
     * Get detalles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetalles() {
        return $this->detalles;
    }

    /**
     * Set cliente
     *
     * @param \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return Pago
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente 
     */
    public function getCliente() {
        return $this->cliente;
    }


    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\user $user
     * @return Pago
     */
    public function setUser(\Sistema\UserBundle\Entity\user $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\user 
     */
    public function getUser()
    {
        return $this->user;
    }
}
