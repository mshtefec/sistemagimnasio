<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Sistema\GymBundle\Validator\Constraints as GymAssert;

/**
 * Cuota
 *
 * @ORM\Table(name="cuota")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\CuotaRepository")
 * @GymAssert\ContraintCuota()
 */
class Cuota {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\ActividadCobro", inversedBy="cuotas")
     * @ORM\JoinColumn(name="actividadCobro_id", referencedColumnName="id")
     */
    private $actividadCobro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="costo", type="decimal")
     */
    private $costo;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="pagada", type="boolean", nullable=true)
     */
    private $pagada;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Gimnasio", inversedBy="cuotas")
     */
    private $gimnasio;

    /**
     * @ORM\OneToMany(targetEntity="PagoCuota", mappedBy="cuota",cascade={"remove"})
     */
    private $pagos;

    /**
     * Constructor
     */
    public function __construct() {

        $this->pagos = new ArrayCollection();
        $this->fecha = new \DateTime();
    }

    public function __toString() {

        return $this->getClienteActividadCobro() . ", " . 
               $this->getFecha()->format("d/m/Y") . ", Saldo: $" . 
               $this->calcularSaldoNegativo()
        ;
    }

    // public function getDetalle() {

    //     return $this->getFecha()->format("d/m/Y") . ", Saldo: $" . 
    //            $this->calcularSaldoNegativo()
    //     ;
    // }    

    private function getClienteActividadCobro() {

        return $this->getActividadcobro()->getCliente();
    }

    public function calcularSaldoNegativo() {
        
        return $this->calcularSaldo() * (-1);
    }
    
    public function calcularSaldo() {

        $suma = $this->sumarPagos();
        $saldo = ($this->costo - $suma);

        return $saldo;
    }

    public function sumarPagos() {

        $suma = 0;

        foreach ($this->pagos as $pago) {
            
            $suma += $pago->getMonto();
        }

        return $suma;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Cuota
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }

    /**
     * Set costo
     *
     * @param string $costo
     * @return Cuota
     */
    public function setCosto($costo) {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return string 
     */
    public function getCosto() {
        return $this->costo;
    }

    /**
     * Get costo
     *
     * @return string 
     */
    public function getCostoSigno() {
        return "$" . $this->costo;
    }

    /**
     * Set actividadCobro
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobro
     * @return Cuota
     */
    public function setActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobro = null) {
        $this->actividadCobro = $actividadCobro;

        return $this;
    }

    /**
     * Get actividadCobro
     *
     * @return \Sistema\GymBundle\Entity\ActividadCobro 
     */
    public function getActividadCobro() {
        return $this->actividadCobro;
    }

    /**
     * Set pagada
     *
     * @param boolean $pagada
     * @return Cuota
     */
    public function setPagada($pagada) {
        $this->pagada = $pagada;

        return $this;
    }

    /**
     * Get pagada
     *
     * @return boolean 
     */
    public function getPagada() {
        return $this->pagada;
    }

    /**
     * Set gimnasio
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasio
     * @return Cuota
     */
    public function setGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasio = null) {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get gimnasio
     *
     * @return \Sistema\GymBundle\Entity\Gimnasio 
     */
    public function getGimnasio() {
        return $this->gimnasio;
    }

    /**
     * Add pagos
     *
     * @param \Sistema\GymBundle\Entity\PagoDetalle $pagos
     * @return Cuota
     */
    public function addPago(\Sistema\GymBundle\Entity\PagoCuota $pagos) {
        $pagos->setCuota($this);
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \Sistema\GymBundle\Entity\PagoDetalle $pagos
     */
    public function removePago(\Sistema\GymBundle\Entity\PagoCuota $pagos) {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos() {
        return $this->pagos;
    }

}
