<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Combinacion
 *
 * @ORM\Table(name="combinacion")
 * @ORM\Entity
 */
class Combinacion {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidadVueltas", type="integer", nullable=true)
     */
    private $cantidadVueltas;

    /**
     * @ORM\OneToMany(targetEntity="RutinaEjercicio", mappedBy="combinacion", cascade={"persist"})
     */
    private $rutinaEjercicios;

    /**
     * @ORM\ManyToOne(targetEntity="RutinaDias", inversedBy="combinaciones")
     * @ORM\JoinColumn(name="rutinadias_id", referencedColumnName="id")
     */
    private $rutinaDia;

    /**
     * Constructor
     */
    public function __construct() {
        $this->rutinaEjercicios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setCantidadVueltas(1);
    }

    public function __toString() {
        $AllRutinaEjercicio = '';
        foreach ($this->getRutinaEjercicios() as $rutEj) {
            $AllRutinaEjercicio = $AllRutinaEjercicio . ' - ' . $rutEj;
        }
        $AllRutinaEjercicio = $AllRutinaEjercicio . '  circuito: ' . $this->getCantidadVueltas();
        return $AllRutinaEjercicio;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidadVueltas
     *
     * @param integer $cantidadVueltas
     * @return Combinacion
     */
    public function setCantidadVueltas($cantidadVueltas) {
        $this->cantidadVueltas = $cantidadVueltas;

        return $this;
    }

    /**
     * Get cantidadVueltas
     *
     * @return integer 
     */
    public function getCantidadVueltas() {
        return $this->cantidadVueltas;
    }

    /**
     * Set rutinaEjercicios
     *
     * @param \Sistema\GymBundle\Entity\RutinaEjercicio $rutinaEjercicios
     * @return Combinacion
     */
    public function setRutinaEjercicios(\Sistema\GymBundle\Entity\RutinaEjercicio $rutinaEjercicios) {
        $rutinaEjercicios->setCombinacion($this);
        $this->rutinaEjercicios[] = $rutinaEjercicios;

        return $this;
    }

    /**
     * Add rutinaEjercicios
     *
     * @param \Sistema\GymBundle\Entity\RutinaEjercicio $rutinaEjercicios
     * @return Combinacion
     */
    public function addRutinaEjercicio(\Sistema\GymBundle\Entity\RutinaEjercicio $rutinaEjercicios) {
        $rutinaEjercicios->setCombinacion($this);
        $this->rutinaEjercicios[] = $rutinaEjercicios;

        return $this;
    }

    /**
     * Remove rutinaEjercicios
     *
     * @param \Sistema\GymBundle\Entity\RutinaEjercicio $rutinaEjercicios
     */
    public function removeRutinaEjercicio(\Sistema\GymBundle\Entity\RutinaEjercicio $rutinaEjercicios) {
        $this->rutinaEjercicios->removeElement($rutinaEjercicios);
    }

    /**
     * Get rutinaEjercicios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRutinaEjercicios() {
        return $this->rutinaEjercicios;
    }

    /**
     * Set rutinaDia
     *
     * @param \Sistema\GymBundle\Entity\RutinaDias $rutinaDia
     * @return Combinacion
     */
    public function setRutinaDia(\Sistema\GymBundle\Entity\RutinaDias $rutinaDia = null) {
        $this->rutinaDia = $rutinaDia;

        return $this;
    }

    /**
     * Get rutinaDia
     *
     * @return \Sistema\GymBundle\Entity\RutinaDias 
     */
    public function getRutinaDia() {
        return $this->rutinaDia;
    }

}
