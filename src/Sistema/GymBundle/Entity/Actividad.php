<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Actividad
 *
 * @ORM\Table(name="actividad")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\ActividadRepository")
 */
class Actividad {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Horario", mappedBy="actividad", cascade={"persist", "remove"}, orphanRemoval=true)
     * @Assert\NotNull()
     */
    private $horarios;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Plan", mappedBy="actividad", cascade={"persist", "remove"}, orphanRemoval=true)
     * @Assert\NotNull()
     */
    private $planes;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Gimnasio", inversedBy="actividades")
     * @ORM\JoinColumn(name="actividades_gimnasios", referencedColumnName="id", nullable=false)
     */
    private $gimnasio;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->horarios  = new ArrayCollection();
        $this->planes    = new ArrayCollection();
        $this->setActivo(true);
    }

    function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param  string    $nombre
     * @return Actividad
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set activo
     *
     * @param  boolean   $activo
     * @return Actividad
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Add horarios
     *
     * @param  \Sistema\GymBundle\Entity\Horario $horarios
     * @return Actividad
     */
    public function addHorario(\Sistema\GymBundle\Entity\Horario $horarios) {
        $horarios->setActividad($this);
        $this->horarios[] = $horarios;

        return $this;
    }

    /**
     * Remove horarios
     *
     * @param \Sistema\GymBundle\Entity\Horario $horarios
     */
    public function removeHorario(\Sistema\GymBundle\Entity\Horario $horarios) {
        $this->horarios->removeElement($horarios);
    }

    /**
     * Get horarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHorarios() {
        return $this->horarios;
    }

    /**
     * Add planes
     *
     * @param  \Sistema\GymBundle\Entity\Plan $planes
     * @return Actividad
     */
    public function addPlane(\Sistema\GymBundle\Entity\Plan $planes) {
        $planes->setActividad($this);
        $this->planes[] = $planes;

        return $this;
    }

    /**
     * Remove planes
     *
     * @param \Sistema\GymBundle\Entity\Plan $planes
     */
    public function removePlane(\Sistema\GymBundle\Entity\Plan $planes) {
        $this->planes->removeElement($planes);
    }

    /**
     * Get planes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanes() {
        return $this->planes;
    }

    /**
     * Set gimnasio
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasio
     * @return Actividad
     */
    public function setGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasio = null)
    {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get gimnasio
     *
     * @return \Sistema\GymBundle\Entity\Gimnasio 
     */
    public function getGimnasio()
    {
        return $this->gimnasio;
    }
}
