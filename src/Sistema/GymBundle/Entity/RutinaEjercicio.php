<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * rutinaEjercicio
 *
 * @ORM\Table(name="rutina_ejercicio")
 * @ORM\Entity()
 */
class RutinaEjercicio {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Ejercicio")
     * @ORM\JoinColumn(name="ejercicio_id", referencedColumnName="id")
     */
    private $ejercicio;

    /**
     * @var string
     *
     * @ORM\Column(name="serie", type="string", length=255)
     */
    private $serie;

    /**
     * @ORM\ManyToOne(targetEntity="Combinacion", inversedBy="rutinaEjercicios")
     * @ORM\JoinColumn(name="combinacion_id", referencedColumnName="id")
     */
    private $combinacion;


    public function __toString() {
        return $this->getEjercicio() . ' series: ' . $this->getSerie();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serie
     *
     * @param string $serie
     * @return RutinaEjercicio
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return string 
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set ejercicio
     *
     * @param \Sistema\GymBundle\Entity\Ejercicio $ejercicio
     * @return RutinaEjercicio
     */
    public function setEjercicio(\Sistema\GymBundle\Entity\Ejercicio $ejercicio = null)
    {
        $this->ejercicio = $ejercicio;

        return $this;
    }

    /**
     * Get ejercicio
     *
     * @return \Sistema\GymBundle\Entity\Ejercicio 
     */
    public function getEjercicio()
    {
        return $this->ejercicio;
    }

    /**
     * Set combinacion
     *
     * @param \Sistema\GymBundle\Entity\Combinacion $combinacion
     * @return RutinaEjercicio
     */
    public function setCombinacion(\Sistema\GymBundle\Entity\Combinacion $combinacion = null)
    {
        $this->combinacion = $combinacion;

        return $this;
    }

    /**
     * Get combinacion
     *
     * @return \Sistema\GymBundle\Entity\Combinacion 
     */
    public function getCombinacion()
    {
        return $this->combinacion;
    }
}
