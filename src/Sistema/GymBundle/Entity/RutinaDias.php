<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RutinaDias
 *
 * @ORM\Table(name="rutina_dias")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\RutinaDiasRepository")
 */
class RutinaDias {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dia", type="string", length=255)
     */
    private $dia;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Combinacion", mappedBy="rutinaDia", cascade={"persist"})
     */
    private $combinaciones;

    /**
     * @ORM\ManyToOne(targetEntity="Rutina", inversedBy="rutinaDias")
     * @ORM\JoinColumn(name="rutina_id", referencedColumnName="id")
     */
    private $rutina;

    /**
     * Constructor
     */
    public function __construct() {
        $this->combinaciones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dia
     *
     * @param string $dia
     * @return RutinaDias
     */
    public function setDia($dia) {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia
     *
     * @return string 
     */
    public function getDia() {
        return $this->dia;
    }

    /**
     * Add combinaciones
     *
     * @param \Sistema\GymBundle\Entity\Combinacion $combinaciones
     * @return RutinaDias
     */
    public function addCombinacion(\Sistema\GymBundle\Entity\Combinacion $combinaciones) {
        $combinaciones->setRutinaDia($this);
        $this->combinaciones[] = $combinaciones;

        return $this;
    }

    /**
     * Remove combinaciones
     *
     * @param \Sistema\GymBundle\Entity\Combinacion $combinaciones
     */
    public function removeCombinacion(\Sistema\GymBundle\Entity\Combinacion $combinaciones) {
        $this->combinaciones->removeElement($combinaciones);
    }

    /**
     * Get combinaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCombinacion() {
        return $this->combinaciones;
    }

    /**
     * Set rutina
     *
     * @param \Sistema\GymBundle\Entity\Rutina $rutina
     * @return RutinaDias
     */
    public function setRutina(\Sistema\GymBundle\Entity\Rutina $rutina = null) {
        $this->rutina = $rutina;

        return $this;
    }

    /**
     * Get rutina
     *
     * @return \Sistema\GymBundle\Entity\Rutina 
     */
    public function getRutina() {
        return $this->rutina;
    }

}
