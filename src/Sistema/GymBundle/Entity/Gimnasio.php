<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Gimnasio
 *
 * @ORM\Table(name="gimnasio")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\GimnasioRepository")
 */
class Gimnasio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Actividad", mappedBy="gimnasio", cascade={"persist", "remove"}, orphanRemoval=true)
     * */
    private $actividades;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="gimnasios")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\RRHHBundle\Entity\Cliente", mappedBy="gimnasio", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $clientes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\ActividadCobro", mappedBy="gimnasio")
     */
    private $actividadCobros;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Cuota", mappedBy="gimnasio")
     */
    private $cuotas;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Asistencia", mappedBy="gimnasio")
     */
    private $asistencias;


    /**
     * Constructor
     */
    public function __construct() {
        
        $this->actividades     = new ArrayCollection();
        $this->cuotas          = new ArrayCollection();
        $this->clientes        = new ArrayCollection();
        $this->actividadCobros = new ArrayCollection();
        $this->asistencias     = new ArrayCollection();
        $this->setActivo(true);
    }

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Gimnasio
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Gimnasio
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Add actividades
     *
     * @param \Sistema\GymBundle\Entity\Actividad $actividades
     * @return Gimnasio
     */
    public function addActividade(\Sistema\GymBundle\Entity\Actividad $actividades) {
        $actividades->setGimnasio($this);
        $this->actividades[] = $actividades;

        return $this;
    }

    /**
     * Remove actividades
     *
     * @param \Sistema\GymBundle\Entity\Actividad $actividades
     */
    public function removeActividade(\Sistema\GymBundle\Entity\Actividad $actividades) {
        $this->actividades->removeElement($actividades);
    }

    /**
     * Get actividades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActividades() {
        return $this->actividades;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Gimnasio
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Add clientes
     *
     * @param \Sistema\RRHHBundle\Entity\Cliente $clientes
     * @return Gimnasio
     */
    public function addCliente(\Sistema\RRHHBundle\Entity\Cliente $clientes) {
        $this->clientes[] = $clientes;

        return $this;
    }

    /**
     * Remove clientes
     *
     * @param \Sistema\RRHHBundle\Entity\Cliente $clientes
     */
    public function removeCliente(\Sistema\RRHHBundle\Entity\Cliente $clientes) {
        $this->clientes->removeElement($clientes);
    }

    /**
     * Get clientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientes() {
        return $this->clientes;
    }


    /**
     * Add actividadCobros
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobros
     * @return Gimnasio
     */
    public function addActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobros)
    {
        $this->actividadCobros[] = $actividadCobros;

        return $this;
    }

    /**
     * Remove actividadCobros
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobros
     */
    public function removeActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobros)
    {
        $this->actividadCobros->removeElement($actividadCobros);
    }

    /**
     * Get actividadCobros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActividadCobros()
    {
        return $this->actividadCobros;
    }

    /**
     * Add cuotas
     *
     * @param \Sistema\GymBundle\Entity\Cuota $cuotas
     * @return Gimnasio
     */
    public function addCuota(\Sistema\GymBundle\Entity\Cuota $cuotas)
    {
        $this->cuotas[] = $cuotas;

        return $this;
    }

    /**
     * Remove cuotas
     *
     * @param \Sistema\GymBundle\Entity\Cuota $cuotas
     */
    public function removeCuota(\Sistema\GymBundle\Entity\Cuota $cuotas)
    {
        $this->cuotas->removeElement($cuotas);
    }

    /**
     * Get cuotas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCuotas()
    {
        return $this->cuotas;
    }

    /**
     * Add asistencias
     *
     * @param \Sistema\GymBundle\Entity\Asistencia $asistencias
     * @return Gimnasio
     */
    public function addAsistencia(\Sistema\GymBundle\Entity\Asistencia $asistencias)
    {
        $this->asistencias[] = $asistencias;

        return $this;
    }

    /**
     * Remove asistencias
     *
     * @param \Sistema\GymBundle\Entity\Asistencia $asistencias
     */
    public function removeAsistencia(\Sistema\GymBundle\Entity\Asistencia $asistencias)
    {
        $this->asistencias->removeElement($asistencias);
    }

    /**
     * Get asistencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsistencias()
    {
        return $this->asistencias;
    }
}
