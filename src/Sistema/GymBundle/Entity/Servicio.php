<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Servicio
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\ServicioRepository")
 */
class Servicio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="monto", type="float")
     */
    private $monto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado;

    /**
     *
     *@ORM\OneToMany(targetEntity="Sistema\UserBundle\Entity\User", mappedBy="servicio", cascade={"persist", "remove"})
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct() {

        $this->users = new ArrayCollection();
    }

    public function __toString(){
        return $this->descripcion;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Servicio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set monto
     *
     * @param float $monto
     * @return Servicio
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float 
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Servicio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add users
     *
     * @param \Sistema\UserBundle\Entity\User $users
     * @return Servicio
     */
    public function addUser(\Sistema\UserBundle\Entity\User $users)
    {
        $users->setServicio($this);
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Sistema\UserBundle\Entity\User $users
     */
    public function removeUser(\Sistema\UserBundle\Entity\User $users)
    {
        $users->setServicio(NULL);
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
