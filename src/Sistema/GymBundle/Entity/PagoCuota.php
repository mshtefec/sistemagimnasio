<?php

namespace Sistema\GymBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PagoDetalle
 *
 * @ORM\Table("pago_cuota")
 * @ORM\Entity(repositoryClass="Sistema\GymBundle\Entity\PagoCuotaRepository")
 */
class PagoCuota {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cuota", inversedBy="pagos", cascade={"persist"})
     * @ORM\JoinColumn(name="cuota_id", referencedColumnName="id")
     */
    private $cuota;

    /**
     * @ORM\ManyToOne(targetEntity="Pago", inversedBy="detalles",cascade={"persist"})
     * @ORM\JoinColumn(name="pago_id", referencedColumnName="id")
     */
    private $pago;

    /**
     * @var string
     *
     * @ORM\Column(name="monto", type="string", length=255)
     * @Assert\GreaterThan(0)
     * @Assert\NotNull()
     */
    private $monto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function __toString() {

        return "Cliente: "             . $this->cuota->getActividadcobro()->getCliente() . 
               ". [Saldo: $"           . $this->cuota->calcularSaldoNegativo() . 
               ", Pagado $"            . $this->monto . 
               "]. Por la actividad: " . $this->cuota->getActividadCobro()->getPlan()->getActividad()
        ;
    }

    /**
     * @Assert\True(message = "El monto tiene que ser menor o igual al saldo de la cuota")
     */
    public function isMontoValido() {
        $res = false;
        if ($this->monto <= $this->cuota->getCosto()) {
            $res = true;
        }

        return $res;
    }

    /**
     * Set monto
     *
     * @param string $monto
     * @return PagoDetalle
     */
    public function setMonto($monto) {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return string 
     */
    public function getMonto() {
        return $this->monto;
    }

    /**
     * Set cuota
     *
     * @param \Sistema\GymBundle\Entity\Cuota $cuota
     * @return PagoDetalle
     */
    public function setCuota(\Sistema\GymBundle\Entity\Cuota $cuota = null) {
        $this->cuota = $cuota;

        return $this;
    }

    /**
     * Get cuota
     *
     * @return \Sistema\GymBundle\Entity\Cuota 
     */
    public function getCuota() {
        return $this->cuota;
    }

    /**
     * Set pago
     *
     * @param \Sistema\GymBundle\Entity\Pago $pago
     * @return PagoDetalle
     */
    public function setPago(\Sistema\GymBundle\Entity\Pago $pago = null) {
        $this->pago = $pago;

        return $this;
    }

    /**
     * Get pago
     *
     * @return \Sistema\GymBundle\Entity\Pago 
     */
    public function getPago() {
        return $this->pago;
    }

}
