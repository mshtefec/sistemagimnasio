// Get the ul that holds the collection
var $collectionCombinacion;
// setup an "add a tag" link
var $addCombinacionsLink = $('<hr><a href="javascript:;" class="add_combinacion_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newCombinacionLinkLi = $('<div></div>');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionCombinacion = $('.combinacion');
    // add the "add a tag" anchor and li to the tags ul

    $collectionCombinacion.append($newCombinacionLinkLi);
    $collectionCombinacion.append($addCombinacionsLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionCombinacion.data('index', $collectionCombinacion.find(':input').length);
    $("#nuevoCombinacion").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionCombinacion, $newCombinacionLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newCombinacionLinkLi).offset().top
        }, 1000);
    });
    $addCombinacionsLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionCombinacion, $newCombinacionLinkLi);
    });
    $collectionCombinacion.delegate('.delete_combinacion', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});