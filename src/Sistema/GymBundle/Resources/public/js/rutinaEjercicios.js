// Get the ul that holds the collection
var $collectionRutinaEjercicio;
// setup an "add a tag" link
var $addRutinaEjercicioLink = $('<hr><a href="javascript:;" class="add_rutinaEjercicios_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newRutinaEjercicioLinkLi = $('<div></div>');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionRutinaEjercicio = $('.rutinaEjercicio');
    // add the "add a tag" anchor and li to the tags ul

    $collectionRutinaEjercicio.append($newRutinaEjercicioLinkLi);
    $collectionRutinaEjercicio.append($addRutinaEjercicioLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionRutinaEjercicio.data('index', $collectionRutinaEjercicio.find(':input').length);
    $("#nuevoRutinaEjercicio").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionRutinaEjercicio, $newRutinaEjercicioLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newRutinaEjercicioLinkLi).offset().top
        }, 1000);
    });
    $addRutinaEjercicioLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionRutinaEjercicio, $newRutinaEjercicioLinkLi);
    });
    $collectionRutinaEjercicio.delegate('.delete_rutinaEjercicio', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});