// Get the ul that holds the collection
var $collectionRutinaDias;
// setup an "add a tag" link
var $addRutinaDiasLink = $('<hr><a href="javascript:;" class="add_rutinaDias_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newRutinaDiasLinkLi = $('<div></div>');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionRutinaDias = $('.rutinaDias');
    // add the "add a tag" anchor and li to the tags ul

    $collectionRutinaDias.append($newRutinaDiasLinkLi);
    $collectionRutinaDias.append($addRutinaDiasLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionRutinaDias.data('index', $collectionRutinaDias.find(':input').length);
    $("#nuevoRutinaDias").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionRutinaDias, $newRutinaDiasLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newRutinaDiasLinkLi).offset().top
        }, 1000);
    });
    $addRutinaDiasLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionRutinaDias, $newRutinaDiasLinkLi);
    });
    $collectionRutinaDias.delegate('.delete_rutinaDias', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});