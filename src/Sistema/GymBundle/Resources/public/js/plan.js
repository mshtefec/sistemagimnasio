// Get the ul that holds the collection
var $collectionPlan;
// setup an "add a tag" link
var $addPlanLink = $('<hr><a href="javascript:;" class="add_planes_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newPlanLinkLi = $('<div></div>');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionPlan = $('.plan');
    // add the "add a tag" anchor and li to the tags ul

    $collectionPlan.append($newPlanLinkLi);
    $collectionPlan.append($addPlanLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionPlan.data('index', $collectionPlan.find(':input').length);
    $("#nuevoPlan").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionPlan, $newPlanLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newPlanLinkLi).offset().top
        }, 1000);
    });
    $addPlanLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionPlan, $newPlanLinkLi);
    });
    $collectionPlan.delegate('.delete_plan', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});