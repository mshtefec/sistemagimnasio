function metodosHandleBar() {
    Handlebars.registerHelper('each', function (context, options) {
        var ret = "";
        for (var i = 0, j = context.length; i < j; i++) {

            ret = ret + options.fn(context[i]);
        }
        return ret;
    });
    Handlebars.registerHelper('ifCond', function (v1, v2, options) {

        if (v1 === v2) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('if', function (conditional, options) {

        if (conditional > 0) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

}


