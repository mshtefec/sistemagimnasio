// Get the ul that holds the collection
var $collectionPagoDetalle;
// setup an "add a tag" link
var $addPagoDetalleLink = $('<hr><a href="javascript:;" class="add_PagoDetalle_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newPagoDetalleLinkLi = $('<div></div>');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionPagoDetalle = $('.pagoDetalle');
    // add the "add a tag" anchor and li to the tags ul

    $collectionPagoDetalle.append($newPagoDetalleLinkLi);
    //$collectionPagoDetalle.append($addPagoDetalleLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionPagoDetalle.data('index', $collectionPagoDetalle.find(':input').length);
    $("#nuevoPagoDetalle").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionPagoDetalle, $newPagoDetalleLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newPagoDetalleLinkLi).offset().top
        }, 1000);
    });
    $addPagoDetalleLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionPagoDetalle, $newPagoDetalleLinkLi);
    });
    $collectionPagoDetalle.delegate('.delete_pagoDetalle', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});