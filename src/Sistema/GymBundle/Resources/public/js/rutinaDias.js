// Get the ul that holds the collection of escuelas
var collectioncampania = jQuery('.rutinaDias');
var index;

jQuery(document).ready(function() {

    collectioncampania.data('index', collectioncampania.find(':input').length);

    jQuery('.rutinaDias').delegate('.btnRemoverutinaDias', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        var li = jQuery(this).closest('.rowremove').attr("li");
        jQuery('#' + li).remove();
        removeForm(jQuery(this).closest('.rowremove'));
    });

    jQuery('.ribon_dom').delegate('.add_rutinaDias_link', 'click', function(e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        index = addForm(collectioncampania, jQuery('.rutinaDias'));
        if (index === 0) {
            addUl(index, 'Seleccione Dia');
            jQuery('#rutinaDias-li-' + index).addClass('active');
            jQuery('#rutinaDias_' + index).addClass('active');
        } else {
            addUl(index, 'Seleccione Rutina Dias');
        }

    });

});