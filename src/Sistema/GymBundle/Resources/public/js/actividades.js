// Get the ul that holds the collection
var $collectionActividades;
// setup an "add a tag" link
var $addActividadLink = $('<hr><a href="#" class="add_actividades_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newActividadLinkLi = $('<div></div>');

jQuery(document).ready(function () {

    $collectionActividades = null;

    // Get the ul that holds the collection of tags
    $collectionActividades = $('.actividad');
    // add the "add a tag" anchor and li to the tags ul

     $collectionActividades.append($newActividadLinkLi);
    // $collectionActividades.append($addActividadLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionActividades.data('index', $collectionActividades.find(':input').length);
    $("#nuevoActividad").on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionActividades, $newActividadLinkLi);

        // $('html, body').stop().animate({
        //     scrollTop: $($newActividadLinkLi).offset().top
        // }, 1000);
    });
    $addActividadLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionActividades, $newActividadLinkLi);
    });
    $collectionActividades.delegate('.delete_actividad', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        removeForm(jQuery(this).closest('.rowremove'));
    });

    $("#nuevoActividad").click();
});