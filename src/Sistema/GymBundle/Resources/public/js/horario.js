// Get the ul that holds the collection
var $collectionHorario;
// setup an "add a tag" link
var $addHorarioLink = $('<hr><a href="javascript:;" class="add_horarios_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newHorarioLinkLi = $('<div></div>');

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionHorario = $('.horario');
    // add the "add a tag" anchor and li to the tags ul

    $collectionHorario.append($newHorarioLinkLi);
    $collectionHorario.append($addHorarioLink);


    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHorario.data('index', $collectionHorario.find(':input').length);
    $("#nuevoHorario").on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionHorario, $newHorarioLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newHorarioLinkLi).offset().top
        }, 1000);
    });
    $addHorarioLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionHorario, $newHorarioLinkLi);
    });
    $collectionHorario.delegate('.delete_horario', 'click', function(e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        removeForm(jQuery(this).closest('.rowremove'));
    });
});