<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * PlanType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class PlanActividadType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('costo')
                ->add('cantidadDiasSemana')
                ->add('activo', null, array(
                    'label' => '',
                    'required' => false,
                ))
                ->add('nombre')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Plan'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_plan';
    }

}
