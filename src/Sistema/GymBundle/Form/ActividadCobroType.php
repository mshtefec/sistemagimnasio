<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * ActividadCobroType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class ActividadCobroType extends AbstractType {

    private $is_super_admin;
    private $gym_actual;

    public function __construct($is_super_admin, $gym_actual) {

        $this->is_super_admin = $is_super_admin;
        $this->gym_actual = $gym_actual;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $isNew = null;

        if (isset($options['data'])) {
            $isNew = $options['data']->getId();

            $builder
                    ->add('cliente', 'select2', array(
                        'required' => false,
                        'class' => 'SistemaRRHHBundle:Cliente',
                        'url' => 'Cliente_autocomplete_cliente',
                        'configs' => array(
                            'multiple' => false, //required true or false
                            'width' => 'off',
                        ),
                        'attr' => array(
                            'class' => "col-lg-6 col-md-6 col-sm-6 col-xs-6",
                        ),
                        'label_attr' => array(
                            'class' => 'col-sm-3 control-label'
                        )
                    ))
            ;
        }

        $builder
                ->add('plan', 'entity', array(
                    'class' => 'SistemaGymBundle:Plan',
                    'query_builder' => function (EntityRepository $er) {
                        if ($this->is_super_admin) {
                            return $er
                                    ->createQueryBuilder('p')
                                    ->where('p.activo = true')
                                    ->orderBy('p.id', 'ASC')
                            ;
                        } else {
                            return $er
                                    ->createQueryBuilder('p')
                                    ->join('p.actividad', 'act')
                                    ->join('act.gimnasio', 'gym')
                                    ->where('gym = :id_gym')
                                    ->andWhere('p.activo = true')
                                    ->setParameter('id_gym', $this->gym_actual)
                                    ->orderBy('p.id', 'ASC')
                            ;
                        }
                    },
                ))
        ;
        if (!is_null($isNew)) {
            $builder
                    ->add('diaCobro', 'integer', array(
                        'required' => true,
                        'label' => 'Dia de cobro',
                        'label_attr' => array(
                            'class' => 'col-lg-3 control-label',
                        ),
                        'attr' => array(
                            'class' => 'col-lg-3',
                            'min' => 1,
                            'max' => 31,
                        ),
                    ))
                    ->add('fecha', 'bootstrapdatetime', array(
                        'required' => true,
                        'label' => 'Fecha de inscripción:',
                        'read_only' => true,
                        'label_attr' => array(
                            'class' => 'col-sm-3 control-label',
                        ),
                        'attr' => array(
                            'class' => 'form-control',
                        ),
                        'widget_type' => 'date',
                    ))
            ;
        } elseif (isset($options['data'])) {
            $builder
                    ->add('fecha', 'bootstrapdatetime', array(
                        'required' => true,
                        'data' => new \DateTime('Today'),
                        'label' => 'Fecha de inscripción:',
                        'read_only' => true,
                        'label_attr' => array(
                            'class' => 'col-sm-3 control-label',
                        ),
                        'attr' => array(
                            'class' => 'form-control',
                        ),
                        'widget_type' => 'date',
                    ))
            ;
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\ActividadCobro'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'historialCobro';
    }

}
