<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\GymBundle\Form\RutinaEjercicioType;

/**
 * CombinacionType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class CombinacionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cantidadVueltas', null, array(
                    'label_attr' => array(
                        'class' => 'control-label',
                    ),
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
                ->add('rutinaEjercicios', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new RutinaEjercicioType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                ))
        /* ->add('rutinaDias', 'select2', array(
          'class' => 'Sistema\GymBundle\Entity\RutinaDias',
          'url'   => 'Combinacion_autocomplete_rutinaDias',
          'configs' => array(
          'multiple' => false,//required true or false
          'width'    => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Combinacion'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_combinacion';
    }

}
