<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * EjercicioType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class EjercicioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', null, array(
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label',
                    ),
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
                ->add('activo', null, array(
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label',
                    ),
                    'required' => false
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Ejercicio'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_ejercicio';
    }

}
