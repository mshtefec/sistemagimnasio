<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * PlanType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class PlanType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('costo')
                ->add('cantidadDiasSemana')
                ->add('activo')
                /* ->add('actividadCobros', 'select2', array(
                  'class' => 'Sistema\GymBundle\Entity\ActividadCobro',
                  'url'   => 'Plan_autocomplete_actividadCobros',
                  'configs' => array(
                  'multiple' => true,//required true or false
                  'width'    => 'off',
                  ),
                  'attr' => array(
                  'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                  )
                  )) */
                ->add('actividad', 'select2', array(
                    'class' => 'Sistema\GymBundle\Entity\Actividad',
                    'url' => 'Plan_autocomplete_actividad',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Plan'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_plan';
    }

}
