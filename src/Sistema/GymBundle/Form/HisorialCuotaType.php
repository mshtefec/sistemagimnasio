<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ActividadCobroType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class HistorialCuotaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('cliente', 'select2', array(
                    'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                    'url' => 'ActividadCobro_autocomplete_cliente',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                        'metodo' => 'buscarCliente',
                    ),
                    'attr' => array(
                        'class' => "col-lg-6 col-md-6 col-sm-6 col-xs-6",
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label'
            )))
                
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\ActividadCobro'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_actividadcobro';
    }

}
