<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\GymBundle\Form\CombinacionType;

/**
 * RutinaDiasType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class RutinaDiasType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dia', 'choice', array(
                    'choices' => array(
                        'Lunes' => 'Lunes', 
                        'Martes' => 'Martes',
                        'Miercoles' => 'Miercoles',
                        'Jueves' => 'Jueves',
                        'Viernes' => 'Viernes',
                        'Sabado' => 'Sabado',
                        'Domingo' => 'Domingo',
                        ),
                    'required' => true,
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label',
                    ),
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
                ->add('combinacion', 'select2', array(
                    'class' => 'Sistema\GymBundle\Entity\Combinacion',
                    'url' => 'RutinaDias_autocomplete_combinacion',
                    'configs' => array(
                        'multiple' => true, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
            )))
        /* ->add('combinacion', 'collection', array(
          'label_attr' => array(
          'class' => 'col-lg-2 col-md-2 col-sm-2',
          ),
          'attr' => array(
          'class' => 'col-lg-4 col-md-4 col-sm-4',
          ),
          'label' => false,
          'type' => new CombinacionType(),
          'allow_add' => true,
          'allow_delete' => true,
          'required' => true,
          'by_reference' => false,
          'prototype_name' => '__nameembed__'
          )) */
        /* ->add('rutina', 'select2', array(
          'class' => 'Sistema\GymBundle\Entity\Rutina',
          'url'   => 'RutinaDias_autocomplete_rutina',
          'configs' => array(
          'multiple' => false,//required true or false
          'width'    => 'off',
          ),
          'attr' => array(
          'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
          )
          )) */
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\RutinaDias'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_rutinadias';
    }

}
