<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PagoCuotaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                /*->add('monto', null, array(
                    'required' => true
                ))*/
                ->add('cuota', 'select2', array(
                    'label' => false,
                    'class' => 'Sistema\GymBundle\Entity\Cuota',
                    'url' => 'Pago_autocomplete_cuotas',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12",
                        'col' => "col-lg-8 col-md-8",
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\PagoCuota'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_pagocuota';
    }

}
