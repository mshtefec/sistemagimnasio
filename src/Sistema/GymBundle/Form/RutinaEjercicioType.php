<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * RutinaEjercicioType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class RutinaEjercicioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serie',null, array(
                'attr' => array(
                    'class' => "form-control",
                )
            ))
            ->add('ejercicio', 'select2', array(
                'class' => 'Sistema\GymBundle\Entity\Ejercicio',
                'url'   => 'RutinaEjercicio_autocomplete_ejercicio',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
            /*->add('combinacion', 'select2', array(
                'class' => 'Sistema\GymBundle\Entity\Combinacion',
                'url'   => 'RutinaEjercicio_autocomplete_combinacion',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\RutinaEjercicio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_gymbundle_rutinaejercicio';
    }
}
