<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\GymBundle\Form\HorarioType;
use Sistema\GymBundle\Form\PlanActividadType;

/**
 * ActividadType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class ActividadType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('nombre', null, array(
                'attr' => array(
                    'col' => 'col-lg-6',
                ),
            ))
            ->add('activo', null, array(
                'attr' => array(
                    'col' => 'col-lg-6',
                ),
            ))
            ->add('horarios', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                'label' => false,
                'type' => new HorarioType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('planes', 'collection', array(
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4 col-sm-4',
                ),
                'label' => false,
                'type' => new PlanActividadType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Actividad',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_actividad';
    }
}