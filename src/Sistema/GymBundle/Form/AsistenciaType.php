<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * AsistenciaType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class AsistenciaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('anio', 'hidden')
                ->add('mes', 'hidden')
                ->add('dia', 'hidden')
                ->add('hora', 'hidden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Asistencia'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_asistencia';
    }

}
