<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CuotaType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class CuotaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('actividadCobro', 'select2', array(
                    'label' => 'Cliente - Actividad - Plan',
                    'class' => 'Sistema\GymBundle\Entity\ActividadCobro',
                    'url' => 'Cuota_autocomplete_actividad_cliente',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 txtActividadCobro",
                        'col' => "col-lg-12 col-md-12",
                    )
                ))
                // ->add('fecha', 'bootstrapdatetime', array(
                //     'required' => true,
                //     'label' => 'Fecha de la cuota a pagar',
                //     'read_only' => true,
                //     'attr' => array(
                //         'col' => "col-lg-6 col-md-6",
                //     ),
                //     'widget_type' => 'date',
                // ))
                ->add('fecha', 'date', array(
                    'widget' => 'choice',
                    'days' => array('1'),
                    'format' => 'yyyy - Md',
                    'required' => true,
                    'label' => 'Fecha de la cuota a pagar',
                    'read_only' => true,
                    'attr' => array(
                        'col' => "col-lg-6 col-md-6",
                    ),                    
                ))
                ->add('costo', null, array(
                    'label' => 'Costo',
                    'attr' => array(
                        'col' => "col-lg-6 col-md-6 txtCosto",
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Cuota'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_cuota';
    }

}
