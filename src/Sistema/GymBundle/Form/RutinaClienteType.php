<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * RutinaClienteType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class RutinaClienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'bootstrapdatetime', array(
                'required'   => true,
                'label'      => 'Fecha',
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'widget_type' => 'date',
            ))
            ->add('rutina', 'select2', array(
                'class' => 'Sistema\GymBundle\Entity\Rutina',
                'url'   => 'RutinaCliente_autocomplete_rutina',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
            ->add('cliente', 'select2', array(
                'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                'url'   => 'RutinaCliente_autocomplete_cliente',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\RutinaCliente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_gymbundle_rutinacliente';
    }
}
