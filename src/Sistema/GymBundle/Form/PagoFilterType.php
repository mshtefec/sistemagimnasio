<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;

/**
 * PagoFilterType filtro.
 * @author Nombre Apellido <name@gmail.com>
 */
class PagoFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fechaHasta = new \DateTime();
        $fechaDesde = clone $fechaHasta;
        $fechaDesde->modify('-1 months');

        $builder
            ->add('fecha', 'filter_date_range', array(
                'label'  => 'Rango de fechas desde - hasta',
                'left_date_options' => array(
                    'attr' => array(
                        'class' => "col-lg-3 col-md-6",
                    ),
                    'data' => $fechaDesde,
                ),
                'right_date_options' => array(
                    'attr' => array(
                        'class' => "col-lg-3 col-md-6",
                    ),
                    'data' => $fechaHasta,
                ),
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Pago'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_gymbundle_pagofiltertype';
    }
}
