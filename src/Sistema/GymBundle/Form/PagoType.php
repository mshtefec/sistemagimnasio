<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;
/**
 * PagoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class PagoType extends AbstractType {

    private $securityContext;

    public function __construct($securityContext)
    {
        $this->securityContext = $securityContext;
    }    

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $is_role_admin = $this->securityContext->isGranted('ROLE_ADMIN');

        $builder
            ->add('cliente', 'select2', array(
                'label' => 'Cliente',
                'required' => false,
                'class' => 'SistemaRRHHBundle:Cliente',
                'url' => 'Cuota_autocomplete_cliente',
                'configs' => array(
                    'multiple' => false, //required true or false
                    'width' => 'off'
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 txtCliente",
                ),
                'label_attr' => array(
                    'class' => "col-lg-2 col-md-2 col-sm-2",
                ),
            ))
            ->add('detalles', 'collection', array(
                'label' => false,
                'type' => new PagoCuotaType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('monto', null, array(
              'attr' => array(
                  'class' => "col-lg-6 col-md-6 col-sm-6 txtCliente",
              )
            ))
        ;
        if($is_role_admin){
        $builder
            ->add('fecha', 'bootstrapdatetime', array(
                    'required' => true,
                    'label' => 'Fecha',
                    'widget_type' => 'date',
                    'attr' => array(
                        'class' => "col-lg-7 col-md-7 col-sm-7",
                    ),
                    'label_attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12",
                    ),
                )
            );
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Pago',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_pago';
    }

}
