<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * GimnasioType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class GimnasioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        if($options['allow_extra_fields'] == 'superadmin'){

            $builder
                ->add('activo')
                ->add('nombre')
                ->add('user', 'entity', array(
                        'class' => 'SistemaUserBundle:User',
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                                ->where("u.username != 'superadmin'")
                                ->orderBy('u.username', 'ASC');
                        },
                        'required'    => true,
                    )
                )
            ;
        }else{
            $builder
                ->add('nombre')
            ;
        }

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Gimnasio'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_gimnasio';
    }

}
