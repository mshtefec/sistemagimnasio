<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\GymBundle\Form\RutinaDiasType;

/**
 * RutinaType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class RutinaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                /* ->add('fechaInicio', 'bootstrapdatetime', array(
                  'required' => true,
                  'label' => 'Fecha Inicio',
                  'label_attr' => array(
                  'class' => 'col-lg-3 col-md-3 col-sm-3 control-label',
                  ),
                  'widget_type' => 'date',
                  'attr' => array(
                  'class' => 'col-lg-3 col-md-3 col-sm-3',
                  ),
                  )) */
                ->add('cantidadSemanas', null, array(
                    'label_attr' => array(
                        'class' => 'control-label',
                    ),
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
                ->add('nombre', null, array(
                    'label_attr' => array(
                        'class' => 'col-lg-3 col-md-3 col-sm-3 control-label',
                    ),
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
                /* ->add('clientes', 'select2', array(
                  'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                  'url' => 'Rutina_autocomplete_clientes',
                  'configs' => array(
                  'multiple' => true, //required true or false
                  'width' => 'off',
                  ),
                  'label_attr' => array(
                  'class' => 'col-lg-3 col-md-3 col-sm-3 control-label',
                  ),
                  'attr' => array(
                  'class' => 'col-lg-12 col-md-12 col-sm-12',
                  )
                  )) */
                ->add('rutinaDias', 'collection', array(
                    'label_attr' => array(
                        'class' => 'col-lg-2 col-md-2 col-sm-2',
                    ),
                    'attr' => array(
                        'class' => 'col-lg-4 col-md-4 col-sm-4',
                    ),
                    'label' => false,
                    'type' => new RutinaDiasType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => true,
                    'by_reference' => false,
                    'prototype' => true,
                    'prototype_name' => '__name__'
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Rutina'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_rutina';
    }

}
