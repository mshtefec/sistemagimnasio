<?php

namespace Sistema\GymBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * HorarioType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class HorarioType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('dia', 'choice', array(
                'choices'   => $this->getDias(),
                'required'  => true,
            ))
            ->add('horaInicio', 'bootstrapdatetime', array(
                'required' => true,
                'label' => 'Horainicio',
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'widget_type' => 'time',
            ))
            ->add('horaFin', 'bootstrapdatetime', array(
                'required' => true,
                'label' => 'Horafin',
                'label_attr' => array(
                    'class' => 'col-lg-2 col-md-2 col-sm-2',
                ),
                'widget_type' => 'time',
            ))
            /*->add('actividad', 'select2', array(
                'class' => 'Sistema\GymBundle\Entity\Actividad',
                'url' => 'Horario_autocomplete_actividad',
                'configs' => array(
                    'multiple' => false, //required true or false
                    'width' => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))*/
        ;
    }

    private function getDias()
    {
        return array(
            'Lunes'     => 'Lunes',
            'Martes'    => 'Martes',
            'Miercoles' => 'Miercoles',
            'Jueves'    => 'Jueves',
            'Viernes'   => 'Viernes',
            'Sabado'    => 'Sabado',
            'Domingo'   => 'Domingo',
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\GymBundle\Entity\Horario'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_gymbundle_horario';
    }

}
