<?php

namespace Sistema\GymBundle\Services;

use Sistema\GymBundle\Entity\Cuota;

class CuotaHandler {

    private $container;
    private $em;

    public function __construct($container, $em) {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * Crear la cuota del mes del cliente en caso de que no exista.
     * El Flush se hace aca.
     * ACL se debe controlar y generar en el controlador
     */
    public function generarCuotaSegunAsistenciasMes($asistencias, $mes, $anio) {
        $cuotas = array();
        //Recorro para controlar las cuotas y crearlas
        foreach ($asistencias as $asistencia) {
            $cuota = $this->generarCuotaMes($asistencia->getActividadCobro(), $mes, $anio);
            if ($cuota) {
                array_push($cuotas, $cuota);
            }
        }

        $this->em->flush();

        return $cuotas;
    }

    /**
     * Crear la cuota del mes del cliente en caso de que no exista.
     * El Flush se hace en el controlador, si solo usa este metodo.
     * Retorna false si la cuota existe y no la genera.
     * ACL se debe controlar y generar en el controlador
     */
    public function generarCuotaMes($actividadCobro, $mes, $anio, $costoDeCuota = null) {
        //Obtener id del gimnasio actual.
        $idGymSession = $this->container->get('session')->get('_idGimnasio');
        //Obtener el objecto parcial del gimnasio actual.
        $partial_gym = $this->em->getPartialReference('Sistema\GymBundle\Entity\Gimnasio', $idGymSession);
        //Controlo si tiene cuota en ese mes
        
        $preferenciasUsuario = $this->container->get('security.token_storage')
                                               ->getToken()->getUser()
                                               ->getPreferencias();

        $metodoDeGeneracionDeCuota = $preferenciasUsuario->getGeneracionDeCuota();
      
        $cuota = $this->em->getRepository('SistemaGymBundle:Cuota')
                          ->existeCuotaEnFecha($actividadCobro->getId(), $mes, $anio);

        if($metodoDeGeneracionDeCuota == 1){ //Generacion de cuotas sin importar el dia de cobro

            if (is_null($cuota)) {

                $cuota = New Cuota();
                $cuota->setGimnasio($partial_gym);
                $cuota->setActividadCobro($actividadCobro);

                //Armo fecha
                $str = $anio . '-' . $mes . '-' . $actividadCobro->getDiaCobro();
                $fecha = \DateTime::createFromFormat('Y-m-d', $str);
                //Fin armo fecha

                $cuota->setFecha($fecha);

                if(!is_null($costoDeCuota)){

                    $cuota->setCosto($costoDeCuota);
                }else{

                    $cuota->setCosto($actividadCobro->getPlan()->getCosto());
                }
                
                
                $this->em->persist($cuota);
            } else {

                $cuota = false;
            }

        }elseif($metodoDeGeneracionDeCuota == 2){ //Generacion de cuotas segun en el dia cobro

            if(date('d') > $actividadCobro->getDiaCobro()){

                if (is_null($cuota)) {

                    $cuota = New Cuota();
                    $cuota->setGimnasio($partial_gym);
                    $cuota->setActividadCobro($actividadCobro);

                    //Armo fecha
                    $str = $anio . '-' . $mes . '-' . $actividadCobro->getDiaCobro();
                    $fecha = \DateTime::createFromFormat('Y-m-d', $str);
                    //Fin armo fecha

                    $cuota->setFecha($fecha);
                    $cuota->setCosto($actividadCobro->getPlan()->getCosto());
                    
                    $this->em->persist($cuota);
                } else {


                    $cuota = false;
                }
            } else {


                $cuota = false;
            }
        }

        return $cuota;
    }

}
