<?php

namespace Sistema\GymBundle\Services;

use Sistema\GymBundle\Entity\Pago;
use Sistema\GymBundle\Entity\PagoCuota;
use Sistema\GymBundle\Entity\Cuota;

/**
 * Description of Asistencia
 *
 * @author rodrigo
 */
class PagoService {

    private $em;
    private $securityContext;
    private $user;

    public function __construct($securityContext, $em) {
        $this->securityContext = $securityContext;
        $this->user = $securityContext->getToken()->getUser();
        $this->em = $em;
    }

    public function calcularSaldoXCliente($id_cliente, $saldoFavor = false){

        $costos = $this->em->getRepository("SistemaGymBundle:Cuota")->getTotalCuota($id_cliente);
        $cobros = $this->em->getRepository("SistemaGymBundle:Pago")->getTotalCobros($id_cliente);

        if ($saldoFavor){

            if($costos - $cobros < 0){
                return $cobros - $costos;
            }else{
                return 0;
            }
        }else{

            if($costos - $cobros > 0){
                return $costos - $cobros;
            }else{
                return 0;
            }
        }
    }
    
    public function generarPagosPorCuota($cuotas) {
        
        $monto = 0;
        $pago = new Pago();
        $cliente = $cuotas[0]->getActividadcobro()->getCliente();
        $pago->setCliente($cliente);

        foreach ($cuotas as $cuota) {

            $cuota->setPagada(true);

            $detalle = new PagoCuota();
            $monto += $cuota->calcularSaldo();

            $detalle->setMonto($monto);

            $pago->addDetalle($detalle);

            $pago->setUser($this->user);
            $cuota->addPago($detalle);
        }

        $pago->setMonto($monto);
        
        $this->em->persist($pago);
        $this->em->flush();
    }

    public function generarPagosConSaldo($cuota) {

        $cliente    = $cuota->getActividadCobro()->getCliente();
        $cliente_id = $cliente->getId();

        $saldoFavor = $this->calcularSaldoXCliente($cliente_id, true);

        if($saldoFavor > 0){

            $pago = new Pago();
            $pago->setCliente($cliente);

            $detalle = new PagoCuota();

            if($cuota->getCosto() <= $saldoFavor){

                $detalle->setMonto($cuota->getCosto());
                $cuota->setPagada(true);
            }else{

                $detalle->setMonto($saldoFavor);
            }

            $pago->setUser($this->user);
            
            $pago->addDetalle($detalle);
            $cuota->addPago($detalle);

            $this->em->persist($pago);
        }
    }

    public function calcularPagos($cliente, $pago) {

        $cuotas = $this->em->getRepository('SistemaGymBundle:Cuota')->queryByIdCliente($cliente, true);

        if (!is_null($cuotas)) {

        //Me fijo si las cuotas se pueden poner como pagadas
            $suma = $pago->getMonto();

            $saldo = $this->calcularSaldoXCliente($cliente, true);

            foreach ($cuotas as $cuota) {

                if (($suma + $saldo) <= 0) {

                    break;
                }

                $temp = $suma;

                $suma = $suma + $cuota->calcularSaldoNegativo();

                $detalle = new \Sistema\GymBundle\Entity\PagoCuota();

                if (($suma + $saldo) >= 0) {

                    $cuota->setPagada(true);
                    $detalle->setMonto(($cuota->calcularSaldo()));
                } else {

                    $detalle->setMonto($temp);
                }

                if(!$this->securityContext->isGranted('ROLE_ADMIN')){

                    $pago->setFecha(new \DateTime('now'));
                }

                $pago->setUser($this->user);
                $pago->addDetalle($detalle);
                
                $cuota->addPago($detalle);

                $this->em->persist($detalle);
            }
        }else{

            return false;
        }
    }
}
