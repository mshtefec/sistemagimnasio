<?php

namespace Sistema\GymBundle\Services;

use Sistema\GymBundle\Entity\Asistencia;
use Sistema\RRHHBundle\Entity\Cliente;
use Sistema\GymBundle\Entity\ActividadCobro;

/**
 * Description of Asistencia
 *
 * @author rodrigo
 */
class AsistenciaService {

    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    //crea la asistencia y retorna el objeto
    public function crearAsistencia($_fecha = null, $_formatoDeFecha = null, Cliente $_cliente = null, ActividadCobro $_actividadCobro = null) {

        if (is_null($_formatoDeFecha)){
        
            $_formatoDeFecha = 'd/m/Y H:i';
        }

        if(is_null($_fecha)){

            $_fecha = new \DateTime("now");
        }

        if (is_string($_fecha)) {

            $fecha = \DateTime::createFromFormat($_formatoDeFecha, $_fecha);
        }

        if(!is_null($_actividadCobro) && ($fecha >= $_actividadCobro->getFecha()) ){
            
            $actividadesCobros = null;
            if (is_null($_actividadCobro)) {
                //Obtener las actividades del cliente
                $actividadesCobrosArray = $_cliente->getActividadCobros();
                if ($actividadesCobrosArray->count() > 0) {
                    $actividadesCobros = $actividadesCobrosArray[0];
                }
            } else {
                $actividadesCobros = $_actividadCobro;
            }

            if (!is_null($actividadesCobros)) {
                
                //creo asistencia
                $asistencia = new Asistencia();
                $asistencia->setGimnasio($_cliente->getGimnasio());
                $asistencia->setCliente($_cliente);
                
                //obtengo primer actividad cobro para setear a la asistencia
                $asistencia->setActividadCobro($actividadesCobros);
              
                $fecha = \DateTime::createFromFormat($_formatoDeFecha, $_fecha);
                $dia = $fecha->format('d');
                $mes = $fecha->format('m');
                $anio = $fecha->format('Y');
                $hora = $fecha;
                $asistencia->setDia($dia);
                $asistencia->setMes($mes);
                $asistencia->setAnio($anio);
                $asistencia->setHora($hora);

                $this->em->persist($asistencia);
            } else {
                
                //retorno un mensaje
                $asistencia = "El cliente no esta inscripto en una actividad-plan";
            }
        }else{

            $asistencia = "El cliente tiene una fecha de inscripcion a la actividad posterior a la fecha de esta asistencia.";
        }
        return $asistencia;
    }

}
