<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\RutinaCliente;
use Sistema\GymBundle\Form\RutinaClienteType;
use Sistema\GymBundle\Form\RutinaClienteFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * RutinaCliente controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/rutinacliente")
 */
class RutinaClienteController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/RutinaCliente.yml',
    );

    /**
     * Lists all RutinaCliente entities.
     *
     * @Route("/", name="admin_rutinacliente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new RutinaClienteFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new RutinaCliente entity.
     *
     * @Route("/", name="admin_rutinacliente_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:RutinaCliente:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new RutinaClienteType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new RutinaCliente entity.
     *
     * @Route("/new", name="admin_rutinacliente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new RutinaClienteType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a RutinaCliente entity.
     *
     * @Route("/{id}", name="admin_rutinacliente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing RutinaCliente entity.
     *
     * @Route("/{id}/edit", name="admin_rutinacliente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new RutinaClienteType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing RutinaCliente entity.
     *
     * @Route("/{id}", name="admin_rutinacliente_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:RutinaCliente:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new RutinaClienteType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a RutinaCliente entity.
     *
     * @Route("/{id}", name="admin_rutinacliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a RutinaCliente entity.
     *
     * @Route("/autocomplete-forms/get-rutina", name="RutinaCliente_autocomplete_rutina")
     */
    public function getAutocompleteRutina() {
        $options = array(
            'repository' => "SistemaGymBundle:Rutina",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a RutinaCliente entity.
     *
     * @Route("/autocomplete-forms/get-cliente", name="RutinaCliente_autocomplete_cliente")
     */
    public function getAutocompleteCliente() {
        $options = array(
            'repository' => "SistemaRRHHBundle:Cliente",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Rutina entity.
     *
     * @Route("/rutina-cliente/crear", name="RutinaCliente_crear")
     */
    public function crearRutinaClienteAjax() {
        $request = $this->getRequest();
        $resultado = false;
        $request = $this->getRequest();
        $idRutina = $request->request->get('rutina', null);
        $clientesArray = $request->request->get('clientes', null);

        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository('SistemaRRHHBundle:Cliente')->findClienteByArrayId($clientesArray);
        $rutina = $em->getRepository('SistemaGymBundle:Rutina')->find($idRutina);

        if (!is_null($rutina)) {
            foreach ($clientes as $cliente) {
                $rutinaCliente = new RutinaCliente();
                $rutinaCliente->setRutina($rutina);
                $rutinaCliente->setCliente($cliente);
                $rutinaCliente->setFecha(date_create(date("d-m-Y")));
                $em->persist($rutinaCliente);
            }
            
            $em->flush();
            $resultado = true;
        }

        
        $array = array(
            'resultado' => $resultado
        );
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
