<?php

namespace Sistema\GymBundle\Controller;

use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Plan;
use Sistema\GymBundle\Form\PlanType;
use Sistema\GymBundle\Form\PlanFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Plan controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/superadmin/plan")
 */
class PlanController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Plan.yml',
    );

    /**
     * Lists all Plan entities.
     *
     * @Route("/", name="admin_plan")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new PlanFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Plan entity.
     *
     * @Route("/", name="admin_plan_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Plan:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new PlanType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Plan entity.
     *
     * @Route("/new", name="admin_plan_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new PlanType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Plan entity.
     *
     * @Route("/{id}", name="admin_plan_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Plan entity.
     *
     * @Route("/{id}/edit", name="admin_plan_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new PlanType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Plan entity.
     *
     * @Route("/{id}", name="admin_plan_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Plan:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new PlanType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Plan entity.
     *
     * @Route("/{id}", name="admin_plan_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
       $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Plan entity.
     *
     * @Route("/autocomplete-forms/get-actividadCobros", name="Plan_autocomplete_actividadCobros")
     */
    public function getAutocompleteActividadCobro() {
        $options = array(
            'repository' => "SistemaGymBundle:ActividadCobro",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Plan entity.
     *
     * @Route("/autocomplete-forms/get-actividad", name="Plan_autocomplete_actividad")
     */
    public function getAutocompleteActividad() {
        $options = array(
            'repository' => "SistemaGymBundle:Actividad",
            'field' => "nombre",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_plan_export")
     */
    public function getExporter($format)
    {
       $response = parent::exportCsvAction($format);

        return $response;
    }

}
