<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Gimnasio;
use Sistema\GymBundle\Form\GimnasioType;
use Sistema\GymBundle\Form\GimnasioFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Gimnasio controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/gimnasio")
 */
class GimnasioController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Gimnasio.yml',
    );

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        //creo query
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
                ->createQueryBuilder('a')
                ->select('a', 'u')
                ->join('a.user', 'u')
        ;
        //si no es ROLE_SUPER_ADMIN entro y filtro segun usuario
        $securityContext = $this->container->get('security.context');
        if (false === $securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            //obtengo id de usuario
            $userId = $this->getUser()->getId();
            $queryBuilder
                    ->where('u.id = :userId')
                    ->setParameter('userId', $userId)
            ;
        }
        //ordeno consulta DESC
        $queryBuilder->orderBy('a.id', 'DESC');

        return $queryBuilder;
    }

    /**
     * Lists all Gimnasio entities.
     *
     * @Route("/", name="admin_gimnasio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        if ($this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->config['filterType'] = new GimnasioFilterType();
            $config = $this->getConfig();
        } else if ($this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            $this->config['filterType'] = new GimnasioFilterType();
            $config = $this->getConfig();
            unset($config['fieldsindex']['u.username']);
        } else if ($this->container->get('security.context')->isGranted('ROLE_EMPLEADO')) {
            $response = $this->forward('SistemaRRHHBundle:Cliente:index');

            return $response;
        }

        list($filterForm, $queryBuilder) = $this->filter($config);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );
        //remove the form to return to the view
        unset($config['filterType']);

        return array(
            'config' => $config,
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Creates a new Gimnasio entity.
     *
     * @Route("/", name="admin_gimnasio_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Gimnasio:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new GimnasioType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //guardo el usuario logueado relacionado al gimnasio

            if (is_null($entity->getUser())) {

                $entity->setUser($this->getUser());
                $user = $entity->getUser();
            } else {

                $user = $entity->getUser();
            }

            //controlo la cantidad de gimnasios
            if ($user->getCantidadGimnasio() > (count($user->getGimnasios()))) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->useACL($entity, 'create');

                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
                $session = $request->getSession();
                $session->set('_idGimnasio', $entity->getId());

                return $this->redirect($nextAction);
            } else {

                if ($this->getUser()->getUsername() == 'superadmin') {

                    $this->get('session')->getFlashBag()
                            ->add('danger', 'Esta cuenta ya no puede tener mas gimnasios, se debe actualizar su plan de cuenta.');
                } else {

                    $this->get('session')->getFlashBag()
                            ->add('danger', 'Ud. no puede crear mas gimnasios para esta cuenta, debe contactarnos para actualizar su plan.');
                }

                return $this->redirectToRoute("admin_gimnasio");
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Gimnasio entity.
     *
     * @Route("/new", name="admin_gimnasio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new GimnasioType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Gimnasio entity.
     *
     * @Route("/{id}", name="admin_gimnasio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Gimnasio entity.
     *
     * @Route("/{id}/edit", name="admin_gimnasio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new GimnasioType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a Post entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($config, $id) {
        //Entra si NO ES superadmin
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $label = 'views.recordactions.disable';
            $mensaje = $this->get('translator')->trans('views.recordactions.disableConfirm', array(), 'MWSimpleAdminCrudBundle');
        } else {
            $label = 'views.recordactions.delete'; //Superadmin Borra
            $mensaje = $this->get('translator')->trans('views.recordactions.delete', array(), 'MWSimpleAdminCrudBundle');
        }

        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($config['delete'], array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleAdminCrudBundle',
                            'label' => $label,
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * Edits an existing Gimnasio entity.
     *
     * @Route("/{id}", name="admin_gimnasio_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Gimnasio:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new GimnasioType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            if (!array_key_exists('saveAndAdd', $config)) {
                $config['saveAndAdd'] = true;
            } elseif ($config['saveAndAdd'] != false) {
                $config['saveAndAdd'] = true;
            }
            if ($config['saveAndAdd']) {
                $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            } else {
                $nextAction = $this->generateUrl($config['show'], array('id' => $id));
            }
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($config, $entity) {
        $form = $this->createForm($config['editType'], $entity, array(
            'action' => $this->generateUrl($config['update'], array('id' => $entity->getId())),
            'method' => 'PUT',
            'allow_extra_fields' => $this->getUser()->getUsername(),
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'form-control btn-success',
                        'col' => 'col-lg-2',
                    )
                ))
        ;

        if (!array_key_exists('saveAndAdd', $config)) {
            $config['saveAndAdd'] = true;
        } elseif ($config['saveAndAdd'] != false) {
            $config['saveAndAdd'] = true;
        }

        if ($config['saveAndAdd']) {
            $form
                    ->add('saveAndAdd', 'submit', array(
                        'translation_domain' => 'MWSimpleAdminCrudBundle',
                        'label' => 'views.new.saveAndAdd',
                        'attr' => array(
                            'class' => 'form-control btn-primary',
                            'col' => 'col-lg-3',
                        )
                    ))
            ;
        }

        return $form;
    }

    /**
     * Creates a form to create a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {
        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
            'allow_extra_fields' => $this->getUser()->getUsername(),
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array(
                        'class' => 'form-control btn-success',
                        'col' => 'col-lg-2',
                    )
                ))
        ;

        if (!array_key_exists('saveAndAdd', $config)) {
            $config['saveAndAdd'] = true;
        } elseif ($config['saveAndAdd'] != false) {
            $config['saveAndAdd'] = true;
        }

        if ($config['saveAndAdd']) {
            $form
                    ->add('saveAndAdd', 'submit', array(
                        'translation_domain' => 'MWSimpleAdminCrudBundle',
                        'label' => 'views.new.saveAndAdd',
                        'attr' => array(
                            'class' => 'form-control btn-primary',
                            'col' => 'col-lg-3',
                        )
                    ))
            ;
        }

        return $form;
    }

    /**
     * Deletes a Gimnasio entity.
     *
     * @Route("/{id}", name="admin_gimnasio_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $form = $this->createDeleteForm($config, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($config['repository'])->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }
            //Entra si NO ES superadmin
            if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
                $entity->setActivo(false);
            } else {
                $em->remove($entity); //Superadmin Borra
            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl($config['index']));
    }

    /**
     * Autocomplete a Gimnasio entity.
     *
     * @Route("/autocomplete-forms/get-actividades", name="Gimnasio_autocomplete_actividades")
     */
    public function getAutocompleteActividad() {
        $options = array(
            'repository' => "SistemaGymBundle:Actividad",
            'field' => "nombre",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Gimnasio entity.
     *
     * @Route("/autocomplete-forms/get-user", name="Gimnasio_autocomplete_user")
     */
    public function getAutocompleteUser() {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Gimnasio entity.
     *
     * @Route("/actualizar-session/gimnasio", name="Gimnasio_actualizar_session")
     */
    public function actualizarSessionGim() {
        $request = $this->getRequest();
        $idGim = $request->request->get('id', null);

        $session = $request->getSession();
        $session->set('gimnasio', $idGim);
        $array = array();

        $array[] = array(
            'resultado' => true,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_gimnasio_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
