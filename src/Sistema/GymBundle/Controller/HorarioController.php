<?php

namespace Sistema\GymBundle\Controller;

use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Horario;
use Sistema\GymBundle\Form\HorarioType;
use Sistema\GymBundle\Form\HorarioFilterType;

/**
 * Horario controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/horario")
 */
class HorarioController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Horario.yml',
    );

    /**
     * Lists all Horario entities.
     *
     * @Route("/", name="admin_horario")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new HorarioFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Horario entity.
     *
     * @Route("/", name="admin_horario_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Horario:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new HorarioType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Horario entity.
     *
     * @Route("/new", name="admin_horario_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new HorarioType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Horario entity.
     *
     * @Route("/{id}", name="admin_horario_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Horario entity.
     *
     * @Route("/{id}/edit", name="admin_horario_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new HorarioType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Horario entity.
     *
     * @Route("/{id}", name="admin_horario_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Horario:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new HorarioType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Horario entity.
     *
     * @Route("/{id}", name="admin_horario_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Horario entity.
     *
     * @Route("/autocomplete-forms/get-actividad", name="Horario_autocomplete_actividad")
     */
    public function getAutocompleteActividad()
    {
        $options = array(
            'repository' => "SistemaGymBundle:Actividad",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}
