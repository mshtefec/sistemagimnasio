<?php

namespace Sistema\GymBundle\Controller;

use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Asistencia;
use Sistema\GymBundle\Form\AsistenciaType;
use Sistema\GymBundle\Form\AsistenciaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Asistencia controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/asistencia")
 */
class AsistenciaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Asistencia.yml',
    );

    /**
     * Lists all Asistencia entities.
     *
     * @Route("/", name="admin_asistencia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        //$this->config['filterType'] = new AsistenciaFilterType();
        //$response = parent::indexAction();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $session = $this->getRequest()->getSession();
            $idGymSession = $session->get('_idGimnasio');
            $entities = $em->getRepository('SistemaRRHHBundle:Cliente')->getClientesActivos($idGymSession);
        } else {
            $entities = $em->getRepository('SistemaRRHHBundle:Cliente')->getClientesActivos();
        }
        $entity = new Asistencia();
        $form = $this->createForm(new AsistenciaType(), $entity, array(
            'attr' => ['id' => 'formAsistencia'],
            'action' => 'crear_asistencia',
            'method' => 'POST',
        ));

        return array(
            'config' => $config,
            'entities' => $entities,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new Asistencia entity.
     *
     * @Route("/", name="admin_asistencia_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Asistencia:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new AsistenciaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Asistencia entity.
     *
     * @Route("/new", name="admin_asistencia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new AsistenciaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Asistencia entity.
     *
     * @Route("/{id}", name="admin_asistencia_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Asistencia entity.
     *
     * @Route("/{id}/edit", name="admin_asistencia_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new AsistenciaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Asistencia entity.
     *
     * @Route("/{id}", name="admin_asistencia_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Asistencia:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new AsistenciaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Asistencia entity.
     *
     * @Route("/{id}", name="admin_asistencia_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Asistencia entity.
     *
     * @Route("/autocomplete-forms/get-actividadCobro", name="Asistencia_autocomplete_actividadCobro")
     */
    public function getAutocompleteActividadCobro() {
        $options = array(
            'repository' => "SistemaGymBundle:ActividadCobro",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * @Route("/asistencia-by-cliente-ActividadC", name="Asistencia_by_cliente_actividadCobro")
     */
    public function getAsistenciaByClienteActividadCobro() {
        $request = $this->getRequest();
        $session = $this->getRequest()->getSession();

        $cliente = $request->request->get('idCliente', null);
        $idGymSession = $session->get('_idGimnasio');

        $array = $this->obtenerAsistenciaByActividaCobro($cliente, $idGymSession);


        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/create-asistencia-by-ajax", name="crear_asistencia_by_ajax")
     * @Method("POST")
     */
    public function createAsistenciaByAjaxAction() {
        $em      = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $fecha     = $request->request->get('fecha', null);
        $idCliente = $request->request->get('idCliente', null);
        $idActividadCobro = $request->request->get('idActividadCobro', null);
                
        $cliente        = null;
        $mensaje        = null;
        $resultado      = false;
        $asistencia     = null;
        $actividadCobro = null;

        if (!is_null($idCliente)) {

            $cliente = $em->getRepository('SistemaRRHHBundle:Cliente')->find($idCliente);

            if (!is_null($cliente)) {

                if (!is_null($idActividadCobro)) {

                    $actividadCobro = $em->getRepository('SistemaGymBundle:ActividadCobro')
                                         ->find($idActividadCobro);

                    // Generar asistencia nueva. GymBundle/Services/Asistencia
                    $asistencia = $this->get('gym_asistencia')
                                       ->crearAsistencia($fecha, null, $cliente, $actividadCobro);
                } else {

                    // Generar asistencia nueva. GymBundle/Services/Asistencia
                    $asistencia = $this->get('gym_asistencia')
                                       ->crearAsistencia($fecha, null, $cliente);
                }
            }
        }

        if (!is_null($asistencia) && !is_string($asistencia)) {

            // Genero cuota segun asistencia GymBundle/Services/Cuota
            $cuota = $this->get('gym_cuota')
                          ->generarCuotaMes($asistencia->getActividadCobro(), 
                                            $asistencia->getMes(), 
                                            $asistencia->getAnio()
            );

            $em->flush();
            $this->useACL($asistencia, 'create');
            $this->useACL($cuota, 'create');
            $resultado = true;

        } else {

            $mensaje = $asistencia;
        }

        $array = array(
            'mensaje'   => $mensaje,
            'resultado' => $resultado
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/create-asistencia", name="crear_asistencia")
     * @Method("POST")
     */
    public function createAsistencia() {
        $request = $this->getRequest();
        $idActividadCobro = $request->request->get('hidAsistencia', null);

        $entity = new Asistencia();
        $form = $this->createForm(new AsistenciaType(), $entity, array(
            'attr' => ['id' => 'formAsistencia'],
            'action' => 'crear_asistencia',
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        $resultado = false;
        $cuota_service = $this->container->get('gym_cuota');

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $actividadCobro = $em->getRepository("SistemaGymBundle:ActividadCobro")->find($idActividadCobro);

            if (!is_null($actividadCobro)) {

                $entity->setActividadCobro($actividadCobro);

                if (!is_null($entity->getHora())) {

                    $entity->setHora(new \DateTime($entity->getHora()));
                } else {

                    $entity->setHora(new \DateTime(date("H:i")));
                }

                $em->persist($entity);

                $cuota_service->generarCuotaMes($entity->getActividadCobro(), $entity->getMes(), $entity->getAnio());

                $em->flush();
                $resultado = true;
            }
        }

        $array[] = array(
            'resultado' => $resultado,
            'count' => 0,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Genera la asistencia con dia 1 y a su vez controla si tiene cuota
     * Si la cuota no existe la crea.
     *
     * @Route("/generar-asistencias-mes", name="admin_generar_asistencias_mes")
     */
    public function generarAsistenciasMes() {
        $request = $this->getRequest();

        $mes = $request->request->get('mes', null);
        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $clientes = $em->getRepository('SistemaRRHHBundle:Cliente')
                ->getClientesActivos($idGymSession);

        $asistencias = array();

        $fechaNow = new \DateTime("now");
        $anio = $fechaNow->format('Y');

        foreach ($clientes as $cliente) {
            $actividadesCobros = $cliente->getActividadCobros();
            if ($actividadesCobros->count() > 0) {
                foreach ($actividadesCobros as $actividadCobro) {
                    $crearAsistencia = false;
                    $fechaActCobro = $actividadCobro->getFecha();
                    if ($fechaActCobro->format('Y') < $anio) {
                        //Entra si el anio es menor
                        $crearAsistencia = true;
                    } elseif ($fechaActCobro->format('Y') == $anio && $fechaActCobro->format('m') <= $mes) {
                        //Entra si el anio es igual y el mes es menor o igual
                        $crearAsistencia = true;
                    }
                    if ($crearAsistencia) {
                        $entity = new Asistencia();
                        $entity->setAnio($anio);
                        $entity->setMes($mes);
                        $entity->setDia(1);
                        $entity->setHora($fechaNow);
                        $entity->setActividadCobro($actividadCobro);

                        $em->persist($entity);
                        array_push($asistencias, $entity);
                    }
                }
            }
        }

        $em->flush();

        //Recorro para ACL y para controlar las cuotas
        foreach ($asistencias as $asistencia) {
            $this->useACL($asistencia, 'create'); //Seteo ACL
        }

        //Obtengo GymBundle/Services/Cuota
        $serviceCuota = $this->get('gym_cuota');
        //Utilizo el Service para crear la cuota de las asistencias
        $cuotas = $serviceCuota->generarCuotaSegunAsistenciasMes($asistencias, $mes, $anio); //Recorro para ACL
        foreach ($cuotas as $cuota) {
            $this->useACL($cuota, 'create'); //Seteo ACL
        }

        $response = new JsonResponse();
        $response->setData(true);

        return $response;
    }

    /**
     * @Route("/eliminar-asistencia", name="Asitencia_eliminar_byActividadCobroFecha")
     * @Method("POST")
     */
    public function eliminarAsitenciaByActCobroFecha() {
        $request = $this->getRequest();
        $idAsistencia = $request->request->get('id', null);

        $resultado = false;
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaGymBundle:Asistencia')->find($idAsistencia);
        $cliente = $entity->getCliente()->getId();
        $gimnasio = $entity->getGimnasio()->getId();

        if (!is_null($entity)) {
            $em->remove($entity);
            $em->flush();
            $resultado = true;
        }
        $array = $this->obtenerAsistenciaByActividaCobro($cliente, $gimnasio);

        $array["resultado"] = $resultado;
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    private function obtenerAsistenciaByActividaCobro($cliente, $idGymSession) {
        $em = $this->getDoctrine()->getManager();
        //obtengo todas las asistencias del cliente segun gym seleccionado
        $asistencias = $em->getRepository("SistemaGymBundle:Asistencia")->getAsistenciaByCliente($cliente, $idGymSession);

        $calendario = $em->getRepository("SistemaGymBundle:Asistencia")->getAsistenciaByClienteGroupBy($cliente, $idGymSession);

        $arrayAsistencia = array();
        foreach ($asistencias as $asist) {
            $arrayAsistencia[] = array(
                'id' => $asist->getId(),
                'fecha' => $asist->__toString(),
                'hora' => $asist->getHora()->format('H:i')
            );
        }

        $array = array(
            'calendario' => $calendario,
            'asistencias' => $arrayAsistencia
        );
        return $array;
    }

    /**
     * Se utiliza en el DefaultController gimnasioEstadistica
     * @Route("/asistencia-by-plan-rango-fecha", name="Asistencia_plan_rangoFecha")
     */
    public function getAsistenciaByPlanRangloFecha() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $idPlan = $request->request->get('plan', null);
        $fechaInicial = $request->request->get('fechaInicio', null);
        $fechaFin = $request->request->get('fechaFin', null);
        $arrayDatos = array();
        $arrayCantidad = array();
        $asistencia = $em->getRepository("SistemaGymBundle:Asistencia")
            ->getAsistenciaByPlanFecha($idPlan, $fechaInicial, $fechaFin);
        $cantidadAsistencia = count($asistencia);

        foreach ($asistencia as $e) {
            if ($e['clieEstado'] == true) {
                $estado = "<span class='label label-success'>Activo</span>";
            } else {
                $estado = "<span class='label label-danger'>Desactivado</span>";
            }
            $arrayDatos[] = array(
                'fecha' => $e['dia'] . '/' . $e['mes'] . '/' . $e['anio'],
                'hora' => $e['hora']->format('h:i'),
                'cliente' => $e['clieApellido'] . ' ' . $e['clieNombre'] . ' ' . $estado,
            );
        }
        $arrayCantidad[] = array(
            'cantidad' => $cantidadAsistencia,
        );

        $array[] = $arrayDatos;
        $array[] = $arrayCantidad;
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
