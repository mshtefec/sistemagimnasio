<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $gimnasios = $em->getRepository('SistemaGymBundle:Gimnasio')->findAll();

        return array(
            'gimnasios' => $gimnasios,
        );
    }

    /**
     * @Route("/cambiar-color", name="cambiar_color")
     * @Template()
     */
    public function cambiarColorAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $theme = $request->request->get('theme', null);
        $tipo = $request->request->get('tipo', null);

        $user = $this->get('security.context')->getToken()->getUser();
        $resultado = true;

        try {
            $user->getConfiguracion()->setItemConfiguraciones($tipo, $theme);
            $em->flush();
        } catch (Exception $e) {
            $resultado = false;
        }

        $array = array();

        $array[] = array(
            'resultado' => $resultado,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction() {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $referer = 'admin_gimnasio';
        } else if ($this->get('security.authorization_checker')->isGranted('ROLE_EMPLEADO')) {
            $referer = 'admin_cliente';
        } else {
            $referer = 'homepage';
        }

        return $this->redirect($this->generateUrl($referer));
    }

    /**
     * @Route("/admin/ayuda", name="default_admin_ayuda")
     * @Template()
     */
    public function adminAyudaAction() {
        return null;
    }

    /**
     * @Route("/admin/estadisticas", name="default_admin_estadisticas")
     * @Template()
     */
    public function adminEstadisticasAction() {
        return null;
    }

    /**
     * @Route("/admin/estadistica-cap", name="estadistica_clientes_asistencias_plan")
     * @Template()
     */
    public function adminEstadisticaCapAction() {
        return null;
    }
}
