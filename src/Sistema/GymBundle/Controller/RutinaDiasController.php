<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\RutinaDias;
use Sistema\GymBundle\Form\RutinaDiasType;
use Sistema\GymBundle\Form\RutinaDiasFilterType;

/**
 * RutinaDias controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/rutinadias")
 */
class RutinaDiasController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/RutinaDias.yml',
    );

    /**
     * Lists all RutinaDias entities.
     *
     * @Route("/", name="admin_rutinadias")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new RutinaDiasFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new RutinaDias entity.
     *
     * @Route("/", name="admin_rutinadias_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:RutinaDias:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new RutinaDiasType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new RutinaDias entity.
     *
     * @Route("/new", name="admin_rutinadias_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new RutinaDiasType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a RutinaDias entity.
     *
     * @Route("/{id}", name="admin_rutinadias_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing RutinaDias entity.
     *
     * @Route("/{id}/edit", name="admin_rutinadias_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new RutinaDiasType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing RutinaDias entity.
     *
     * @Route("/{id}", name="admin_rutinadias_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:RutinaDias:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new RutinaDiasType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a RutinaDias entity.
     *
     * @Route("/{id}", name="admin_rutinadias_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a RutinaDias entity.
     *
     * @Route("/autocomplete-forms/get-combinacion", name="RutinaDias_autocomplete_combinacion")
     */
    public function getAutocompleteCombinacion()
    {
        $options = array(
            'repository' => "SistemaGymBundle:Combinacion",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a RutinaDias entity.
     *
     * @Route("/autocomplete-forms/get-rutina", name="RutinaDias_autocomplete_rutina")
     */
    public function getAutocompleteRutina()
    {
        $options = array(
            'repository' => "SistemaGymBundle:Rutina",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}