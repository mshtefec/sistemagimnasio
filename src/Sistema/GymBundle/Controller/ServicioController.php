<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Servicio;
use Sistema\GymBundle\Form\ServicioType;
use Sistema\GymBundle\Form\ServicioFilterType;

/**
 * Servicio controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/superadmin/servicio")
 */
class ServicioController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Servicio.yml',
    );

    /**
     * Lists all Servicio entities.
     *
     * @Route("/", name="superadmin_servicio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ServicioFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Servicio entity.
     *
     * @Route("/", name="superadmin_servicio_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Servicio:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ServicioType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form   = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            if (!array_key_exists('saveAndAdd', $config)) {
                $config['saveAndAdd'] = true;
            } elseif ($config['saveAndAdd'] != false) {
                $config['saveAndAdd'] = true;
            }

            if ($config['saveAndAdd']) {
                $nextAction = $form->get('saveAndAdd')->isClicked()
                ? $this->generateUrl($config['new'])
                : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            } else {
                $nextAction = $this->generateUrl($config['show'], array('id' => $entity->getId()));
            }

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Servicio entity.
     *
     * @Route("/new", name="superadmin_servicio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ServicioType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Servicio entity.
     *
     * @Route("/{id}", name="superadmin_servicio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Servicio entity.
     *
     * @Route("/{id}/edit", name="superadmin_servicio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ServicioType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Servicio entity.
     *
     * @Route("/{id}", name="superadmin_servicio_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Servicio:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ServicioType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Servicio entity.
     *
     * @Route("/{id}", name="superadmin_servicio_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Servicio.
     *
     * @Route("/exporter/{format}", name="superadmin_servicio_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Servicio entity.
     *
     * @Route("/autocomplete-forms/get-users", name="Servicio_autocomplete_users")
     */
    public function getAutocompleteUser()
    {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field'      => "username",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}