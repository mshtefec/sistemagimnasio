<?php

namespace Sistema\GymBundle\Controller;

use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\ActividadCobro;
use Sistema\GymBundle\Entity\Asistencia;
use Sistema\GymBundle\Form\ActividadCobroType;
use Sistema\GymBundle\Form\ActividadCobroFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * ActividadCobro controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/actividadcobro")
 */
class ActividadCobroController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/ActividadCobro.yml',
    );

    /**
     * Lists all ActividadCobro entities.
     *
     * @Route("/", name="admin_actividadcobro")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ActividadCobroFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $security = $this->get('security.context');

        if ($security->isGranted('ROLE_ADMIN')) {
            $idGymSession = $session->get('_idGimnasio');
        } else {
            $user = $security->getToken()->getUser();
            $idGymSession = $user->getPersonal()->getGimnasio()->getId();
        }

        $qb = $em->createQueryBuilder();
        $qb
                ->select('a, cli, gim, cuentacorriente')
                ->from('Sistema\GymBundle\Entity\ActividadCobro', 'a')
                ->join('a.cliente', 'cli')
                ->leftJoin('cli.cuentaCorriente', 'cuentacorriente')
                ->join('cli.gimnasio', 'gim')
        ;
        if (!$security->isGranted('ROLE_SUPER_ADMIN')) {
            $qb
                    ->where("gim.id = :idGym")
                    ->setParameter("idGym", $idGymSession)
            ;
        }

        return $qb->orderby("a.id");
    }

    /**
     * Creates a new ActividadCobro entity.
     *
     * @Route("/empleado/", name="admin_actividadcobro_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:ActividadCobro:new.html.twig")
     */
    public function createAction() {
        $this->instanciarFormularioActividadCobroType('newType');

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            //Obtener id del gimnasio actual.
            $idGymSession = $this->container->get('session')->get('_idGimnasio');

            //Obtener el objecto parcial del gimnasio actual.
            $partial_gym = $em->getPartialReference('Sistema\GymBundle\Entity\Gimnasio', $idGymSession);

            //Settear el gimnasio en la entidad actividadCobro.
            $entity->setGimnasio($partial_gym);

            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            if (!array_key_exists('saveAndAdd', $config)) {
                $config['saveAndAdd'] = true;
            } elseif ($config['saveAndAdd'] != false) {
                $config['saveAndAdd'] = true;
            }

            if ($config['saveAndAdd']) {
                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) :
                  $this->generateUrl($config['show'], array('id' => $entity->getId()));
            } else {
                $nextAction = $this->generateUrl($config['show'], array('id' => $entity->getId()));
            }

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {
        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
            'attr' => array('class' => 'form-horizontal')
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new ActividadCobro entity.
     *
     * @Route("/empleado/new", name="admin_actividadcobro_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->instanciarFormularioActividadCobroType('newType');

        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ActividadCobro entity.
     *
     * @Route("/empleado/{id}", name="admin_actividadcobro_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing ActividadCobro entity.
     *
     * @Route("/{id}/edit", name="admin_actividadcobro_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->instanciarFormularioActividadCobroType('editType');
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing ActividadCobro entity.
     *
     * @Route("/{id}", name="admin_actividadcobro_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:ActividadCobro:edit.html.twig")
     */
    public function updateAction($id) {
        $this->instanciarFormularioActividadCobroType('editType');
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a ActividadCobro entity.
     *
     * @Route("/{id}", name="admin_actividadcobro_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a ActividadCobro entity.
     *
     * @Route("/autocomplete-forms/get-plan", name="ActividadCobro_autocomplete_plan")
     */
    public function getAutocompletePlan() {
        $options = array(
            'repository' => "SistemaGymBundle:Plan",
            'field' => "nombre",
        );
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $term = $request->query->get('q', null);
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->getRequest()->getSession();

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $gimnasio = $session->get('_idGimnasio');

            $qb = $em->createQueryBuilder();
            $qb
                    ->select('p')
                    ->from('Sistema\GymBundle\Entity\Plan', 'p')
                    ->join('p.actividad', 'act')
                    ->join('act.gimnasio', 'gim')
                    ->where("act." . $options['field'] . " LIKE ?1 and p.activo=true and act.activo=true and p.activo=true")
                    ->andWhere('gim.user = :user')
                    ->andWhere('act.gimnasio= :gimn')
                    ->orderby("act." . $options['field'])
                    ->setParameter(1, "%" . $term . "%")
                    ->setParameter('user', $user->getId())
                    ->setParameter('gimn', $gimnasio)
            ;
            $entities = $qb->getQuery()->getResult();
        } else {
            $gimnasio = $user->getPersonal()->getGimnasio()->getId();
            $entities = $em->getRepository('SistemaGymBundle:Plan')->planByidGym($gimnasio);
        }

        $array = array();
        foreach ($entities as $entity) {
            $cantidadCliente = count($entity->getActividadCobros());
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'actividad' => $entity->getActividad()->__toString(),
                'plan' => $entity->getNombre(),
                'cantDias' => $entity->getCantidadDiasSemana(),
                'costo' => $entity->getCosto(),
                'cantidadCliente' => $cantidadCliente
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a ActividadCobro entity.
     *
     * @Route("/autocomplete-forms/get-cuotas", name="ActividadCobro_autocomplete_cuotas")
     */
    public function getAutocompleteCuota() {
        $options = array(
            'repository' => "SistemaGymBundle:Cuota",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a ActividadCobro entity.
     *
     * @Route("/autocomplete-forms/get-asistencias", name="ActividadCobro_autocomplete_asistencias")
     */
    public function getAutocompleteAsistencia() {
        $options = array(
            'repository' => "SistemaGymBundle:Asistencia",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Cuota entity.
     *
     * @Route("/autocomplete-forms/get-actividadCobrobyCliente", name="cuota_autocomplete_cliente_allactividadcobro")
     */
    public function getAutocompleteActividadCobroByCliente() {

        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();

        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $entities = $em->getRepository('SistemaGymBundle:Cuota')
                       ->findCuotasXGym($idGymSession, null, 'impaga', $id, '=');

        $array = array();
        $arrayRes = array();

        foreach ($entities as $entity) {

            $dia = $entity["FechaEmision"]->format('d');
            $mes = $entity["FechaEmision"]->format('m');
            $anio = $entity["FechaEmision"]->format('Y');

            $array[] = array(
                'id' => $entity["id"],
                'actividad' => $entity["planNombre"],
                'cantDias' => $entity["cantidadDiasSemana"],
                'pagada' => $entity['pagada'],
                'diaCobro' => $dia,
                'anio' => $anio,
                'mes' => $mes,
                'costo' => $entity["costo"],
            );
        }

        $arrayRes[] = $array;
        $response = new JsonResponse();
        $response->setData($arrayRes);

        return $response;
    }

        /**
     * Autocomplete a Cuota entity.
     *
     * @Route("/get-cuentaCorriente", name="cuenta_corriente_by_cliente")
     */
    public function getCuentaCorrienteByCliente() {

        $request   = $this->getRequest();
        $em        = $this->getDoctrine()->getManager();

        $idCliente = $request->get('id');
        $resultado = false;

        $session      = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $entities = $em->getRepository('SistemaGymBundle:ActividadCobro')
                       ->findActividadCobroByCliente($idCliente);

        if (!empty($entities)) {

            $array_cuotas_no_pagas = array();

            $options = array(
                'repository' => "SistemaGymBundle:Cuota",
                'field'      => "id",
            );

            $pagosFaltantes = $em->getRepository($options['repository'])
                                 ->queryByIdCliente($idCliente, true);

            if(!empty($pagosFaltantes)){

                foreach ($pagosFaltantes as $key => $pagoFaltante) {

                    $pago_actual = $pagoFaltante->calcularSaldo();

                    $array_cuotas_no_pagas[] = array(
                        'plan'           => $pagoFaltante->getActividadCobro()->getPlan()->getNombre(),
                        'fecha'          => $pagoFaltante->getFecha()->format('d/m/Y'),
                        'pagosFaltantes' => '$ ' . $pago_actual,
                    );
                }
            }else{

                $array_cuotas_no_pagas[] = array(
                    'plan'           => '',
                    'fecha'          => '',
                    'pagosFaltantes' => 'El cliente no adeuda cuotas.',
                );
            }

            $cliente = current($entities);

            $servicioSaldo = $this->get('gym_pagos');

            $array = array(
                'mostrarModal' => true,
                'resultado'    => true,
                'cliente' => $cliente["apellidoCliente"] . ' ' . $cliente["nombreCliente"],
                'foto'    => $this->container->get('templating.helper.assets')
                                             ->getUrl('uploads/' . $cliente["filePath"]),
                'CuotasNoPagas'    => $array_cuotas_no_pagas,
                'actividades'      => $entities,
                'fechaInscripcion' => $cliente['fechaDeIngreso']->format('d/m/Y'),
                'total_deuda'      => $servicioSaldo->calcularSaldoXCliente($idCliente),
                'saldoAFavor'      => $servicioSaldo->calcularSaldoXCliente($idCliente, true),
            );
        } else {

            $array = array(
                'mostrarModal' => false,
                'resultado'    => false,
                'mensaje'      => "El cliente no esta inscripto en una actividad-plan."
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }


    /**
     * Autocomplete a Cuota entity.
     *
     * @Route("/get-actividadCobrobyCliente", name="actividad_cobro_by_cliente")
     */
    public function getActividadCobroByCliente() {
        $user      = $this->getUser();
        $em        = $this->getDoctrine()->getManager();
        $request   = $this->getRequest();
        $idCliente = $request->get('id');
        $resultado = false;
        $mensaje   = null;

        $preferencia = $em->getRepository('SistemaUserBundle:Preferencia')
            ->findPreferenciaByUser($user->getId());

        if ($preferencia->getMostrarAsistencia()) {
            $session = $request->getSession();
            $idGymSession = $session->get('_idGimnasio');
            $entities = $em->getRepository('SistemaGymBundle:ActividadCobro')
                ->findActividadCobroByCliente($idCliente);

            if (!empty($entities)) {
                $cuotasFaltanPagar = $em->getRepository('SistemaGymBundle:Cuota')
                    ->findCuotasXGym($idGymSession, null, false, $idCliente);

                $cliente = current($entities);

                $array = array(
                    'mostrarModal' => true,
                    'resultado'    => true,
                    'actividades'  => $entities,
                    'foto'    => $this->container->get('templating.helper.assets')
                                                 ->getUrl('uploads/' . $cliente["filePath"]),
                    'cliente' => $cliente["apellidoCliente"] . ' ' . $cliente["nombreCliente"],
                    'cantCuotasNoPagas' => count($cuotasFaltanPagar),
                    'idCliente' => $cliente["idCliente"],
                    'rutaCuota' => $this->generateUrl(
                        'admin_cuenta_corriente', array('id_cliente' => $cliente["idCliente"])
                    ),
                );
            } else {
                $array = array(
                    'mostrarModal' => false,
                    'resultado'    => false,
                    'mensaje'      => "El cliente no esta inscripto en una actividad-plan"
                );
            }
        } else {
            $cliente = $em->getRepository('SistemaRRHHBundle:Cliente')->find($idCliente);

            if (!is_null($cliente)) {
                // Generar asistencia nueva. GymBundle/Services/Asistencia
                $asistencia = $this->get('gym_asistencia')->crearAsistencia(null, null, $cliente);
                if (!is_string($asistencia)) {
                    // Genero cuota segun asistencia GymBundle/Services/Cuota
                    $cuota = $this->get('gym_cuota')
                        ->generarCuotaMes($asistencia->getActividadCobro(), $asistencia->getMes(), $asistencia->getAnio());

                    $em->flush();
                    $this->useACL($asistencia, 'create');
                    $this->useACL($cuota, 'create');
                    $resultado = true;
                } else {
                    $mensaje = $asistencia;
                }
            }
            $array = array(
                'mostrarModal' => false,
                'resultado'    => $resultado,
                'mensaje'      => $mensaje
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-clientes", name="ActividadCobro_autocomplete_cliente")
     */
    public function getClientesAction() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->getRequest()->getSession();

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $idGymSession = $session->get('_idGimnasio');
            $entities = $em->getRepository("SistemaRRHHBundle:Cliente")->likeNombreGim($term, $user->getId(), true, $idGymSession);
        } else {
            $idGym = $user->getPersonal()->getGimnasio()->getId();
            $entities = $em->getRepository("SistemaRRHHBundle:Cliente")->getClientesActivos($idGym);
        }

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'fotoPerfil' => $entity->getFilePath()
            );
        }
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_actividadcobro_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /*
     * Creo el formulario seteando los parametros necesarios
     */

    private function instanciarFormularioActividadCobroType($configType) {
        $security = $this->get('security.context');
        $request  = $this->getRequest();
        $session  = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $this->config[$configType] = new ActividadCobroType(
                $security->isGranted('ROLE_SUPER_ADMIN'), 
                $idGymSession
        );
    }
}
