<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\RutinaEjercicio;
use Sistema\GymBundle\Form\RutinaEjercicioType;
use Sistema\GymBundle\Form\RutinaEjercicioFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * RutinaEjercicio controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/rutinaejercicio")
 */
class RutinaEjercicioController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/RutinaEjercicio.yml',
    );

    /**
     * Lists all RutinaEjercicio entities.
     *
     * @Route("/", name="admin_rutinaejercicio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new RutinaEjercicioFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new RutinaEjercicio entity.
     *
     * @Route("/", name="admin_rutinaejercicio_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:RutinaEjercicio:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new RutinaEjercicioType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new RutinaEjercicio entity.
     *
     * @Route("/new", name="admin_rutinaejercicio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new RutinaEjercicioType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a RutinaEjercicio entity.
     *
     * @Route("/{id}", name="admin_rutinaejercicio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing RutinaEjercicio entity.
     *
     * @Route("/{id}/edit", name="admin_rutinaejercicio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new RutinaEjercicioType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing RutinaEjercicio entity.
     *
     * @Route("/{id}", name="admin_rutinaejercicio_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:RutinaEjercicio:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new RutinaEjercicioType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a RutinaEjercicio entity.
     *
     * @Route("/{id}", name="admin_rutinaejercicio_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a RutinaEjercicio entity.
     *
     * @Route("/autocomplete-forms/get-ejercicio", name="RutinaEjercicio_autocomplete_ejercicio")
     */
    public function getAutocompleteEjercicio() {
        $options = array(
            'repository' => "SistemaGymBundle:Ejercicio",
            'field' => "nombre",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository($options['repository'])->createQueryBuilder('a');
        $qb
                ->add('where', "a." . $options['field'] . " LIKE ?1 and a.activo= :estado")
                ->add('orderBy', "a." . $options['field'] . " ASC")
                ->setParameter(1, "%" . $term . "%")
                ->setParameter('estado', true)
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a RutinaEjercicio entity.
     *
     * @Route("/autocomplete-forms/get-combinacion", name="RutinaEjercicio_autocomplete_combinacion")
     */
    public function getAutocompleteCombinacion() {
        $options = array(
            'repository' => "SistemaGymBundle:Combinacion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

}
