<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Ejercicio;
use Sistema\GymBundle\Form\EjercicioType;
use Sistema\GymBundle\Form\EjercicioFilterType;

/**
 * Ejercicio controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/ejercicio")
 */
class EjercicioController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Ejercicio.yml',
    );

    /**
     * Lists all Ejercicio entities.
     *
     * @Route("/", name="admin_ejercicio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new EjercicioFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Ejercicio entity.
     *
     * @Route("/", name="admin_ejercicio_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Ejercicio:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new EjercicioType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Ejercicio entity.
     *
     * @Route("/new", name="admin_ejercicio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new EjercicioType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Ejercicio entity.
     *
     * @Route("/{id}", name="admin_ejercicio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Ejercicio entity.
     *
     * @Route("/{id}/edit", name="admin_ejercicio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new EjercicioType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Ejercicio entity.
     *
     * @Route("/{id}", name="admin_ejercicio_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Ejercicio:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new EjercicioType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Ejercicio entity.
     *
     * @Route("/{id}", name="admin_ejercicio_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }
}