<?php

namespace Sistema\GymBundle\Controller;

use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Cuota;
use Sistema\GymBundle\Entity\PagoCuota;
use Sistema\GymBundle\Entity\Pago;
use Sistema\GymBundle\Entity\ActividadCobro;
use Sistema\GymBundle\Form\CuotaType;
use Sistema\GymBundle\Form\CuotaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Cuota controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/cuota")
 */
class CuotaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Cuota.yml',
    );

    /**
     * Lists all Cuota entities.
     *
     * @Route("/", name="admin_cuota", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $idCliente = $request->query->get('cliente', null);
        $cliente = null;

        if (!is_null($idCliente)) {
            $cliente = $em->getRepository('SistemaRRHHBundle:Cliente')->find($idCliente);
        }
        $defaultData = array('cliente' => $cliente);

        $form = $this->createFormBuilder($defaultData)
                ->add('cliente', 'select2', array(
                    'class' => 'Sistema\RRHHBundle\Entity\Cliente',
                    'url' => 'Cuota_autocomplete_cliente',
                    'configs' => array(
                        'multiple' => false, //required true or false
                        'width' => 'off',
                        'metodo' => 'buscarCliente',
                    ),
                    'attr' => array(
                        'class' => "col-lg-6 col-md-6 col-sm-6 col-xs-6",
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label'
            )))
                ->getForm()
        ;

        return array(
            'config' => $config,
            'form' => $form->createView(),
            'cliente' => $idCliente
        );

    }

    /**
     * Creates a new Cuota entity.
     *
     * @Route("/empleado/", name="admin_cuota_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Cuota:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new CuotaType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
    
        if ($form->isValid()) {

            if($entity->getFecha() < $entity->getActividadCobro()->getFecha()){

                $this->get('session')->getFlashBag()
                     ->add('danger', 'La fecha de la cuota no puede ser menor a la fecha de inscripcion de la actividad.');

                return array(
                    'config' => $config,
                    'entity' => $entity,
                    'form' => $form->createView(),
                ); 
            }
            $em = $this->getDoctrine()->getManager();

            $cuota = $this->get('gym_cuota')->generarCuotaMes(
                $entity->getActividadCobro(), 
                $entity->getFecha()->format('m'), 
                $entity->getFecha()->format('Y'),
                $entity->getCosto()
            );

            if(!$cuota){

                $this->get('session')->getFlashBag()->add('danger', 'No se han generados las cuotas.');

                return array(
                    'config' => $config,
                    'entity' => $cuota,
                    'form' => $form->createView(),
                ); 
            }

            $this->get('gym_pagos')->generarPagosConSaldo($cuota);

            $em->flush();
            
            $this->useACL($cuota, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $cuota->getId()));
            return $this->redirect($nextAction);
        }
        
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Cuota entity.
     *
     * @Route("/empleado/new", name="admin_cuota_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new CuotaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Cuota entity.
     *
     * @Route("/empleado/{id}", name="admin_cuota_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');        

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }

        if ($entity->getGimnasio()->getId() == $idGymSession || $this->isGranted('ROLE_SUPER_ADMIN')) {

            $this->useACL($entity, 'show');
            $deleteForm = $this->createDeleteForm($config, $id);

            return array(
                'config'      => $config,
                'entity'      => $entity,
                'delete_form' => $deleteForm->createView(),
            );            
        }else{

            throw $this->createAccessDeniedException('Esta cuota no pertenece a este gimnasio');
        }        
    }

/*    *
     * Displays a form to edit an existing Cuota entity.
     *
     * @Route("/{id}/edit", name="admin_cuota_edit")
     * @Method("GET")
     * @Template()
     
    public function editAction($id) {
        $this->config['editType'] = new CuotaType();
        $response = parent::editAction($id);

        return $response;
    }
*/
    /**
     * Edits an existing Cuota entity.
     *
     * @Route("/{id}", name="admin_cuota_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Cuota:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new CuotaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Cuota entity.
     *
     * @Route("/{id}", name="admin_cuota_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Cuota entity.
     *
     * @Route("/autocomplete-table/get-cuotaByActividadCobro", name="Cuota_autocomplete_cuota_byActividadCobro")
     */
    public function getAutocompleteActividadCobroByCliente() {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();

        $entities = $entity = $em->getRepository('Sistema\GymBundle\Entity\ActividadCobro')->find($id);

        $array = array();

        foreach ($entities->getCuotas() as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'dia' => $entity->getFecha()->format("d"),
                'mes' => $entity->getFecha()->format("m"),
                'anio' => $entity->getFecha()->format("Y"),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/cuota-vencidas", name="cuotas_vencidas")
     */
    public function getAllCuotasVencidas(){

        $request = $this->getRequest();
        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $em = $this->getDoctrine()->getManager();

        $preferenciasUsuario = $this->container->get('security.token_storage')->getToken()->getUser()->getPreferencias();

        $intervalo = $preferenciasUsuario->getIntervaloDeVencimiento();        
        
        $cuotasVencidas = $em->getRepository('SistemaGymBundle:Cuota')
                             ->findCuotasXGym($idGymSession, null, 'vencida', null, null, $intervalo);
        $array = array();

        foreach ($cuotasVencidas as $entity) {
            $deuda = 
                str_replace('$','',$entity["costo"]) - str_replace('$','',$entity["saldo"]);
            $array[] = array(
                'cliente' => "<a  href='javascript:;' title='Ver Perfil' onclick='verPerfilCompleto(" . $entity["idCliente"] . ")' ><i class='glyphicon glyphicon-user tooltips'></i></a> &nbsp; &nbsp;" . $entity["apellidoCliente"] . "&nbsp;" . $entity["nombreCliente"],
                'actividad' => $entity["planNombre"] . "&nbsp;-&nbsp;Dias semanales:  &nbsp;" . $entity["cantidadDiasSemana"],
                'fecha' => $entity["FechaEmision"]->format("d\/m\/Y"),
                'costo' => $entity["costo"],
                'saldo' => $entity["saldo"],
                'accion' => "
                <a class='glyphicon glyphicon-usd tooltips' 
                    onClick=" . '"' . "cargarDatoModal(" . $entity["id"] . ",'$" .  $deuda . "');
                        " . '"' . " 
                    href='javascript:;' 
                    title='Pagar'>
                </a>",
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-clientes-all", name="Cuota_autocomplete_cliente")
     */
    public function getAllClientesAction() {

        $term = $this->getRequest()->query->get('q', null);
        $em = $this->getDoctrine()->getManager();
        $security = $this->get('security.context');
        $request = $this->getRequest();
        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');        
        $user = $security->getToken()->getUser();

        //Entra si NO ES superadmin. trae usuarios activos e inactivos.
        if ($security->isGranted('ROLE_SUPER_ADMIN')) {

            $entities = $em->getRepository("SistemaRRHHBundle:Cliente")
                           ->likeNombreGim($term, null, null, null);                    
        } else {

            if($security->isGranted('ROLE_ADMIN')){

                $entities = $em->getRepository("SistemaRRHHBundle:Cliente")
                               ->likeNombreGim($term, $user->getId(), null, $idGymSession);
            }else{

                $entities = $em->getRepository("SistemaRRHHBundle:Cliente")
                               ->likeNombreGim($term, null, null, $idGymSession);                
            }
        }

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'fotoPerfil' => $entity->getFilePath()
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Pago entity.
     *
     * @Route("/autocomplete-forms/get-actividad-cliente", name="Cuota_autocomplete_actividad_cliente")
     */
    public function getAutocompleteActividadCliente() {
        $options = array(
            'repository' => "SistemaGymBundle:ActividadCobro",
            'field' => "id",
        );
        //AGREGO PARA PISAR EL QUERYBUILDER
        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $term = $this->getRequest()->get('q');
        $em = $this->getDoctrine()->getManager();
        //true busca los clientes activos
        $qb = $em->getRepository($options['repository'])
                ->queryActividadCobroByGimnasioLikeCliente($idGymSession, true, $term);



        //AGREGO PARA PISAR EL QUERYBUILDER
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        if (is_null($qb)) {
            $em = $this->getDoctrine()->getManager();

            $qb = $em->getRepository($options['repository'])->createQueryBuilder('a');
            $qb
                    ->where("a." . $options['field'] . " LIKE :term")
                    ->orderBy("a." . $options['field'], "ASC")
            ;
        }

        $qb->setParameter("term", "%" . $term . "%");

        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'costo' => $entity->getPlan()->getCosto()
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/listado-cobros/", name="listado_cobros")
     * @Template("SistemaGymBundle:Cuota:cobros.html.twig")
     */
    public function listadoCobros() {

        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('Sistema\GymBundle\Entity\Pago')
                ->findAll()
        ;
        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/cuota-pagar", name="cuota_pagar")
     * @Method("POST")
     */
    public function pagarCuotas() {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $arrayId = $request->request->get('id', null);
        $countArrayId = count($arrayId);
        $resultado = false;
        $contador = NULL;


        if ($countArrayId > 0) {
            $arrayCuota = array();

            // Fin creacion de pago.
            for ($i = 0; $i < $countArrayId; $i++) {
                if (!empty($arrayId[$i])) {
                    $cuota = $em->getRepository('SistemaGymBundle:Cuota')->find($arrayId[$i]);

                    if (!is_null($cuota)) {
                        $arrayCuota[] = $cuota;
                        $contador ++;
                    }
                }
            }
            if ($arrayCuota > 0) {
                $this->get('gym_pagos')->generarPagosPorCuota($arrayCuota);

                $resultado = true;
                //Session y actualizo la cantidad de cuotas
                $session = $request->getSession();

                $cantCuotas = $session->get('countCuotas') - $contador;
                $session->set('countCuotas', $cantCuotas);
            }
        }

        $array = array();

        $array[] = array(
            'resultado' => $resultado,
            'contador' => $contador,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }


    /**
     * @Route("/cuotas-no-pagas-todos", name="cuotas_no_pagas_todos_cliente")
     */
    public function getCuotasNoPagasAllClientes() {
        $request = $this->getRequest();

        $mes = $request->request->get('mes', null);
        $signo_fecha = $request->request->get('signo_fecha', "=");


        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $cuotasFaltanPagar = $em->getRepository('SistemaGymBundle:Cuota')
                                ->findCuotasXGym($idGymSession, $mes, 'impaga', null, $signo_fecha);

        //Actualizo cuotas en session
        $countCuotas = count($cuotasFaltanPagar);
        $session->set('countCuotas', $countCuotas);
        //Fin Actualizo cuotas en session
        $array = array();
        foreach ($cuotasFaltanPagar as $entity) {
            $deuda = 
                str_replace('$','',$entity["costo"]) - str_replace('$','',$entity["saldo"]);
            $array[] = array(
                'cliente' => "<a  href='javascript:;' title='Ver Perfil' onclick='verPerfilCompleto(" . $entity["idCliente"] . ")' ><i class='glyphicon glyphicon-user tooltips'></i></a> &nbsp; &nbsp;" . $entity["apellidoCliente"] . "&nbsp;" . $entity["nombreCliente"],
                'actividad' => $entity["planNombre"] . "&nbsp;-&nbsp;Dias semanales:  &nbsp;" . $entity["cantidadDiasSemana"],
                'fecha' => $entity["FechaEmision"]->format("d\/m\/Y"),
                'costo' => $entity["costo"],
                'saldo' => $entity["saldo"],
                'accion' => "
                <a class='glyphicon glyphicon-usd tooltips' 
                    onClick=" . '"' . "cargarDatoModal(" . $entity["id"] . ",'$" .  $deuda . "');
                        " . '"' . " 
                    href='javascript:;' 
                    title='Pagar'>
                </a>",
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/cuota-eliminar", name="Cuotas_eliminar")
     * @Method("POST")
     */
    public function eliminarCuota() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $idCuota = $request->request->get('id', null);
        $cuota = $em->getRepository('SistemaGymBundle:Cuota')->find($idCuota);
        $resultado = false;
        if (!is_null($cuota)) {
            $em->remove($cuota);
            $em->flush();
            $resultado = true;
        }
        $array = array();

        $session = $request->getSession();
        $user = $this->getUser();

        $cantCuotas = $session->get('countCuotas') + 1;
        $session->set('countCuotas', $cantCuotas);

        $array["resultado"] = $resultado;
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/cuenta-corriente/{id_cliente}", name="admin_cuenta_corriente")
     * @Method("GET")
     * @Template()
     */
    public function cuentaCorriente($id_cliente) {

        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $cliente = $em->getRepository('SistemaRRHHBundle:Cliente')->find($id_cliente);

        if($cliente->getGimnasio()->getId() != $idGymSession){

            throw $this->createAccessDeniedException('Este cliente no pertenece a este gimnasio');
        }

        $saldo = $this->get('gym_pagos')->calcularSaldoXCliente($cliente, true);
        $deuda = $this->get('gym_pagos')->calcularSaldoXCliente($cliente);

        $cuotas = array();
        $cuotas = $em->getRepository('SistemaGymBundle:Cuota')
                     ->findAllCuotasByCliente($id_cliente);

        return array(
            'cuotas'  => $cuotas,
            'cliente' => $cliente,
            'deuda'   => $deuda,
            'saldo'   => $saldo
        );
    }

    /**
     * @Route("/cuota-pagas-by-cliente", name="cuotas_pagas_por_cliente")
     * @Method("POST")
     */
    public function cuotasPagasByCliente() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        $idCliente = $request->request->get('id', null);

        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $cuotas = $em->getRepository('SistemaGymBundle:Pago')
                ->findPagosCuotasByGymCliente($idGymSession, $idCliente);
        $array = array();
        if (!empty($cuotas)) {
            
            foreach ($cuotas as $entity) {
                $url = $this->generateUrl('admin_cuota_show', array('id' => $entity['cuotaId']));
            $deuda = 
                str_replace('$','',$entity["cuotaCosto"]) - str_replace('$','',$entity["saldo"]);
                $array[] = array(
                    'fecha' => $entity["cuotaFecha"],
                    'fechaPago' => $entity["pagoFecha"],
                    'actividad' => $entity["actividadNombre"] . "&nbsp;-&nbsp;Plan:&nbsp" . $entity["planNombre"] . "&nbsp;-&nbsp;Dias semanales:  &nbsp;" . $entity["cantidadDiasSemana"],
                    'costo' => $entity["cuotaCosto"],
                    'saldo' => $entity["saldo"],
                    'pagada' => $entity["cuotaPagada"],
                    'accion' => "
                    <a class='glyphicon glyphicon-usd tooltips' 
                        onClick=" . '"' . "cargarDatoModal(" . $entity["id"] . ",'$" .  $deuda . "');
                            " . '"' . " 
                        href='javascript:;' 
                        title='Pagar'>
                    </a>",
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Controla si tiene cuota.
     * Si la cuota no existe la crea en el mes.
     *
     * @Route("/generar-cuotas-mes", name="admin_generar_cuotas_mes")
     */
    public function generarCuotasMes() {
        $request = $this->getRequest();

        $mes = $request->request->get('mes', null);
        $em = $this->getDoctrine()->getManager();

        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $clientes = $em->getRepository('SistemaRRHHBundle:Cliente')
                ->getClientesActivos($idGymSession);

        $asistencias = array();
        $cuotas = array();

        $fechaNow = new \DateTime("now");
        $anio = $fechaNow->format('Y');

        //Obtengo GymBundle/Services/Cuota
        $serviceCuota = $this->get('gym_cuota');

        foreach ($clientes as $cliente) {
            $actividadesCobros = $cliente->getActividadCobros();
            if ($actividadesCobros->count() > 0) {
                foreach ($actividadesCobros as $actividadCobro) {
                    $crearCuota = false;
                    $fechaActCobro = $actividadCobro->getFecha();
                    if ($fechaActCobro->format('Y') < $anio) {

                        //Entra si el anio es menor
                        $crearCuota = true;
                    } elseif ($fechaActCobro->format('Y') == $anio && $fechaActCobro->format('m') <= $mes) {

                        //Entra si el anio es igual y el mes es menor o igual
                        $crearCuota = true;
                    }
                    if ($crearCuota) {
                        //Utilizo el Service para crear la cuota
                        $cuota = $serviceCuota->generarCuotaMes($actividadCobro, $mes, $anio); 

                        //Recorro para ACL
                        if($cuota){

                            $this->get('gym_pagos')->generarPagosConSaldo($cuota);    
                        }
                        
                        array_push($cuotas, $cuota);
                    }
                }
            }
        }

        $em->flush();

        //Recorro para ACL y para controlar las cuotas
        foreach ($cuotas as $cuota) {
            $this->useACL($cuota, 'create'); //Seteo ACL
        }

        $response = new JsonResponse();
        $response->setData(true);

        return $response;
    }

}
