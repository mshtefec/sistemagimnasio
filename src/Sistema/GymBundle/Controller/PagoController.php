<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Pago;
use Sistema\GymBundle\Form\PagoFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Pago controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/pago")
 */
class PagoController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Pago.yml',
    );

    /**
     * Lists all Pago entities.
     *
     * @Route("/", name="admin_pago")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new PagoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
                ->createQueryBuilder('a')
                ->join("a.detalles", "detalles")
                ->join("detalles.cuota", "cuota")
                ->join("cuota.gimnasio", "gimnasio")
                ->Where('gimnasio.id = :idGimnasio')
                ->andWhere('a.monto > 0')
                ->setParameter('idGimnasio', $idGymSession)
                ->orderBy('a.id', 'DESC')
        ;
        return $queryBuilder;
    }

    /**
     * Creates a new Pago entity.
     *
     * @Route("/empleado/", name="admin_pago_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Pago:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = $this->get('app_form_pago');
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            
            if($this->get('gym_pagos')->calcularPagos($entity->getCliente()->getId(), $entity)) {

                $this->get('session')->getFlashBag()
                     ->add('danger', 'Ha ocurrido un error con el pago. Asegurese de haber puesto un monto correcto, y que el cliente adeude alguna cuota.');

                unset($config['newType']);

                return array(
                    'config' => $config,
                    'entity' => $entity,
                    'form' => $form->createView(),
                );
            }

            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            if (!array_key_exists('saveAndAdd', $config)) {
                $config['saveAndAdd'] = true;
            } elseif ($config['saveAndAdd'] != false) {
                $config['saveAndAdd'] = true;
            }

            if ($config['saveAndAdd']) {
                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            } else {
                $nextAction = $this->generateUrl($config['show'], array('id' => $entity->getId()));
            }

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

// remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Pago entity.
     *
     * @Route("/empleado/new", name="admin_pago_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = $this->get('app_form_pago');
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Pago entity.
     *
     * @Route("/empleado/{id}", name="admin_pago_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {

        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $request = $this->getRequest();
        $session = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');        

        $user = $this->getUser();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {

            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        
        if ($entity->getUser()->getId() == $user->getId() ||
            $entity->getCliente()->getGimnasio()->getUser()->getId() == $user->getId() ||
            $this->isGranted('ROLE_SUPER_ADMIN')) {

            $this->useACL($entity, 'show');

            $deleteForm = $this->createDeleteForm($config, $id);

            return array(
                'config'      => $config,
                'entity'      => $entity,
                'delete_form' => $deleteForm->createView(),
            );
        }else{

            throw $this->createAccessDeniedException('Este pago no ha sido creado por ud.');
        }
    }

    /**
     * Displays a form to edit an existing Pago entity.
     *
     * @Route("/{id}/edit", name="admin_pago_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {

        $this->config['editType'] = $this->get('app_form_pago');
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Pago entity.
     *
     * @Route("/{id}", name="admin_pago_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Pago:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = $this->get('app_form_pago');
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            if (!array_key_exists('saveAndAdd', $config)) {
                $config['saveAndAdd'] = true;
            } elseif ($config['saveAndAdd'] != false) {
                $config['saveAndAdd'] = true;
            }
            if ($config['saveAndAdd']) {
                $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            } else {
                $nextAction = $this->generateUrl($config['show'], array('id' => $id));
            }
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

// remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Pago entity.
     *
     * @Route("/{id}", name="admin_pago_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $form = $this->createDeleteForm($config, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($config['repository'])->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }

            foreach ($entity->getDetalles() as $detalle) {

                $detalle->getCuota()->setPagada(false);

                $em->persist($detalle);
            }
            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl($config['index']));
    }   

    /**
     * Exporter Pago.
     *
     * @Route("/exporter/{format}", name="admin_pago_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Pago entity.
     *
     * @Route("/autocomplete-forms/get-cuotas", name="Pago_autocomplete_cuotas")
     */
    public function getAutocompleteCuota() {
        $options = array(
            'repository' => "SistemaGymBundle:Cuota",
            'field' => "id",
        );
//AGREGO PARA PISAR EL QUERYBUILDER

        $session = $this->getRequest()->getSession();
        $cliente = $session->get('selectPago_cliente');

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository($options['repository'])->queryByIdCliente($cliente, true);

//AGREGO PARA PISAR EL QUERYBUILDER

        $response = parent::getAutocompleteFormsMwsAction($options, $qb);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-clientes-all", name="Pago_autocomplete_cliente")
     */
    public function getClientesAction() {
        $term = $this->getRequest()->query->get('q', null);
        $em = $this->getDoctrine()->getManager();
        //$session = $this->getRequest()->getSession();
        //$idGymSession = $session->get('_idGimnasio');
        //trae usuarios del gimnasio y que esten activos
        $user = $this->getUser()->getId();

        $entities = $em->getRepository("SistemaRRHHBundle:Cliente")
                ->likeNombreGim($term, $user, true, null);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
                'fotoPerfil' => $this->container->get('templating.helper.assets')->getUrl('uploads/' . $entity->getFilePath())
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/empleado/pagos-detalles-by-cliente/{cliente}", name="Pago_create_pagos_detalle", options={"expose"=true})
     */
    public function getcuotasSaldoByClienteAction($cliente) {

        $em = $this->getDoctrine()->getManager();
        $options = array(
            'repository' => "SistemaGymBundle:Cuota",
            'field' => "id",
        );
        
        $respuesta['Cuotas']     = array();
        $respuesta['saldoFavor'] = array();

        $respuesta['saldoFavor'] = $this->get('gym_pagos')->calcularSaldoXCliente($cliente, true);
        $respuesta['totalSaldo'] = $this->get('gym_pagos')->calcularSaldoXCliente($cliente);
        
        $entities = $em->getRepository($options['repository'])
                       ->queryByIdCliente($cliente, true);

        $array = array();

        $saldoTotal = 0;

        foreach ($entities as $entity) {

            // $saldoTotal += $entity->calcularSaldo();

            $array[] = array(

                'id' => $entity->getId(),
                'text' => 'Fecha: ' . $entity->getFecha()->format('d/m/Y') . 
                      ' | Costo: $' . $entity->calcularSaldo(),
            );

            // $respuesta['totalSaldo'] = $saldoTotal;
        }

        $respuesta['Cuotas'] = $array;
        
        $response = new JsonResponse();
        $response->setData($respuesta);

        return $response;
    }

    /**
     * Pagos por dia.
     *
     * @Route("/listado/pagos/por/dia", name="admin_pagos_dia")
     * @Method({"GET", "POST"})     
     * @Template()
     */
    public function pagosPorDiaAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $session      = $request->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $fecha = null;

        $form = $this->createFormBuilder()
            ->add('fecha_dia', 'date', array(
                'label'  => 'Seleccionar dia de pagos',
                'data'   => new \DateTime('Today')
            ))
            ->add('save', 'submit', array(
                'label' => 'Mostrar pagos',
                'attr'  => array(
                    'class' => 'btn-success'
                ))
            )
            ->getForm()
        ;

        $form->handleRequest($request);   

        if ($form->isValid()) {
            
            $data  = $form->getData()['fecha_dia'];
            $fecha = $data->format('d/m/Y');
        }

        $pagos = $em->getRepository('SistemaGymBundle:Pago')
                    ->getPagosXdiaByGym($idGymSession, $fecha)
        ;

        return array(
            'pagos' => $pagos,
            'form'  => $form->createView()
        );
    }
}
