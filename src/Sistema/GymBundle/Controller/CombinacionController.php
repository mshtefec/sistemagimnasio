<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Combinacion;
use Sistema\GymBundle\Form\CombinacionType;
use Sistema\GymBundle\Form\CombinacionFilterType;

/**
 * Combinacion controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/combinacion")
 */
class CombinacionController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Combinacion.yml',
    );

    /**
     * Lists all Combinacion entities.
     *
     * @Route("/", name="admin_combinacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new CombinacionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Combinacion entity.
     *
     * @Route("/", name="admin_combinacion_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Combinacion:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new CombinacionType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Combinacion entity.
     *
     * @Route("/new", name="admin_combinacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new CombinacionType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Combinacion entity.
     *
     * @Route("/{id}", name="admin_combinacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Combinacion entity.
     *
     * @Route("/{id}/edit", name="admin_combinacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new CombinacionType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Combinacion entity.
     *
     * @Route("/{id}", name="admin_combinacion_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Combinacion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new CombinacionType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Combinacion entity.
     *
     * @Route("/{id}", name="admin_combinacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Combinacion entity.
     *
     * @Route("/autocomplete-forms/get-rutinaDias", name="Combinacion_autocomplete_rutinaDias")
     */
    public function getAutocompleteRutinaDias()
    {
        $options = array(
            'repository' => "SistemaGymBundle:RutinaDias",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
    /**
     * Autocomplete a Combinacion entity.
     *
     * @Route("/autocomplete-forms/get-rutinaEjercicio", name="Combinacion_autocomplete_rutinaEjercicio")
     */
    public function getAutocompleteRutinaEjercicio()
    {
        $options = array(
            'repository' => "SistemaGymBundle:RutinaEjercicio",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}