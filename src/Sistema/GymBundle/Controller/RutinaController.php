<?php

namespace Sistema\GymBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Rutina;
use Sistema\GymBundle\Form\RutinaType;
use Sistema\GymBundle\Form\RutinaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\GymBundle\Form\CombinacionType;
use Sistema\GymBundle\Form\EjercicioType;
use Sistema\GymBundle\Entity\Combinacion;
use Sistema\GymBundle\Entity\Ejercicio;

/**
 * Rutina controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/rutina")
 */
class RutinaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Rutina.yml',
    );

    /**
     * Lists all Rutina entities.
     *
     * @Route("/", name="admin_rutina")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new RutinaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $user = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
                ->createQueryBuilder('a')
                ->join('a.user', 'user')
                ->where('user.id = :u')
                ->setParameter('u', $user)
                ->orderBy('a.id', 'DESC')
        ;

        return $queryBuilder;
    }

    /**
     * Creates a new Rutina entity.
     *
     * @Route("/", name="admin_rutina_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Rutina:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new RutinaType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();

        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $entity->setUser($user);
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Rutina entity.
     *
     * @Route("/new", name="admin_rutina_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new RutinaType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        //$em = $this->getDoctrine()->getManager();
        //$combinaciones = $em->getRepository('SistemaGymBundle:Combinacion')->findAll();
        // remove the form to return to the view
        unset($config['newType']);
        $ejercicio = new Ejercicio();
        $combinacion = new Combinacion();
        $formCombinacion = $this->crearFormulario(new CombinacionType(), $combinacion, 'admin_combinacion_create', 'formCombinacion');
        $formEjercicio = $this->crearFormulario(new EjercicioType(), $ejercicio, 'admin_ejercicio_create', 'formEjercicio');

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
            //'combinacion' => $combinaciones,
            'formCombinacion' => $formCombinacion->createView(),
            'formEjercicio' => $formEjercicio->createView()
        );
    }

    /**
     * Finds and displays a Rutina entity.
     *
     * @Route("/{id}", name="admin_rutina_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Rutina entity.
     *
     * @Route("/{id}/edit", name="admin_rutina_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new RutinaType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);
        $ejercicio = new Ejercicio();
        $combinacion = new Combinacion();
        $formCombinacion = $this->crearFormulario(new CombinacionType(), $combinacion, 'admin_combinacion_create', 'formCombinacion');
        $formEjercicio = $this->crearFormulario(new EjercicioType(), $ejercicio, 'admin_ejercicio_create', 'formEjercicio');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'formCombinacion' => $formCombinacion->createView(),
            'formEjercicio' => $formEjercicio->createView()
        );
    }

    /**
     * Edits an existing Rutina entity.
     *
     * @Route("/{id}", name="admin_rutina_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Rutina:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new RutinaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Rutina entity.
     *
     * @Route("/{id}", name="admin_rutina_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Rutina entity.
     *
     * @Route("/autocomplete-forms/get-clientes", name="Rutina_autocomplete_clientes")
     */
    public function getAutocompleteCliente() {
        $options = array(
            'repository' => "SistemaRRHHBundle:Cliente",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Rutina entity.
     *
     * @Route("/autocomplete-forms/get-rutinaDias", name="Rutina_autocomplete_rutinaDias")
     */
    public function getAutocompleteRutinaDias() {
        $options = array(
            'repository' => "SistemaGymBundle:RutinaDias",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a RutinaCliente entity.
     *
     * @Route("/autocomplete/get-rutina", name="Rutina_autocomplete_rutinaById")
     */
    public function getAutocompleteRutina() {
        $request = $this->getRequest();
        $rutinaId = $request->request->get('id', null);

        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $rutinaCliente = $em->getRepository('SistemaGymBundle:RutinaCliente')->findBy(
                array('rutina' => $rutinaId)
        );

        if (empty($rutinaCliente)) {
            $entity = $em->getRepository('SistemaGymBundle:Rutina')->find($rutinaId);
        } else {
            //obtengo rutina
            $entity = current($rutinaCliente)->getRutina();
        }

        //busco todos los clientes que no esten en la rutina
        $clientes = $em->getRepository('SistemaGymBundle:RutinaCliente')->findClientesNotInRutina($rutinaId, $idGymSession);

        $rutinaArray = $this->createArrayRutina($entity, $rutinaCliente);
        $clientesArray = array();
        foreach ($clientes as $cliente) {
            $cli = array(
                'id' => $cliente->getId(),
                'text' => $cliente->__toString()
            );

            array_push($clientesArray, $cli);
        }

        $array = array();
        $array["rutina"] = $rutinaArray;
        $array["clientes"] = $clientesArray;

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    private function crearFormulario($formType, $entity, $ruta, $id = "form") {
        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($ruta),
            'method' => 'POST',
            'attr' => array('id' => $id)
        ));

        return $form;
    }

    /**
     * Autocomplete a Rutina entity.
     *
     * @Route("/rutina/previsualizar", name="Rutina_previsualizar")
     */
    public function getRutinaPrevisualizar() {
        $request = $this->getRequest();

        $entity = new Rutina();
        $form = $this->createForm(new RutinaType(), $entity, array(
            'attr' => ['id' => 'formuarioRutina'],
            'action' => 'Rutina_previsualizar',
            'method' => 'POST',
        ));
        $form->handleRequest($request);

        $rutinaArray = $this->createArrayRutina($entity, null);

        $response = new JsonResponse();
        $response->setData($rutinaArray);

        return $response;
    }

    private function createArrayRutina($entity, $rutinaClientes) {
        $rutinaArray = array();
        $rutinaDiasArray = array();
        $rutinaArray["cantidadSemana"] = $entity->getCantidadSemanas();
        $rutinaArray["nombre"] = $entity->getNombre();
        //creo array de rutinadias
        foreach ($entity->getRutinaDias() as $rutinaDia) {
            $rutinaDiaArray = array();
            $combinacionesArray = array();
            $rutinaDiaArray["dia"] = $rutinaDia->getDia();
            //creo array de combinacion
            foreach ($rutinaDia->getCombinacion() as $combinacion) {
                $combinacionArray = array();
                $rutinaEjercicioArray = array();
                $combinacionArray["cantidadVueltas"] = $combinacion->getCantidadVueltas();
                //creo array de rutina Ejercicio
                foreach ($combinacion->getRutinaEjercicios() as $rutinaEjercicio) {
                    $rutinaEjercicioRenglonArray = array();
                    $rutinaEjercicioRenglonArray["ejercicio"] = $rutinaEjercicio->getEjercicio() . ' - ' . $rutinaEjercicio->getSerie();

                    array_push($rutinaEjercicioArray, $rutinaEjercicioRenglonArray);
                }
                $combinacionArray["rutinaEjercicios"] = $rutinaEjercicioArray;
                array_push($combinacionesArray, $combinacionArray);
            }
            $rutinaDiaArray["combinaciones"] = $combinacionesArray;
            array_push($rutinaDiasArray, $rutinaDiaArray);
        }
        $rutinaArray["rutinaDias"] = $rutinaDiasArray;
        $ClienteInRutina = array();
        //clientes que estan en la rutina
        foreach ($rutinaClientes as $rutinaCliente) {
            $cliente = array();
            $cliente['cliente'] = $rutinaCliente->getCliente()->__toString();
            array_push($ClienteInRutina, $cliente);
        }
        $rutinaArray["clientes"] = $ClienteInRutina;

        return $rutinaArray;
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_rutina_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
