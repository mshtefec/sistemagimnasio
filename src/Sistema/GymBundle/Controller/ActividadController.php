<?php

namespace Sistema\GymBundle\Controller;

use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\GymBundle\Entity\Actividad;
use Sistema\GymBundle\Entity\Plan;
use Sistema\GymBundle\Entity\Horario;
use Sistema\GymBundle\Form\ActividadType;
use Sistema\GymBundle\Form\ActividadFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Actividad controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/actividad")
 */
class ActividadController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/GymBundle/Resources/config/Actividad.yml',
    );

    /**
     * Lists all Actividad entities.
     *
     * @Route("/", name="admin_actividad")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ActividadFilterType();
        $config = $this->getConfig();
        
        if (!$this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            unset($config['fieldsindex']['a.gimnasio']);
        }

        list($filterForm, $queryBuilder) = $this->filter($config);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );
        //remove the form to return to the view
        unset($config['filterType']);

        return array(
            'config' => $config,
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $qb = $em->createQueryBuilder();
        $qb
                ->select('a')
                ->from('Sistema\GymBundle\Entity\Actividad', 'a')
                ->join('a.gimnasio', 'gim')
        ;
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $session = $this->getRequest()->getSession();
            $idGymSession = $session->get('_idGimnasio');
            $qb
                    ->setParameter("gimId", $idGymSession)
                    ->where("gim.id = :gimId")
            ;
        }
        $qb->orderby("a.id");

        return $qb;
    }

    /**
     * Creates a new Actividad entity.
     *
     * @Route("/", name="admin_actividad_create")
     * @Method("POST")
     * @Template("SistemaGymBundle:Actividad:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ActividadType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
                
        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        if($idGymSession == 0){

            $this->get('session')->getFlashBag()->add('danger', 'Ud. no ha creado ningun gimnasio para gestionar actividades.');

            unset($config['newType']);

            return array(
                'config' => $config,
                'entity' => $entity,
                'form' => $form->createView(),
            );
        }

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $gimnasio = $em->getRepository('SistemaGymBundle:Gimnasio')->find($idGymSession);
            $entity->setGimnasio($gimnasio);
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Actividad entity.
     *
     * @Route("/new", name="admin_actividad_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ActividadType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $plan = new Plan();
        $horario = new Horario();
        $entity->addPlane($plan);
        $entity->addHorario($horario);
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Actividad entity.
     *
     * @Route("/{id}", name="admin_actividad_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Actividad entity.
     *
     * @Route("/{id}/edit", name="admin_actividad_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ActividadType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Creates a form to delete a Post entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($config, $id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.disableConfirm', array(), 'MWSimpleAdminCrudBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($config['delete'], array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleAdminCrudBundle',
                            'label' => 'views.recordactions.disable',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * Edits an existing Actividad entity.
     *
     * @Route("/{id}", name="admin_actividad_update")
     * @Method("PUT")
     * @Template("SistemaGymBundle:Actividad:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ActividadType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Actividad entity.
     *
     * @Route("/{id}", name="admin_actividad_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $form = $this->createDeleteForm($config, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($config['repository'])->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }

            $entity->setActivo(false);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Elemento deshabilitado satifactoriamente');
        }

        return $this->redirect($this->generateUrl($config['index']));
    }

    /**
     * Autocomplete a Actividad entity.
     *
     * @Route("/autocomplete-forms/get-horarios", name="Actividad_autocomplete_horarios")
     */
    public function getAutocompleteHorario() {
        $options = array(
            'repository' => "SistemaGymBundle:Horario",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Actividad entity.
     *
     * @Route("/autocomplete-forms/get-planes", name="Actividad_autocomplete_planes")
     */
    public function getAutocompletePlan() {
        $options = array(
            'repository' => "SistemaGymBundle:Plan",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    protected function useACL($entity, $action) {
        $aclConf = $this->container->hasParameter('mw_simple_admin_crud.acl') ?
                $this->container->getParameter('mw_simple_admin_crud.acl') : null;

        if ($aclConf['use']) {
            if ($this->isInstanceOf($entity, $aclConf['entities'])) {
                $aclManager = $this->container->get('mws_acl_manager');
                switch ($action) {
                    case 'create':
                        $aclManager->createACL($entity);
                        break;
                    case 'show':
                        //controlo si es rol empleado entra
                        $securityContext = $this->container->get('security.context');
                        if ($securityContext->isGranted('ROLE_ADMIN')) {
                            $aclManager->controlACL($entity, 'VIEW', $aclConf['exclude_role']);
                        } elseif ($securityContext->isGranted('ROLE_EMPLEADO')) {
                            $this->controlGimnasio($entity);
                        } else {
                            $aclManager->controlACL($entity, 'VIEW', $aclConf['exclude_role']);
                        }
                        break;
                    case 'edit':
                        //controlo si es rol empleado entra
                        $securityContext = $this->container->get('security.context');
                        if ($securityContext->isGranted('ROLE_ADMIN')) {
                            $aclManager->controlACL($entity, 'EDIT', $aclConf['exclude_role']);
                        } elseif ($securityContext->isGranted('ROLE_EMPLEADO')) {
                            $this->controlGimnasio($entity);
                        } else {
                            $aclManager->controlACL($entity, 'EDIT', $aclConf['exclude_role']);
                        }
                        break;
                    case 'update':
                        //controlo si es rol empleado entra
                        $securityContext = $this->container->get('security.context');
                        if ($securityContext->isGranted('ROLE_ADMIN')) {
                            $aclManager->controlACL($entity, 'EDIT', $aclConf['exclude_role']);
                        } elseif ($securityContext->isGranted('ROLE_EMPLEADO')) {
                            $this->controlGimnasio($entity);
                        } else {
                            $aclManager->controlACL($entity, 'EDIT', $aclConf['exclude_role']);
                        }
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }
    }

    protected function controlGimnasio($entity) {
        //obtengo usuario
        $user = $this->getUser();
        if ($user->getPersonal()->getGimnasio()->getId() != $entity->getGimnasio()->getId()) {
            throw new AccessDeniedException();
        }
    }

    /**
     * Autocomplete a ActividadCobro entity.
     *
     * @Route("/autocomplete-forms/get-gimnasio", name="Actividad_autocomplete_gimnasio")
     */
    public function getAutocompleteAsistencia() {
        $options = array(
            'repository' => "SistemaGymBundle:Gimnasio",
            'field' => "nombre",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb
                ->select('gim')
                ->from('Sistema\GymBundle\Entity\Gimnasio', 'gim')
                ->Where('gim.user= :user')
                ->andWhere('gim.activo=true')
                ->setParameter('user', $user->getId())
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_actividad_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
