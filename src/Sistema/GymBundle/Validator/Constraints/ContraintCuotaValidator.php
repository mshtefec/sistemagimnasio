<?php

namespace Sistema\GymBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContraintCuotaValidator extends ConstraintValidator {

    public function validate($cuota, Constraint $constraint) {

        $cuotas = $cuota->getActividadCobro()->getCuotas();
        $idActividadCobro = $cuota->getActividadCobro()->getId();
        $fechaBuscar = $cuota->getFecha()->format('m-Y');

        $cuotaEncontrada = $cuotas->filter(
                function($cuota) use ($fechaBuscar, $idActividadCobro) {
            if ($fechaBuscar == $cuota->getFecha()->format('m-Y') && $idActividadCobro == $cuota->getActividadCobro()->getId())
                return $cuota;
        }
        );
        if (count($cuotaEncontrada) > 0) {
            $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', "La actividad ya tiene una cuota generada para la fecha seleccionada.")
                    ->addViolation();
        }
    }

}
