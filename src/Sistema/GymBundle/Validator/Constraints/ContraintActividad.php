<?php

namespace Sistema\GymBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class ContraintActividad extends Constraint {
 
	public $message = 'El cliente ya esta inscripto a un plan con la actividad %string%.';

	public function validatedBy(){	

	    return get_class($this).'Validator';
	}	

	public function getTargets(){

	    return self::CLASS_CONSTRAINT;
	}
}
