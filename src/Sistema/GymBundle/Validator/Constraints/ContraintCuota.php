<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sistema\GymBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class ContraintCuota extends Constraint {

    public $message = '"%string%"';

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }

    public function getTargets() {

        return self::CLASS_CONSTRAINT;
    }

}
