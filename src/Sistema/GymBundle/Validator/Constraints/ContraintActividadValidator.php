<?php

namespace Sistema\GymBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContraintActividadValidator extends ConstraintValidator {

    public function validate($actividad_cobro, Constraint $constraint) {

        if(!is_null($actividad_cobro->getPlan())){

            $cliente = $actividad_cobro->getCliente();

            if(!is_null($cliente)){
                
                $id_actividad = $actividad_cobro->getPlan()->getActividad()->getId();

                $inscripciones_del_cliente = $cliente->getActividadCobros()->filter(

                    function($inscripcion) use ($id_actividad) {

                        if($id_actividad == $inscripcion->getPlan()->getActividad()->getId()
                            && !is_null($inscripcion->getId())) return $inscripcion;
                    }
                );

                if(count($inscripciones_del_cliente) > 0){
                    
                    $this->context->buildViolation($constraint->message)
                         ->setParameter('%string%', $actividad_cobro->getPlan()->getActividad()->getNombre())
                         ->addViolation();
                }
            }else{
                $this->context->buildViolation("Debe elegir un cliente.")->addViolation();  
            }
        }else{
            
            $this->context->buildViolation("No se pueden crear clientes que no tengan asignados algun plan.")
                 ->addViolation();   
        }
    }
}
