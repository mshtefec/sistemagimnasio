<?php

namespace Sistema\MWSCONFBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ConfiguracionType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class ConfiguracionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('empresa')
            ->add('homepage',null,array('attr'  => array('cols'=>'1000','rows'=>'10')))
            ->add('imagenLogo', new ImagenLogoType(), array(
                'label' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\MWSCONFBundle\Entity\Configuracion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_mwsconfbundle_configuracion';
    }
}
