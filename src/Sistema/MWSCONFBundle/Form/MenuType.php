<?php

namespace Sistema\MWSCONFBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MenuType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class MenuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('url', null, array(
                'label' => 'Ruta',
            ))
            ->add('activo', null, array('required' => false))
            ->add('orden')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\MWSCONFBundle\Entity\Menu',
            'validation_groups' => array('registration'),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_mwsconfbundle_menu';
    }

}
