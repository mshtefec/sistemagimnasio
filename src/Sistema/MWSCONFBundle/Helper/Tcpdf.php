<?php
/**
* TCPDF Bridge
*
* @author ioalessio
*/
namespace Sistema\MWSCONFBundle\Helper;
use Symfony\Component\HttpFoundation\Response;

class Tcpdf extends \TCPDF
{
    private $config;

    public function Header()
    {
        if ($this->header_xobjid === false) {
            // start a new XObject Template
            $this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);
            $headerfont = $this->getHeaderFont();
            $headerdata = $this->getHeaderData();
            $this->y = $this->header_margin;
            if ($this->rtl) {
                $this->x = $this->w - $this->original_rMargin;
            } else {
                $this->x = $this->original_lMargin;
            }

                        $cell_height = $this->getCellHeight($headerfont[2] / $this->k);
                         if (strncasecmp(PHP_OS, 'WIN', 3) == 0) {
                             $ruta = '\\..\\..\\..\\..\\web\\bundles\\sistemamwsconf\\images\\';
                         } else {
                             $ruta = '/../../../../web/bundles/sistemamwsconf/images/';
                         }

                        if (($headerdata['logo']) and ($headerdata['logo'] != K_BLANK_IMAGE)) {
                $imgtype = \TCPDF_IMAGES::getImageFileType(__DIR__.$ruta.$headerdata['logo']);
                if (($imgtype == 'eps') or ($imgtype == 'ai')) {
                    $this->ImageEps(__DIR__.$ruta.$headerdata['logo'], '', '', $headerdata['logo_width']);
                } elseif ($imgtype == 'svg') {
                    $this->ImageSVG(__DIR__.$ruta.$headerdata['logo'], '', '', $headerdata['logo_width']);
                } else {
                                        //Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()) {
                    $this->Image(__DIR__.$ruta.$headerdata['logo'], '17', '6', $headerdata['logo_width'],0,'','','',false,300,'',false,false,0,false,false,false,false,0);
                }
                $imgy = $this->getImageRBY();
            } else {
                $imgy = $this->y;
            }

            // set starting margin for text data cell
            if ($this->getRTL()) {
                $header_x = $this->original_rMargin + ($headerdata['logo_width'] * 2.1);
            } else {
                $header_x = $this->original_lMargin + ($headerdata['logo_width'] * 2.1);
            }
            $cw = $this->w - $this->original_lMargin - $this->original_rMargin - ($headerdata['logo_width'] * 1.1);
            $this->SetTextColorArray($this->header_text_color);
            // header title
            $this->SetFont($headerfont[0], 'B', 8);

                        $this->SetY(20);
                        $this->SetX(27);
                        //Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M') {
            $this->Cell($cw, $cell_height, $headerdata['title'], 0, 'LTR', 'L', 0, '', 0);

                        if (isset($this->config['PDF_HEADER_STRING_FACTURA']) && isset($this->config['PDF_HEADER_STRING_FACTURA'])) {

                        //SetFont($family, $style='', $size=null, $fontfile='', $subset='default', $out=true)
                        $this->SetFont('times', 'B', 50, '', true);

            $this->MultiCell(40, $cell_height, $this->config['PDF_HEADER_STRING_FACTURA'], 1, 'C', false, 1, '110', '5', true, 0, false, true, 0, 'T', false);

                        $this->SetFont('times', '', 10, '', true);

            $this->MultiCell(40, $cell_height, $this->config['PDF_HEADER_STRING_FACTURA_DES'], 0, 'C', false, 1, '110', '22', true, 0, false, true, 0, 'T', false);

                        }

                        if (isset($headerdata['string'])) {
            // header string
            $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
            $this->SetX($header_x);
                        $this->SetY(5);
                        //public function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false) {
            $this->MultiCell(180, $cell_height, $headerdata['string'], 1, 'L', false, 1, '15', '', true, 0, false, true, 0, 'T', false);
            // print an ending header line
            }
                        //
                        $this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $headerdata['line_color']));
            $this->SetY((2.835 / $this->k) + max($imgy, $this->y));
            if ($this->rtl) {
                $this->SetX($this->original_rMargin);
            } else {
                $this->SetX($this->original_lMargin);
            }
            $this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
            $this->endTemplate();
        }
        // print header template
        $x = 0;
        $dx = 0;
        if (!$this->header_xobj_autoreset and $this->booklet and (($this->page % 2) == 0)) {
            // adjust margins for booklet mode
            $dx = ($this->original_lMargin - $this->original_rMargin);
        }
        if ($this->rtl) {
            $x = $this->w + $dx;
        } else {
            $x = 0 + $dx;
        }
        $this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
        if ($this->header_xobj_autoreset) {
            // reset header xobject template at each page
            $this->header_xobjid = false;
        }
    }

        /**
     * This method is used to render the page footer.
     * It is automatically called by AddPage() and could be overwritten in your own inherited class.
     * @public
     */
    public function Footer()
    {
        $cur_y = $this->y;
        $this->SetTextColorArray($this->footer_text_color);
        //set style for cell border
        $line_width = (0.85 / $this->k);
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));
        //print document barcode
        $barcode = $this->getBarcode();
        if (!empty($barcode)) {
            $this->Ln($line_width);
            $barcode_width = round(($this->w - $this->original_lMargin - $this->original_rMargin) / 3);
            $style = array(
                'position' => $this->rtl ? 'R' : 'L',
                'align' => $this->rtl ? 'R' : 'L',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0,0,0),
                'bgcolor' => false,
                'text' => false
            );
            $this->write1DBarcode($barcode, 'C128', '', $cur_y + $line_width, '', (($this->footer_margin / 3) - $line_width), 0.3, $style, '');
        }
        $w_page = isset($this->l['w_page']) ? $this->l['w_page'].' ' : '';
        if (empty($this->pagegroups)) {
            $pagenumtxt = $w_page.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
        } else {
            $pagenumtxt = $w_page.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
        }
        $this->SetY($cur_y);
        //Print page number
        if ($this->getRTL()) {
            $this->SetX($this->original_rMargin);
            $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
        } else {
            $this->SetX($this->original_lMargin);
            $this->Cell(0, 0, $this->getAliasRightShift().$pagenumtxt, 'T', 0, 'R');
        }

                // Position at 15 mm from bottom
        $this->SetY(-25);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->MultiCell(180, 30, $this->config['pie'], 0, 'L', false, 1, '15', '', true, 0, false, true, 0, 'T', false);
    }

        public function init($config)
        {

            // set document information
            $this->SetCreator(PDF_CREATOR);

            if (isset($config['autor'])) {
            $this->SetAuthor($config['autor']);
            } else {
            $this->SetAuthor('Alessio');
            }

            if (isset($config['titulo'])) {
            $this->SetTitle($config['titulo']);
            } else {
            $this->SetTitle('Test');
            }

            if (isset($config['asunto'])) {
            $this->SetSubject($config['asunto']);
            } else {
            $this->SetSubject('TCPDF test');
            }

            if (isset($config['palabras_claves'])) {
            $this->SetKeywords($config['palabras_claves']);
            } else {
            $this->SetKeywords('TCPDF, PDF, example, test, guide');
            }

            // set default header data
            $this->SetHeaderData($config['PDF_HEADER_LOGO'], $config['PDF_HEADER_LOGO_WIDTH'], $config['PDF_HEADER_TITLE'], $config['PDF_HEADER_STRING']);

            // set header and footer fonts
            $this->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $this->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $this->SetHeaderMargin(PDF_MARGIN_HEADER);
            $this->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set auto page breaks
            $this->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

            //set image scale factor
            $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // ---------------------------------------------------------

            // set default font subsetting mode
            $this->setFontSubsetting(true);

            // Set font
            // dejavusans is a UTF-8 Unicode font, if you only need to
            // print standard ASCII chars, you can use core fonts like
            // helvetica or times to reduce file size.
            $this->SetFont('dejavusans', '', 14, '', true);

            // Add a page
            // This method has several options, check the source code documentation for more information.
            $this->AddPage();

        }
        /**
         */
        public function quick_pdf($html, $config=null, $file = "html.pdf", $format = "S")
        {
          $this->config = $config;
          $this->init($config);

            // Close and output PDF document
            // This method has several options, check the source code documentation for more information.
            $this->writeHTML($html, true, false, true, false, '');

            $response =  new Response($this->Output($file, $format));
            $response->headers->set('Content-Type', 'application/pdf');

            return $response;

        }
}
