<?php

namespace Sistema\MWSCONFBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\Role;
//se usa para ejectaur un command console
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
//fin

/**
 * Router controller.
 * @author Gonzalo Alonso <gonkpo@gmail.com>
 *
 * @Route("/superadmin/router")
 */
class RouterController extends Controller
{
    /**
     * Lists all Router entities.
     *
     * @Route("/", name="superadmin_role_router_debug")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        // $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaUserBundle:Role')->findAll();

        $kernel = $this->container->get('kernel');
        $app = new Application($kernel);

        $input = new StringInput('router:debug');
        $output = new StreamOutput(fopen('php://temp', 'w'));

        $app->doRun($input, $output);

        rewind($output->getStream());
        $response = stream_get_contents($output->getStream());
        $response = str_replace('[router] Current routes', '', $response);
        $response = preg_split("/[\s,]+/", $response);
        $rutas = array();
        $control = 0;
        $clave = 1;
        foreach ($response as $key => $value) {
            if ($key != 0) {
                $rutas[$control][$clave] = $value;
                if ($key % 5 == 0) {
                    $control++;
                    $clave = 1;
                } else {
                    $clave++;
                }
            }
        }
        foreach ($rutas as $key => $values) {
            if ($key == 0) {
                // $rutas[$key][3] = 'Activo';
                $rutas[$key][4] = 'Acceso';
                $rutas[$key][6] = 'Role nombre';
                $rutas[$key][7] = 'Generar';
                // $rutas[$key][8] = 'Orden';
            } else {
                foreach ($entities as $role) {
                    if ($values[1] == $role->getUrl()) {
                        // $activo = null;
                        $acceso = null;
                        // if ($role->getActivo()) {
                        //     $activo = 'checked';
                        // }
                        if ($role->getAcceso()) {
                            $acceso = 'checked';
                        }
                        // $rutas[$key][3] = '<input type="checkbox" name="form['.$key.'][activo]" '.$activo.'></input>';
                        $rutas[$key][4] = '<input type="checkbox" name="form['.$key.'][acceso]" '.$acceso.'></input>';
                        $rutas[$key][6] = '<input type="text" value="'.$role->getRoleName().'" name="form['.$key.'][role_name]" size="30%"></input>';
                        $rutas[$key][7] = '<input type="checkbox" name="form['.$key.'][check]" class="generar" checked></input>';
                        // $rutas[$key][8] = '<input type="text" value="'.$role->getOrden().'" name="form['.$key.'][orden]" size="3px;"></input>';
                        $rutas[$key][9] = '<input type="hidden" value="'.$role->getId().'" name="form['.$key.'][id]"></input>';
                    }
                }
                if (!array_key_exists(7, $rutas[$key])) {
                    // si no tiene nombre lo creo segun la url.
                    $roleName = strtoupper(str_replace("_", " ", $rutas[$key][1]));
                    // $rutas[$key][3] = '<input type="checkbox" name="form['.$key.'][activo]"></input>';
                    $rutas[$key][4] = '<input type="checkbox" name="form['.$key.'][acceso]"></input>';
                    $rutas[$key][6] = '<input type="text" value="'.$roleName.'" name="form['.$key.'][role_name]" size="30%"></input>';
                    $rutas[$key][7] = '<input type="checkbox" name="form['.$key.'][check]" class="generar"></input>';
                    // $rutas[$key][8] = '<input type="text" value="0" name="form['.$key.'][orden]" size="3px;></input>';
                    $rutas[$key][9] = '<input type="hidden" value="" name="form['.$key.'][id]"></input>';
                }
                $rutas[$key][1] = '<input type="text" value="'.$rutas[$key][1].'" name="form['.$key.'][url]" size="40%" readonly></input>';
            }
//echo $key;ladybug_dump_die($values);
        }
        $titles = array_shift($rutas);
        array_pop($rutas);
        // $response = explode('\n', $response);
        // ladybug_dump_die($rutas);
        return array(
            'titles' => $titles,
            'rutas' => $rutas,
        );
    }

    /**
     * Create o update role segun route.
     *
     * @Route("/router/debug/update", name="superadmin_role_router_debug_update")
     * @Method("POST")
     * @Template()
     */
    public function routerDebugUpdateAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaUserBundle:Role')->findAll();

        $values = $request->request->all();
// ladybug_dump($entities);
        foreach ($values['form'] as $key => $value) {
            if (!empty($value['id'])) {
                foreach ($entities as $key => $entity) {
                    if ($value['id'] == $entity->getId()) {
                        if (array_key_exists('check', $value)) {
                            // if ($value['role_name'] != $entity->getRoleName()) {
                                $entity->setRoleName($value['role_name']);
                                // if (empty($value['activo'])) {
                                //     $entity->setActivo(false);
                                // } else {
                                //     $entity->setActivo(true);
                                // }
                                $entity->setUrl($value['url']);
                                // $entity->setOrden($value['orden']);
                                if (empty($value['acceso'])) {
                                    $entity->setAcceso(false);
                                } else {
                                    $entity->setAcceso(true);
                                }
                                $em->persist($entity);
                            // }
                            unset($entities[$key]);
                        }
                    }
                }
            } else {
                if (array_key_exists('check', $value)) {
                    $role = new Role();
                    $role->setRoleName($value['role_name']);
                    // if (empty($value['activo'])) {
                    //     $role->setActivo(false);
                    // } else {
                    //     $role->setActivo(true);
                    // }
                    $role->setUrl($value['url']);
                    // $role->setOrden($value['orden']);
                    // $role->setRoles(null);
                    if (empty($value['acceso'])) {
                        $role->setAcceso(false);
                    } else {
                        $role->setAcceso(true);
                    }
                    $em->persist($role);
                }
            }
        }
        foreach ($entities as $entity) {
            if ($entity->getName() != 'ROLE_ADMIN') {
                $em->remove($entity);
            }
        }
        $em->flush();

        return $this->redirect($this->generateUrl('superadmin_role_router_debug'));
    }

    private function validarUrl($value)
    {
        $estado = false;
        try {
            $this->redirect($this->generateUrl($value));
            $estado = true;
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('danger', 'La Url no es valida');
        }

        return $estado;
    }

}
