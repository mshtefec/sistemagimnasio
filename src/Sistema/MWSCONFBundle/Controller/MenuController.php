<?php

namespace Sistema\MWSCONFBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\MWSCONFBundle\Entity\Menu;
use Sistema\MWSCONFBundle\Form\MenuType;
use Sistema\MWSCONFBundle\Form\MenuFilterType;
//se usa para ejectaur un command console
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;

//fin

/**
 * Menu controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/superadmin/menu")
 */
class MenuController extends Controller
{
    /**
     * Lists all Menu entities.
     *
     * @Route("/", name="superadmin_menu")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), (isset($this->container->parameters['knp_paginator.page_range'])) ? $this->container->parameters['knp_paginator.page_range'] : 10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaMWSCONFBundle:Menu')
                ->createQueryBuilder('a')
                ->orderBy('a.orden', 'ASC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('MenuControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('MenuControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('MenuControllerFilter')) {
                $filterData = $session->get('MenuControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }

    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new MenuFilterType(), $filterData, array(
            'action' => $this->generateUrl('superadmin_menu'),
            'method' => 'GET',
        ));

        $form
                ->add('filter', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.filter',
                    'attr' => array('class' => 'btn btn-success col-lg-1'),
                ))
                ->add('reset', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.index.reset',
                    'attr' => array('class' => 'btn btn-danger col-lg-1 col-lg-offset-1'),
                ))
        ;

        return $form;
    }

    /**
     * Creates a new Menu entity.
     *
     * @Route("/", name="superadmin_menu_create")
     * @Method("POST")
     * @Template("SistemaMWSCONFBundle:Menu:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Menu();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $estadoUrl = false;
        if ($form->isValid()) {
            //pregunta si la url es valida
            $estadoUrl = $this->validarUrl($entity->getUrl());
            if ($estadoUrl) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

                $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl('superadmin_menu_new') : $this->generateUrl('superadmin_menu_show', array('id' => $entity->getId()));

                return $this->redirect($nextAction);
            }
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Menu entity.
     *
     * @param Menu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Menu $entity)
    {
        $form = $this->createForm(new MenuType(), $entity, array(
            'action' => $this->generateUrl('superadmin_menu_create'),
            'method' => 'POST',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Menu entity.
     *
     * @Route("/new", name="superadmin_menu_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new Menu();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Menu entity.
     *
     * @Route("/{id}", name="superadmin_menu_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaMWSCONFBundle:Menu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Menu entity.
     *
     * @Route("/{id}/edit", name="superadmin_menu_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaMWSCONFBundle:Menu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Menu entity.
     *
     * @param Menu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Menu $entity)
    {
        $form = $this->createForm(new MenuType(), $entity, array(
            'action' => $this->generateUrl('superadmin_menu_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add(
                        'saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Edits an existing Menu entity.
     *
     * @Route("/{id}", name="superadmin_menu_update")
     * @Method("PUT")
     * @Template("SistemaMWSCONFBundle:Menu:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaMWSCONFBundle:Menu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Menu entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $estadoUrl = false;

        if ($editForm->isValid()) {
            //pregunta si la url es valida
            $estadoUrl = $this->validarUrl($entity->getUrl());
            if ($estadoUrl) {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

                $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl('superadmin_menu_new') : $this->generateUrl('superadmin_menu_show', array('id' => $id));

                return $this->redirect($nextAction);
            }
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Menu entity.
     *
     * @Route("/{id}", name="superadmin_menu_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaMWSCONFBundle:Menu')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Menu entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('superadmin_menu'));
    }

    /**
     * Creates a form to delete a Menu entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleCrudGeneratorBundle');
        $onclick = 'return confirm("' . $mensaje . '");';

        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('superadmin_menu_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                            'label' => 'views.recordactions.delete',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    private function validarUrl($value)
    {
        $estado = false;
        try {
            $this->redirect($this->generateUrl($value));
            $estado = true;
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('danger', 'La Url no es valida');
        }

        return $estado;
    }

    /**
     * @Route("/router/debug", name="superadmin_menu_router_debug")
     * @Template()
     */
    public function routerDebugAction()
    {
        $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaMWSCONFBundle:Menu')->findAll();

        $kernel = $this->container->get('kernel');
        $app = new Application($kernel);

        $input = new StringInput('router:debug');
        $output = new StreamOutput(fopen('php://temp', 'w'));

        $app->doRun($input, $output);

        rewind($output->getStream());
        $response = stream_get_contents($output->getStream());
        $response = str_replace('[router] Current routes', '', $response);
        $response = preg_split("/[\s,]+/", $response);
        $rutas = array();
        $control = 0;
        $clave = 1;
        foreach ($response as $key => $value) {
            if ($key != 0) {
                $rutas[$control][$clave] = $value;
                if ($key % 5 == 0) {
                    $control++;
                    $clave = 1;
                } else {
                    $clave++;
                }
            }
        }
        foreach ($rutas as $key => $values) {
            if ($key == 0) {
                $rutas[$key][3] = 'Activo';
                $rutas[$key][4] = 'Acceso';
                $rutas[$key][6] = 'Titulo';
                $rutas[$key][7] = 'Menu';
                $rutas[$key][8] = 'Orden';
            } else {
                foreach ($entities as $menu) {
                    if ($values[1] == $menu->getUrl()) {
                        $activo = null;
                        // $acceso = null;
                        if ($menu->getActivo()) {
                            $activo = 'checked';
                        }
                        // if ($menu->getAcceso()) {
                        //     $acceso = 'checked';
                        // }
                        $rutas[$key][3] = '<input type="checkbox" name="form[' . $key . '][activo]" ' . $activo . '></input>';
                        // $rutas[$key][4] = '<input type="checkbox" name="form['.$key.'][acceso]" '.$acceso.'></input>';
                        $rutas[$key][6] = '<input type="text" value="' . $menu->getTitulo() . '" name="form[' . $key . '][titulo]" size="25%"></input>';
                        $rutas[$key][7] = '<input type="checkbox" name="form[' . $key . '][check]" checked></input>';
                        $rutas[$key][8] = '<input type="text" value="' . $menu->getOrden() . '" name="form[' . $key . '][orden]" size="3px;"></input>';
                        $rutas[$key][9] = '<input type="hidden" value="' . $menu->getId() . '" name="form[' . $key . '][id]"></input>';
                    }
                }
                if (!array_key_exists(7, $rutas[$key])) {
                    $rutas[$key][3] = '<input type="checkbox" name="form[' . $key . '][activo]"></input>';
                    // $rutas[$key][4] = '<input type="checkbox" name="form['.$key.'][acceso]"></input>';
                    $rutas[$key][6] = '<input type="text" value="" name="form[' . $key . '][titulo]" size="25%"></input>';
                    $rutas[$key][7] = '<input type="checkbox" name="form[' . $key . '][check]"></input>';
                    $rutas[$key][8] = '<input type="text" value="0" name="form[' . $key . '][orden]" size="3px;></input>';
                    $rutas[$key][9] = '<input type="hidden" value="" name="form[' . $key . '][id]"></input>';
                }
                $rutas[$key][1] = '<input type="text" value="' . $rutas[$key][1] . '" name="form[' . $key . '][url]" size="40%" readonly></input>';
            }
//echo $key;ladybug_dump_die($values);
        }
        $titles = array_shift($rutas);
        array_pop($rutas);
        // $response = explode('\n', $response);
        // ladybug_dump_die($rutas);
        return array(
            'titles' => $titles,
            'rutas' => $rutas,
        );
    }

    /**
     * Create o update menu segun route.
     *
     * @Route("/router/debug/update", name="superadmin_menu_router_debug_update")
     * @Method("POST")
     * @Template()
     */
    public function routerDebugUpdateAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SistemaMWSCONFBundle:Menu')->findAll();

        $values = $request->request->all();
// ladybug_dump($entities);
        foreach ($values['form'] as $key => $value) {
            if (!empty($value['id'])) {
                foreach ($entities as $key => $entity) {
                    if ($value['id'] == $entity->getId()) {
                        if (array_key_exists('check', $value)) {
                            // if ($value['titulo'] != $entity->getTitulo()) {
                            $entity->setTitulo($value['titulo']);
                            if (empty($value['activo'])) {
                                $entity->setActivo(false);
                            } else {
                                $entity->setActivo(true);
                            }
                            $entity->setUrl($value['url']);
                            $entity->setOrden($value['orden']);
                            // if (empty($value['acceso'])) {
                            //     $entity->setAcceso(false);
                            // } else {
                            //     $entity->setAcceso(true);
                            // }
                            $em->persist($entity);
                            // }
                            unset($entities[$key]);
                        }
                    }
                }
            } else {
                if (array_key_exists('check', $value)) {
                    $menu = new Menu();
                    $menu->setTitulo($value['titulo']);
                    if (empty($value['activo'])) {
                        $menu->setActivo(false);
                    } else {
                        $menu->setActivo(true);
                    }
                    $menu->setUrl($value['url']);
                    $menu->setOrden($value['orden']);
                    // $menu->setRoles(null);
                    // if (empty($value['acceso'])) {
                    //     $menu->setAcceso(false);
                    // } else {
                    //     $menu->setAcceso(true);
                    // }
                    $em->persist($menu);
                }
            }
        }
        foreach ($entities as $entity) {
            $em->remove($entity);
        }
        $em->flush();

        return $this->redirect($this->generateUrl('superadmin_menu_router_debug'));
    }

}
