<?php

namespace Sistema\MWSCONFBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sistema\MWSCONFBundle\Entity\Configuracion;

trait Referer {
    private function getRefererParams() {
        $request = $this->getRequest();
        $referer = $request->headers->get('referer');
        $baseUrl = $request->getBaseUrl();
        $lastPath = substr($referer, strpos($referer, $baseUrl) + strlen($baseUrl));
        return $this->get('router')->getMatcher()->match($lastPath);
    }
}

class DefaultController extends Controller {
    use Referer;

    public function logoAction() {
        $session = $this->getRequest()->getSession();

        if (!$session->get('logoWebPath')) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaMWSCONFBundle:Configuracion')->findAll();

            if (empty($entity)) {
                $session->set('logoWebPath', 'bundles/sistemamwsconf/images/logo.jpg');
            } else {
                if ($entity[0]->getImagenlogo() != null) {
                    $session->set('logoWebPath', $entity[0]->getImagenlogo()->getWebPath());
                } else {
                    $session->set('logoWebPath', 'bundles/sistemamwsconf/images/logo.jpg');
                }
            }
        }

        return $this->render('SistemaMWSCONFBundle:Default:logo.html.twig', array(
            'logoWebPath' => $session->get('logoWebPath')
        ));
    }

    public function findGymByUserAction() {
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->get('security.context');
        $user            = $this->getUser();
        
        if (false === $securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            if ($securityContext->isGranted('ROLE_ADMIN')) {
                $entities = $em->getRepository('SistemaGymBundle:Gimnasio')->findGymByIdUser($user->getId());
            } else if ($securityContext->isGranted('ROLE_EMPLEADO')) {
                $idGymSession = $user->getPersonal()->getGimnasio()->getId();
                $entities = $em->getRepository('SistemaGymBundle:Gimnasio')->find($idGymSession);
                $entities = array($entities);
            }
        } else {
            $entities = $em->getRepository('SistemaGymBundle:Gimnasio')->findGymByIdUser();
        }

        return $this->render('SistemaMWSCONFBundle:Default:findGymByUser.html.twig', array(
            'gimnasiosUser' => $entities,
        ));
    }

    /**
     * @Route("/admin/cambiar/gimnasio/{idGym}", name="cambiar_gym")
     * @Method("GET")
     */
    public function cambiarGymAction($idGym) {
        $request = $this->getRequest();
        $session = $request->getSession();
        $em      = $this->getDoctrine()->getManager();

        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $user = $this->getUser();
            $entity = $em->getRepository('SistemaGymBundle:Gimnasio')->findOneGymByUser($user->getId(), $idGym);
        } else {
            $entity = $em->getRepository('SistemaGymBundle:Gimnasio')->findOneGymByUser(null, $idGym);
        }

        if (!is_null($entity)) {
            $session->set('_idGimnasio', $idGym);
            $countCuotas = $em->getRepository('SistemaGymBundle:Cuota')
                              ->findCuotasXGym($idGym, null, "impaga", '=');
            //Actualizo cuotas en session
            $countCuotas = count($countCuotas);
            $session->set('countCuotas', $countCuotas);
            //Fin Actualizo cuotas en session
        }

        $referer = $request->server->get('HTTP_REFERER');
        $secciones = array("gimnasio", "actividad", "cliente", "actividadcobro", "asistencia", "cuota", "personal", "gimnasio-estadistica", "rutina");

        $ruta = 'admin_gimnasio';

        foreach ($secciones as $url) {
            $pos = strpos($referer, $url);
            if ($pos) {
                $ruta = $url;
                break;
            }
        }

        switch ($ruta) {
            case 'actividad':
                $ruta = 'admin_actividad';
                break;
            case 'cliente':
                $ruta = 'admin_cliente';
                break;
            case 'actividadcobro':
                $ruta = 'admin_actividadcobro';
                break;
            case 'asistencia':
                $ruta = 'admin_asistencia';
                break;
            case 'cuota':
                $ruta = 'admin_cuota';
                break;
            case 'pago':
                $ruta = 'admin_pago';
                break;
            case 'personal':
                $ruta = 'admin_personal';
                break;
            case 'gimnasio-estadistica':
                $ruta = 'gimnasio_estadistica';
                break;
            case 'rutina':
                $ruta = 'admin_rutina';
                break;
            default:
                $ruta = 'admin_gimnasio';
                break;
        }

        return $this->redirectToRoute($ruta);
    }
}