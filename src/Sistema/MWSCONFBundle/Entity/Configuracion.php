<?php

namespace Sistema\MWSCONFBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Configuracion
 *
 * @ORM\Table(name="configuracion")
 * @ORM\Entity(repositoryClass="Sistema\MWSCONFBundle\Entity\ConfiguracionRepository")
 */
class Configuracion {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="text")
     */
    private $homepage;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="ImagenLogo", mappedBy="configuracion", cascade={"all"}, orphanRemoval=true)
     */
    private $imagenLogo;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="configuracion", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="configuraciones", type="array")
     */
    private $configuraciones;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Configuracion
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Adds/sets an element in the collection at the index / with the specified key.
     *
     * When the collection is a Map this is like put(key,value)/add(key,value).
     * When the collection is a List this is like add(position,value).
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function setItemConfiguraciones($key, $value) {
        $this->configuraciones[$key] = $value;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set empresa
     *
     * @param string $empresa
     * @return Configuracion
     */
    public function setEmpresa($empresa) {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string 
     */
    public function getEmpresa() {
        return $this->empresa;
    }

    /**
     * Set homepage
     *
     * @param string $homepage
     * @return Configuracion
     */
    public function setHomepage($homepage) {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return string 
     */
    public function getHomepage() {
        return $this->homepage;
    }

    /**
     * Set configuraciones
     *
     * @param array $configuraciones
     * @return Configuracion
     */
    public function setConfiguraciones($configuraciones) {
        $this->configuraciones = $configuraciones;

        return $this;
    }

    /**
     * Get configuraciones
     *
     * @return array 
     */
    public function getConfiguraciones() {
        return $this->configuraciones;
    }

    /**
     * Set imagenLogo
     *
     * @param \Sistema\MWSCONFBundle\Entity\ImagenLogo $imagenLogo
     * @return Configuracion
     */
    public function setImagenLogo(\Sistema\MWSCONFBundle\Entity\ImagenLogo $imagenLogo = null) {
        $this->imagenLogo = $imagenLogo;

        return $this;
    }

    /**
     * Get imagenLogo
     *
     * @return \Sistema\MWSCONFBundle\Entity\ImagenLogo 
     */
    public function getImagenLogo() {
        return $this->imagenLogo;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Configuracion
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

}
