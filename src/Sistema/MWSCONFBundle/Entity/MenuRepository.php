<?php

namespace Sistema\MWSCONFBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Sistema\UserBundle\Entity\User;

/**
 * MenuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MenuRepository extends EntityRepository
{
    /**
     * Busca los menu segun el usuario y sus roles.
     */
    public function GetMenuByUser($user)
    {
        if ($user === 'anon.') {
            return null;
        } else {
            foreach ($user->getRoles() as $key => $value) {
                $rolesUrls[$key] = $value->getUrl();
            }

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb
                ->from('SistemaMWSCONFBundle:Menu', 'm')
                ->select('m.titulo', 'm.url')
                ->where($qb->expr()->in('m.url', ':roles_urls'))
                ->andWhere('m.activo = true')
                ->add('orderBy', 'm.orden ASC')
                ->setParameter('roles_urls', $rolesUrls)
            ;

            return $qb
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
            ;
        }
    }

    /**
     * Busca todos los menu es superusuario.
     */
    public function GetMenuBySuperuser()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->from('SistemaMWSCONFBundle:Menu', 'm')
            ->select('m.titulo', 'm.url')
            ->add('orderBy', 'm.orden ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;
    }

    /**
     * Busca los menu es admin.
     */
    public function GetMenuByAdmin()
    {
        $nots = array(
            'superadmin_configuracion', 'superadmin_menu', 'role'
        );
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->from('SistemaMWSCONFBundle:Menu', 'm')
            ->where($qb->expr()->notIn('m.url', $nots))
            ->select('m.titulo', 'm.url')
            ->add('orderBy', 'm.orden ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;
    }
}
