<?php

namespace Sistema\MWSCONFBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MWSimple\Bundle\AdminCrudBundle\Entity\BaseFile;

/**
 * ImagenLogo
 *
 * @ORM\Table(name="imagen_logo")
 * @ORM\Entity
 */
class ImagenLogo extends BaseFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="Configuracion", inversedBy="imagenLogo")
     * @ORM\JoinColumn(name="configuracion_id", referencedColumnName="id")
     */
    private $configuracion;

    /**
     * Get Upload directory
     *
     * @return string
     */
    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        $this->uploadDir = 'uploads/imagen_logo';

        return $this->uploadDir;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set configuracion
     *
     * @param  \Sistema\MWSCONFBundle\Entity\Configuracion $configuracion
     * @return ImagenLogo
     */
    public function setConfiguracion(\Sistema\MWSCONFBundle\Entity\Configuracion $configuracion = null)
    {
        $this->configuracion = $configuracion;

        return $this;
    }

    /**
     * Get configuracion
     *
     * @return \Sistema\MWSCONFBundle\Entity\Configuracion
     */
    public function getConfiguracion()
    {
        return $this->configuracion;
    }
}
