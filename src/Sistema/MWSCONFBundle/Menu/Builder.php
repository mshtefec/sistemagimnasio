<?php

namespace Sistema\MWSCONFBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function adminMenu(FactoryInterface $factory, array $options)
    {
        $em = $this->container->get('doctrine')->getManager();
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'sidebar-nav');

        $isAuthenticated = $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY');

        if ($isAuthenticated != false) {
            if ($this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
                $entities = $em->getRepository('SistemaMWSCONFBundle:Menu')->GetMenuBySuperuser();
            } elseif ($this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
                $entities = $em->getRepository('SistemaMWSCONFBundle:Menu')->GetMenuByAdmin();
            } else {
                $user = $this->container->get('security.context')->getToken()->getUser();
                $entities = $em->getRepository('SistemaMWSCONFBundle:Menu')->GetMenuByUser($user);
            }

            foreach ($entities as $m) {
                // if ($m['acceso']==false) {
                   $menu->addChild($m['titulo'], array('route' => $m['url']));
                // }
            }
        } else {
            $menu->addChild('', array('route' => ''));
        }

        return $menu;
    }
}
