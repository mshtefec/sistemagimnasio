<?php

namespace Sistema\MWSCONFBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Sistema\MWSCONFBundle\Entity\Configuracion;

class LoadConfiguracion implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $configuracion = new Configuracion();
        $configuracion->setTitulo('<span class="titulo1">NEON<span class="titulo3r"> 3R</span> </span><span class="titulo3">INSTALACIONES ELECTRICAS</span>');
        $configuracion->setEmpresa('M.M.O. Raúl Roberto Rojas');
        $configuracion->setHomepage('Colon N° 754 Resistencia - Chaco Tel: 362-4448580 / Cel: 362 154647854  mail: <a href="mailto:gerencia@neon3r.com.ar">gerencia@neon3r.com.ar</a> ');
        $manager->persist($configuracion);
        $manager->flush();
    }
}
