<?php

namespace Sistema\FACTURACIONBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CuentaCorriente
 *
 * @ORM\Table(name="cuenta_corriente")
 * @ORM\Entity(repositoryClass="Sistema\FACTURACIONBundle\Entity\CuentaCorrienteRepository")
 */
class CuentaCorriente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\RRHHBundle\Entity\Cliente", inversedBy="cuentaCorriente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=true)
     */
    private $cliente;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cliente
     *
     * @param  \Sistema\RRHHBundle\Entity\Cliente $cliente
     * @return CuentaCorriente
     */
    public function setCliente(\Sistema\RRHHBundle\Entity\Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sistema\RRHHBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

}
