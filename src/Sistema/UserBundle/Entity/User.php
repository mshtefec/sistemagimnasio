<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Entity\User as BaseUser;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="fos_user")
 * @ORM\Entity()
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="fos_user_role")
     */
    protected $user_roles;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Gimnasio", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $gimnasios;

    /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="Sistema\MWSCONFBundle\Entity\Configuracion", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $configuracion;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\RRHHBundle\Entity\Personal", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $personal;

    /**
     * @ORM\Column(name="fecha_fin_servicio", type="date")
     */
    private $fecha_fin_servicio;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Servicio", inversedBy="users")
     */
    private $servicio;

    /**
     * @var integer
     *
     * @ORM\column(type="integer")
     */
    private $cantidad_gimnasio;

    /**
     * @ORM\OneToOne(targetEntity="Preferencia", inversedBy="user", cascade={"all"})
     * @ORM\JoinColumn(name="preferencia_id", referencedColumnName="id")
     */
    private $preferencias;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Rutina", mappedBy="user", cascade={"all"})
     */
    private $rutinas;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Pago", mappedBy="user")
     */
    private $pagos;    


    public function __construct()
    {
        parent::__construct();
        $this->user_roles = new ArrayCollection();
        $this->gimnasios = new ArrayCollection();
        $this->rutinas = new ArrayCollection();
        $this->cantidad_gimnasio = 1;
        $fecha_actual = new \DateTime();
        $this->fecha_fin_servicio = $fecha_actual->modify('next month');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns an ARRAY of Role objects with the default Role object appended.
     * @return array
     */
    public function getRoles() {
        return $this->user_roles->toArray();
    }

    /**
     * Returns the true ArrayCollection of Roles.
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    public function getRolesCollection() {
        return $this->user_roles;
    }

    /**
     * Pass a string, get the desired Role object or null.
     * @param  string    $role
     * @return Role|null
     */
    public function getRole($role) {
        foreach ($this->getRoles() as $roleItem) {
            if ($role == $roleItem->getRole()) {
                return $roleItem;
            }
        }

        return null;
    }

    /**
     * Pass a string, checks if we have that Role. Same functionality as getRole() except returns a real boolean.
     * @param  string  $role
     * @return boolean
     */
    public function hasRole($role) {
        if ($this->getRole($role)) {
            return true;
        }

        return false;
    }

    /**
     * Adds a Role OBJECT to the ArrayCollection. Can't type hint due to interface so throws Exception.
     * @throws Exception
     * @param  Role      $role
     */
    public function addRole($role) {
        if (!$role instanceof Role) {
            throw new \Exception("addRole takes a Role object as the parameter");
        }

        if (!$this->hasRole($role->getRole())) {
            $this->user_roles->add($role);
        }
    }

    /**
     * Pass a string, remove the Role object from collection.
     * @param string $role
     */
    public function removeRole($role) {
        $roleElement = $this->getRole($role);
        if ($roleElement) {
            $this->user_roles->removeElement($roleElement);
        }
    }

    /**
     * Pass an ARRAY of Role objects and will clear the collection and re-set it with new Roles.
     * Type hinted array due to interface.
     * @param array $user_roles Of Role objects.
     */
    public function setRoles(array $user_roles) {
        $this->user_roles->clear();
        foreach ($user_roles as $role) {
            $this->addRole($role);
        }
    }

    /**
     * Directly set the ArrayCollection of Roles. Type hinted as Collection which is the parent of (Array|Persistent)Collection.
     * @param Doctrine\Common\Collections\Collection $role
     */
    public function setRolesCollection(Collection $collection) {
        $this->user_roles = $collection;
    }

    /**
     * Add user_roles
     *
     * @param  \Sistema\UserBundle\Entity\Role $userRoles
     * @return User
     */
    public function addUserRole(\Sistema\UserBundle\Entity\Role $userRoles) {
        $this->user_roles[] = $userRoles;

        return $this;
    }

    /**
     * Remove user_roles
     *
     * @param \Sistema\UserBundle\Entity\Role $userRoles
     */
    public function removeUserRole(\Sistema\UserBundle\Entity\Role $userRoles) {
        $this->user_roles->removeElement($userRoles);
    }

    /**
     * Get user_roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles() {
        return $this->user_roles;
    }

    /**
     * Add gimnasios
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasios
     * @return User
     */
    public function addGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasios) {
        $this->gimnasios[] = $gimnasios;

        return $this;
    }

    /**
     * Remove gimnasios
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasios
     */
    public function removeGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasios) {
        $this->gimnasios->removeElement($gimnasios);
    }

    /**
     * Get gimnasios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGimnasios() {
        return $this->gimnasios;
    }

    /**
     * Set configuracion
     *
     * @param \Sistema\MWSCONFBundle\Entity\Configuracion $configuracion
     * @return User
     */
    public function setConfiguracion(\Sistema\MWSCONFBundle\Entity\Configuracion $configuracion = null) {
        $configuracion->setUser($this);
        $this->configuracion = $configuracion;

        return $this;
    }

    /**
     * Get configuracion
     *
     * @return \Sistema\MWSCONFBundle\Entity\Configuracion 
     */
    public function getConfiguracion() {
        return $this->configuracion;
    }

    /**
     * Set personal
     *
     * @param \Sistema\RRHHBundle\Entity\Personal $personal
     * @return User
     */
    public function setPersonal(\Sistema\RRHHBundle\Entity\Personal $personal = null) {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Sistema\RRHHBundle\Entity\Personal 
     */
    public function getPersonal() {
        return $this->personal;
    }

    /**
     * Set cantidad_gimnasio
     *
     * @param integer $cantidadGimnasio
     * @return User
     */
    public function setCantidadGimnasio($cantidadGimnasio) {
        $this->cantidad_gimnasio = $cantidadGimnasio;

        return $this;
    }

    /**
     * Get cantidad_gimnasio
     *
     * @return integer 
     */
    public function getCantidadGimnasio() {
        return $this->cantidad_gimnasio;
    }

    /**
     * Set fecha_fin_servicio
     *
     * @param \DateTime $fechaFinServicio
     * @return User
     */
    public function setFechaFinServicio($fechaFinServicio) {
        $this->fecha_fin_servicio = $fechaFinServicio;

        return $this;
    }

    /**
     * Get fecha_fin_servicio
     *
     * @return \DateTime 
     */
    public function getFechaFinServicio() {
        return $this->fecha_fin_servicio;
    }

    /**
     * Set servicio
     *
     * @param \Sistema\GymBundle\Entity\Servicio $servicio
     * @return User
     */
    public function setServicio(\Sistema\GymBundle\Entity\Servicio $servicio = null) {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio
     *
     * @return \Sistema\GymBundle\Entity\Servicio 
     */
    public function getServicio() {
        return $this->servicio;
    }

    /**
     * Set preferencias
     *
     * @param \Sistema\UserBundle\Entity\Preferencia $preferencias
     * @return User
     */
    public function setPreferencias(\Sistema\UserBundle\Entity\Preferencia $preferencias = null) {
        $preferencias->setUser($this);
        $this->preferencias = $preferencias;

        return $this;
    }

    /**
     * Get preferencias
     *
     * @return \Sistema\UserBundle\Entity\Preferencia 
     */
    public function getPreferencias() {
        return $this->preferencias;
    }

    /**
     * Add rutinas
     *
     * @param \Sistema\GymBundle\Entity\Rutina $rutinas
     * @return User
     */
    public function addRutina(\Sistema\GymBundle\Entity\Rutina $rutinas) {
        $rutinas->setUser($this);
        $this->rutinas[] = $rutinas;

        return $this;
    }

    /**
     * Remove rutinas
     *
     * @param \Sistema\GymBundle\Entity\Rutina $rutinas
     */
    public function removeRutina(\Sistema\GymBundle\Entity\Rutina $rutinas) {
        $this->rutinas->removeElement($rutinas);
    }

    /**
     * Get rutinas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRutinas() {
        return $this->rutinas;
    }


    /**
     * Add pagos
     *
     * @param \Sistema\GymBundle\Entity\Pago $pagos
     * @return User
     */
    public function addPago(\Sistema\GymBundle\Entity\Pago $pagos)
    {
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \Sistema\GymBundle\Entity\Pago $pagos
     */
    public function removePago(\Sistema\GymBundle\Entity\Pago $pagos)
    {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos()
    {
        return $this->pagos;
    }
}
