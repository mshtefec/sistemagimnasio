<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preferencia
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\UserBundle\Entity\PreferenciaRepository")
 */
class Preferencia {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mostrarAsistencia", type="boolean")
     */
    private $mostrarAsistencia;

    /**
     * @var string
     *
     * @ORM\Column(name="generacionDeCuota", type="string")
     */
    private $generacionDeCuota;

    /**
     * @var string
     *
     * @ORM\Column(name="intervaloDeVencimiento", type="string")
     */
    private $intervaloDeVencimiento;


    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="preferencias")
     */
    private $user;
    
    
    public function __construct() {
        $this->mostrarAsistencia=true;
        $this->generacionDeCuota=1;
        $this->intervaloDeVencimiento=0;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set mostrarAsistencia
     *
     * @param string $mostrarAsistencia
     * @return Preferencia
     */
    public function setMostrarAsistencia($mostrarAsistencia) {
        $this->mostrarAsistencia = $mostrarAsistencia;

        return $this;
    }

    /**
     * Get mostrarAsistencia
     *
     * @return string 
     */
    public function getMostrarAsistencia() {
        return $this->mostrarAsistencia;
    }


    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Preferencia
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set generacionDeCuota
     *
     * @param string $generacionDeCuota
     * @return Preferencia
     */
    public function setGeneracionDeCuota($generacionDeCuota)
    {
        $this->generacionDeCuota = $generacionDeCuota;

        return $this;
    }

    /**
     * Get generacionDeCuota
     *
     * @return string 
     */
    public function getGeneracionDeCuota()
    {
        return $this->generacionDeCuota;
    }

    /**
     * Set intervaloDeVencimiento
     *
     * @param string $intervaloDeVencimiento
     * @return Preferencia
     */
    public function setIntervaloDeVencimiento($intervaloDeVencimiento)
    {
        $this->intervaloDeVencimiento = $intervaloDeVencimiento;

        return $this;
    }

    /**
     * Get intervaloDeVencimiento
     *
     * @return string 
     */
    public function getIntervaloDeVencimiento()
    {
        return $this->intervaloDeVencimiento;
    }
}
