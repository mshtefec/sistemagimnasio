<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * PreferenciaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class PreferenciaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('mostrarAsistencia', null, array(
                    'label' => 'Mostrar informacion del cliente al poner asistencia',
                    'required' => false
                ))
                ->add('generacionDeCuota', 'choice', array(
                        'choices' => array(
                            1 => 'Generacion de cuotas sin importar el dia de cobro del plan',
                            2 => 'Generacion de cuotas segun el dia de cobro del plan',
                        )
                    )
                )
                ->add('intervaloDeVencimiento', null, array(
                        'label' => 'Ingrese la cantidad de dias que deben transcrurrir para que una cuota este vencida.'
                    )
                )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\Preferencia'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_userbundle_preferencia';
    }

}
