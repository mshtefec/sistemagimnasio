<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

/**
 * UserType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!is_null($builder->getData()->getId())) {//New
                
            $tieneIdEsTrue = false;
        } else {//Edit
            
            $tieneIdEsTrue = true;
        }

        $builder
            ->add('username', null, array(
                'label' => 'Nombre de usuario',
                'attr' => array(
                    'col' => "col-lg-4 col-md-4",
                )
            ))
            ->add('cantidad_gimnasio', null, array(
                'label' => 'Cantidad de gimnasios',
                'attr' => array(
                    'col' => "col-lg-3 col-md-3",
                )
            ))
            ->add('email', null, array(
                'label' => 'Correo',
                'attr' => array(
                    'col' => "col-lg-5 col-md-5",
                )
            ))
            ->add('servicio','entity', array(
                'class' => 'Sistema\GymBundle\Entity\Servicio',
                'required' => true,
                'attr' => array(
                    'col' => "col-lg-6 col-md-6",
                )
            ))
            ->add('fecha_fin_servicio', 'bootstrapdatetime', array(
                'label'       => 'Fecha de fin de servicio',
                'required'    => true,
                'read_only'   => true,
                'widget_type' => 'date',
                'attr' => array(
                    'col' => "col-lg-6 col-md-6",
                )
            ))
            ->add('password', 'repeated', array(
                'type'            => 'password',
                'required'        => $tieneIdEsTrue,
                'invalid_message' => 'Las Contraseñas deben Coincidir.',
                'options'         => array(
                    'attr' => array(
                        'class' => 'password-field',
                        'col' => "col-lg-6 col-md-6",
                    )
                ),
                'first_options'  => array(
                    'label' => 'Contraseña',
                ),
                'second_options' => array(
                    'label' => 'Repetir',
                ),
            ))
            ->add('user_roles', 'select2', array(
                'class' => 'Sistema\UserBundle\Entity\Role',
                'url'   => 'User_autocomplete_user_roles',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12",
                    'col'   => "col-lg-8 col-md-8",
                )
            ))
            ->add('enabled', null, array(
                'label' => 'Activo',
                'attr' => array(
                    'class' => "col-lg-12 col-md-12",
                    'col'   => "col-lg-4 col-md-4",
                )
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_user';
    }
}
