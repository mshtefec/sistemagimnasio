<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * UserType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class UserEmbedType extends AbstractType {

    private $required;
    private $mostrarRoles;

    public function __construct($required, $mostrarRoles = true) {
        $this->required = $required;
        $this->mostrarRoles = $mostrarRoles;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('username', null, array(
                'label' => 'Nombre de usuario',
                'attr' => array(
                    'col' => "col-lg-4 col-md-4",
                )
            ))
            ->add('email', null, array(
                'label' => 'Correo',
                'attr' => array(
                    'col' => "col-lg-4 col-md-4",
                )
            ))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'required' => $this->required,
                'invalid_message' => 'Las Contraseñas deben Coincidir.',
                'options' => array(
                    'attr' => array(
                        'class' => 'password-field',
                        'col' => "col-lg-4 col-md-4",
                    )
                ),
                'first_options' => array(
                    'label' => 'Contraseña',
                ),
                'second_options' => array(
                    'label' => 'Repetir Contraseña',
                ),
            ))
            ->add('enabled', null, array(
                'label' => 'Activo',
                'required' => false
            ))
        ;

        if ($this->mostrarRoles) {
            $builder
                ->add('user_roles', null, array(
                    'label' => 'Rol',
                    'required' => true,
                    'class' => 'SistemaUserBundle:Role',
                    'property' => 'roleName',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('a')
                            ->select('a')
                            ->where('a.id <> 1')
                            ->orderBy('a.role_name', 'ASC')
                        ;
                    }
                ))
            ;
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User',
            'constraints' => array(
                new UniqueEntity(array(
                    'fields' => 'username',
                )),
                new UniqueEntity(array(
                    'fields' => 'email',
                )),
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_userbundle_user';
    }

}
