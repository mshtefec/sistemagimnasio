<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {

    /**
     * @Route("/admin/cuenta-usuario", name="cuenta-usuario")
     * @Template()
     */
    public function cuentaUsuarioAction() {

        $user = $this->get('security.context')->getToken()->getUser();
        $form = $this->container->get('fos_user.profile.form');

        $formHandler = $this->container->get('fos_user.profile.form.handler');

        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('fos_user_success', 'profile.flash.updated');

            return new RedirectResponse($this->getRedirectionUrl($user));
        }

        return array(
        	'user' => $user, 
        	'form' => $form->createView(),
        );
    }  
}