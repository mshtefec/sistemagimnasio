<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\User;
use Sistema\UserBundle\Form\UserType;
use Sistema\UserBundle\Form\UserFilterType;
use Sistema\MWSCONFBundle\Entity\Configuracion;
use Sistema\UserBundle\Entity\Preferencia;

/**
 * User controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/superadmin/user")
 */
class UserController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/User.yml',
    );

    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new UserFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="admin_user_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:User:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new UserType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //seteo configuracion default
            $theme = array(
                "header" => "default.css",
                "sidebar" => "default.css"
            );
            $configuracion = new Configuracion();
            $configuracion->setConfiguraciones($theme);
            $configuracion->setTitulo("");
            $configuracion->setHomepage("");
            $configuracion->setEmpresa("");
            $entity->setConfiguracion($configuracion);
            $preferencia = new Preferencia();
            $entity->setPreferencias($preferencia);
            $this->setSecurePassword($entity);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="admin_user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new UserType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="admin_user_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new UserType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="admin_user_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:User:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new UserType();

        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }

        $this->useACL($entity, 'update');
        $current_pass = $entity->getPassword(); //pass anterior
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        // $cantidad_gimnasios = count($entity->getGimnasios());
        // $cantidad_gimnasios_desabilitados = $cantidad_gimnasios - $entity->getCantidadGimnasio();
        // //ladybug_dump_die($cantidad_gimnasios_desabilitados);
        // while ($cantidad_gimnasios_desabilitados !== 0) {
        //     $entity->getGimnasios()[($entity->getCantidadGimnasio() + $cantidad_gimnasios_desabilitados) - 1]->setActivo(0);
        //     $cantidad_gimnasios_desabilitados --;
        // }

        if ($editForm->isValid()) {
            //si pass es nulo guardo al anterior
            if (is_null($entity->getPassword())) {
                $entity->setPassword($current_pass);
            } elseif ($current_pass != $entity->getPassword()) {//si el password cambio lo codifico
                $this->setSecurePassword($entity);
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="admin_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/autocomplete-forms/get-user_roles", name="User_autocomplete_user_roles")
     */
    public function getAutocompleteRole() {
        $options = array(
            'repository' => "SistemaUserBundle:Role",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    private function setSecurePassword($entity) {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_user_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
