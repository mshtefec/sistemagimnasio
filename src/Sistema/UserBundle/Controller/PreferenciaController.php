<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\Preferencia;
use Sistema\UserBundle\Form\PreferenciaType;
use Sistema\UserBundle\Form\PreferenciaFilterType;

/**
 * Preferencia controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/preferencia")
 */
class PreferenciaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/Preferencia.yml',
    );

    /**
     * Lists all Preferencia entities.
     *
     * @Route("/", name="admin_preferencia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new PreferenciaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Preferencia entity.
     *
     * @Route("/", name="admin_preferencia_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Preferencia:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new PreferenciaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Preferencia entity.
     *
     * @Route("/new", name="admin_preferencia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new PreferenciaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Preferencia entity.
     *
     * @Route("/{id}", name="admin_preferencia_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Preferencia entity.
     *
     * @Route("/{id}/edit", name="admin_preferencia_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new PreferenciaType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Preferencia entity.
     *
     * @Route("/edit/preferencias-by-user", name="admin_preferencia_edit_byuser")
     * @Method("GET")
     */
    public function editByUserAction() {
        $this->config['editType'] = new PreferenciaType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $entity = $em->getRepository($config['repository'])->findPreferenciaByUser($user->getId());

        if (!$entity) {
            $entity = new Preferencia();
            $user->setPreferencias($entity);
            $em->persist($entity);
            $em->flush();
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);


        // remove the form to return to the view
        unset($config['editType']);
        return $this->render(
                        'SistemaUserBundle:Preferencia:edit.html.twig', array(
                    'config' => $config,
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                        )
        );
    }

    /**
     * Edits an existing Preferencia entity.
     *
     * @Route("/{id}", name="admin_preferencia_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:Preferencia:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new PreferenciaType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');


            $nextAction = $this->generateUrl('admin_cliente');

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a Preferencia entity.
     *
     * @Route("/{id}", name="admin_preferencia_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Preferencia.
     *
     * @Route("/exporter/{format}", name="admin_preferencia_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Preferencia entity.
     *
     * @Route("/autocomplete-forms/get-user", name="Preferencia_autocomplete_user")
     */
    public function getAutocompleteUser() {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

}
