<?php

namespace Sistema\UserBundle\EventListener;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

class LoginListener implements AuthenticationSuccessHandlerInterface {

    /**
     * @var string
     */
    protected $idGimnasio;
    protected $cuotas;
    protected $diasRestantes;
    protected $preferencias;
    protected $fechaFinServicio;

    /** @var \Symfony\Component\Security\Core\SecurityContext */
    private $securityContext;

    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /** @var \Symfony\Component\Routing\Router */
    private $router;

    /**
     * @param SecurityContext $securityContext
     * @param EntityManager $em
     * @param Router $router
     */
    public function __construct(SecurityContext $securityContext, Doctrine $doctrine, Router $router) {
        $this->securityContext = $securityContext;
        $this->em = $doctrine->getEntityManager();
        $this->router = $router;
    }

    /**
     * Do the magic.
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event) {
        $token = $event->getAuthenticationToken();
        $request = $event->getRequest();

        if ($this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $event->getAuthenticationToken()->getUser();

            if ($this->securityContext->isGranted('ROLE_SUPER_ADMIN')) {
                $this->idGimnasio = null;
                $this->cuotas = 0;
                $this->diasRestantes = null;
                $this->fechaFinServicio = null;
            } else {
                // do some other magic here
                if ($this->securityContext->isGranted('ROLE_ADMIN')) {
                    $gimnasios = $user->getGimnasios()->getValues();
                    $fechaFinServicio = $user->getFechaFinServicio();
                } elseif ($this->securityContext->isGranted('ROLE_EMPLEADO')) {
                    $gimnasios = false;
                    $gimnasio = $user->getPersonal()->getGimnasio();
                    //Obtengo el usuario admin del gimnasio para sacar la fecha fin de servicio
                    $fechaFinServicio = $gimnasio->getUser()->getFechaFinServicio();
                }
                //Control fecha fin de servicio
                $fecha_actual = new \datetime('Today');
                $diasRestantes = date_diff($fecha_actual, $fechaFinServicio)->format('%R%a');

                if ($diasRestantes <= 15) {
                    $this->diasRestantes = $diasRestantes;
                }
                //Set Gym y Cuotas. Entra si es ROLE_ADMIN
                if (!empty($gimnasios)) {
                    $this->idGimnasio = $gimnasios[0]->getId();
                    $countCuotas = $this->em->getRepository('SistemaGymBundle:Cuota')
                            ->findCuotasXGym($this->idGimnasio, null, "impaga", '=');
                    $this->cuotas = count($countCuotas);
                } elseif (!empty($gimnasio)) {//Entra si es ROLE_EMPLEADO
                    $this->idGimnasio = $gimnasio->getId();
                    $countCuotas = $this->em->getRepository('SistemaGymBundle:Cuota')
                            ->findCuotasXGym($this->idGimnasio, null, "impaga", '=');
                    $this->cuotas = count($countCuotas);
                } else {
                    $this->idGimnasio = null;
                    $this->cuotas = 0;
                    $this->diasRestantes = null;
                }
                $preferencia = $this->em->getRepository('SistemaUserBundle:Preferencia')
                        ->findPreferenciaByUser($user->getId());

                if (!$preferencia) {
                    $preferencia = new \Sistema\UserBundle\Entity\Preferencia();
                    $user->setPreferencias($preferencia);
                    $this->em->persist($user);
                    $this->em->flush();
                }
            }
        } else {
            $this->idGimnasio = null;
            $this->cuotas = 0;
            $this->diasRestantes = null;
        }

        // if ($this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        //     // user has logged in using remember_me cookie
        // }

        $this->onAuthenticationSuccess($request, $token);
    }

    //Retorno segun el role la ruta del admin
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
        if ($this->securityContext->isGranted('ROLE_ADMIN')) {
            $referer = 'admin_gimnasio';
        } else if ($this->securityContext->isGranted('ROLE_EMPLEADO')) {
            $referer = 'admin_cliente';
        } else {
            $referer = 'homepage';
        }

        return new RedirectResponse($this->router->generate($referer));
    }

    //Seteo los valores en la session
    public function onKernelResponse(FilterResponseEvent $event) {
        $request = $event->getRequest();

        if (!is_null($this->idGimnasio)) {
            $request->getSession()->set('_idGimnasio', $this->idGimnasio);
            $request->getSession()->set('countCuotas', $this->cuotas);
            $request->getSession()->set('diasRestantes', $this->diasRestantes);
            $request->getSession()->set('preferencias', $this->preferencias);
        }
    }

}
