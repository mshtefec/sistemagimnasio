<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Regex;
use libphonenumber\PhoneNumberFormat;

class TelefonoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
          ->add('tipo', 'choice', array(
            'label' => false,
            'choices' => $this->getTipo()
          ))
          ->add('numero', 'tel', array(
            'label' => false,
            'default_region' => 'AR',
            'format'         => PhoneNumberFormat::NATIONAL
          ))
          // ->add('persona')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Telefono'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_rrhhbundle_telefono';
    }

    private function getTipo() {
        return array(
            'Personal' => 'Personal',
            'Laboral'  => 'Laboral',
        );
    }

}
