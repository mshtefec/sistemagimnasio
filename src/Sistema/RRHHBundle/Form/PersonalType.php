<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\RRHHBundle\Form\TelefonoType;
use Sistema\RRHHBundle\Form\DomicilioType;
use Sistema\UserBundle\Form\UserEmbedType;
use Doctrine\ORM\EntityRepository;

/**
 * PersonalType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class PersonalType extends AbstractType {

    private $security;
    private $user;

    public function __construct($security) {
        $this->security = $security;
        $this->user = $this->security->getToken()->getUser();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {//New
            $required = true;
        } else {//Edit
            $required = false;
        }
        $builder
            ->add('gimnasio', null, array(
                'class' => 'SistemaGymBundle:Gimnasio',
                'query_builder' => function(EntityRepository $er) {
                    if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
                        return $er->createQueryBuilder('a')
                            ->select('a')
                            ->orderBy('a.id', 'DESC')
                        ;
                    } else {
                        return $er->createQueryBuilder('a')
                            ->select('a')
                            ->where('a.user = :user_id')
                            ->setParameter('user_id', $this->user->getId())
                            ->orderBy('a.id', 'DESC')
                        ;
                    }
                },
                'attr' => array(
                    'col' => 'col-lg-4 col-md-4',
                )
            ))
            ->add('apellido', null, array(
                'attr' => array(
                    'col' => 'col-lg-4 col-md-4',
                )
            ))
            ->add('nombre', null, array(
                'attr' => array(
                    'col' => 'col-lg-4 col-md-4',
                )
            ))
            ->add('dni', null, array(
                'attr' => array(
                    'col' => 'col-lg-4 col-md-4',
                )
            ))
            ->add('fechanacimiento', 'bootstrapdatetime', array(
                'required' => false,
                'label' => 'Fecha de Nacimiento',
                'attr' => array(
                    'class' => 'col-lg-4 col-md-4',
                    'col' => 'col-lg-4 col-md-4',
                ),
                'widget_type' => 'date',
            ))
            ->add('user', new UserEmbedType($required, false), array(
                'label' => false,
            ))
            ->add('telefonos', 'collection', array(
                'label' => false,
                'type' => new TelefonoType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('domicilios', 'collection', array(
                'label' => false,
                'type' => new DomicilioType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Personal'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_rrhhbundle_personal';
    }

}
