<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityRepository;

/**
 * PersonalFilterType filtro.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class PersonalFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    private $security;
    private $user;

    public function __construct($security) {
        $this->security = $security;
        $this->user = $this->security->getToken()->getUser();
    }    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('apellido', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('nombre', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            ->add('dni', 'filter_text_like', array(
                'attr'=> array('class'=>'form-control')
            ))
            // ->add('activo', 'filter_choice', array(
            //     'attr'=> array('class'=>'form-control')
            // ))
            ->add('gimnasio', null, array(
                'class' => 'SistemaGymBundle:Gimnasio',
                'query_builder' => function(EntityRepository $er) {
                    if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
                        return $er->createQueryBuilder('a')
                            ->select('a')
                            ->orderBy('a.id', 'DESC')
                        ;
                    } else {
                        return $er->createQueryBuilder('a')
                            ->select('a')
                            ->where('a.user = :user_id')
                            ->setParameter('user_id', $this->user->getId())
                            ->orderBy('a.id', 'DESC')
                        ;
                    }
                },
            ))
        ;

        $listener = function(FormEvent $event)
        {
            // Is data empty?
            foreach ((array)$event->getForm()->getData() as $data) {
                if ( is_array($data)) {
                    foreach ($data as $subData) {
                        if (!empty($subData)) {
                            return;
                        }
                    }
                } else {
                    if (!empty($data)) {
                        return;
                    }    
                }
            }
            $event->getForm()->addError(new FormError('Filter empty'));
        };
        $builder->addEventListener(FormEvents::POST_SUBMIT, $listener);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Personal',
            'validation_groups' => array('no_validation')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_rrhhbundle_personalfiltertype';
    }
}
