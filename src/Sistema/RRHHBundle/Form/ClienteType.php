<?php

namespace Sistema\RRHHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sistema\RRHHBundle\Form\TelefonoType;
use Sistema\RRHHBundle\Form\DomicilioType;
use Sistema\GymBundle\Form\ActividadCobroType;
use Doctrine\ORM\EntityRepository;

/**
 * ClienteType form.
 * @author TECSPRO <contacto@tecspro.com.ar>
 */
class ClienteType extends AbstractType {
    private $is_super_admin;
    private $gym_actual;

    public function __construct($is_super_admin, $gym_actual) {

        $this->is_super_admin = $is_super_admin;
        $this->gym_actual     = $gym_actual;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('fotoPerfil', 'hidden', array(
                'mapped' => false
            ))
            ->add('gimnasio', null, array(
                'class' => 'SistemaGymBundle:Gimnasio',
                'query_builder' => function(EntityRepository $er) {
                    if ($this->is_super_admin) {

                        return $er->createQueryBuilder('a')
                                  ->select('a')
                                  ->orderBy('a.id', 'DESC')
                        ;
                    } else {

                        return $er->createQueryBuilder('g')
                                  ->where('g.id = :id_gym')
                                  ->setParameter('id_gym', $this->gym_actual)
                        ;
                    }
                },
                'attr' => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12',
                ),
            ))
            ->add('apellido', null, array(
                'attr' => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12',
                ),
            ))
            ->add('nombre', null, array(
                'attr' => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12',
                ),
            ))
            ->add('dni', null, array(
                'attr' => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12',
                ),
            ))
            ->add('email', null, array(
                'required' => false,
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                ),
            ))
            ->add('fechanacimiento', 'bootstrapdatetime', array(
                'required' => false,
                'label'    => 'Fecha de Nacimiento',
                'attr'     => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12',
                ),
                'widget_type' => 'date',
            ))
            ->add('fechaInscripcion', 'bootstrapdatetime', array(
                'required' => true,
                'data' => new \DateTime('Today'),
                'label' => 'Fecha de Inscripcion',
                'read_only' => true,
                'attr' => array(
                    'col' => "col-lg-6 col-md-6",
                ),
                'widget_type' => 'date',
            ))
            ->add('activo', null, array(
                'attr' => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12',
                ),
            ))
            ->add('observaciones', null, array(
                'attr' => array(
                    'col' => 'col-lg-3 col-md-6 col-sm-12 autosize',
                ),
            ))
            ->add('telefonos', 'collection', array(
                'attr' => array(
                    'col' => 'col-lg-4 col-md-6 col-sm-12',
                ),
                'label' => false,
                'type' => new TelefonoType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('domicilios', 'collection', array(
                'attr' => array(
                    'col' => 'col-lg-4 col-md-6 col-sm-12',
                ),
                'label' => false,
                'type' => new DomicilioType(),
                'allow_add' => true,
                'allow_delete' => true,
                'required' => true,
                'by_reference' => false,
            ))
            ->add('actividadCobros', 'collection', array(
                    'label' => 'Inscripcion a plan de actividades',
                    'type'  => new ActividadCobroType($this->is_super_admin, $this->gym_actual),
                    'allow_add' => true,
                    'required'  => true,
                )
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\RRHHBundle\Entity\Cliente',
            'cascade_validation' => true
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'sistema_rrhhbundle_cliente';
    }

}
