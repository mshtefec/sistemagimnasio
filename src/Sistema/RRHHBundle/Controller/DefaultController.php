<?php

namespace Sistema\RRHHBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * Ajax response Domicilio
     *
     * @Route("/ajax/domicilio/{id}", name="ajax_domicilio", options={"expose"=true})
     *
     * @return $ajaxResponse
     */
    public function domiciliosByClienteAction()
    {
        $request = $this->getRequest();
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder()
            ->select('d')
            ->from('SistemaRRHHBundle:Domicilio', 'd')
            ->where('d.persona = :clienteId')
            ->setParameter('clienteId', $id)
        ;
        $domicilios = $qb->getQuery()
            ->getResult()
        ;
        $array = array();
        foreach ($domicilios as $entity) {
            $array[] = array(
                'id'   => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }
        $ajaxResponse = new JsonResponse();
        $ajaxResponse->setStatusCode(200);
        $ajaxResponse->setData($array);

        return $ajaxResponse;
    }
}