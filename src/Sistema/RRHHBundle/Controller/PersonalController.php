<?php

namespace Sistema\RRHHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\RRHHBundle\Entity\Personal;
use Sistema\RRHHBundle\Form\PersonalType;
use Sistema\RRHHBundle\Form\PersonalFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\UserBundle\Entity\User;

/**
 * Personal controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/personal")
 */
class PersonalController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/RRHHBundle/Resources/config/Personal.yml',
    );

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        //creo query
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
                ->createQueryBuilder('a')
                ->select('a', 'g', 'up', 'u', 'r')
                ->join('a.gimnasio', 'g')
                ->join('a.user', 'up')
                ->join('g.user', 'u')
                ->join('u.user_roles', 'r')
        ;
        //si no es ROLE_SUPER_ADMIN entro y filtro segun usuario
        $securityContext = $this->container->get('security.context');
        if (false === $securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            //obtengo id de usuario
            $userId = $this->getUser()->getId();
            $queryBuilder
                    ->where('u.id = :userId')
                    ->setParameter('userId', $userId)
            ;
        }
        //ordeno consulta DESC
        $queryBuilder->orderBy('a.id', 'DESC');

        return $queryBuilder;
    }

    /**
     * Lists all Personal entities.
     *
     * @Route("/", name="admin_personal")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $security = $this->get('security.context');
        $this->config['filterType'] = new PersonalFilterType($security);
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Personal entity.
     *
     * @Route("/", name="admin_personal_create")
     * @Method("POST")
     * @Template("SistemaRRHHBundle:Personal:new.html.twig")
     */
    public function createAction() {
        $security = $this->get('security.context');
        $this->config['newType'] = new PersonalType($security);

        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $user = new User();
        $entity->setUser($user);
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $entity->getUser();
            $user->setEnabled(true);
            $roleEmpleado = $em->getRepository('SistemaUserBundle:Role')->findOneByName('ROLE_EMPLEADO');
            $user->addRole($roleEmpleado);
            $entity->setUser($user);
            $this->setSecurePassword($entity->getUser());

            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {
        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
            'attr' => array('class' => 'form-horizontal')
        ));

        $form
            ->add('save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary')
            ))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Personal entity.
     *
     * @Route("/new", name="admin_personal_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $security = $this->get('security.context');
        $this->config['newType'] = new PersonalType($security);
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();
        $entity = new $config['entity']();
        $user = new User();
        $user->setEnabled(true);
        $entity->setUser($user);
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Personal entity.
     *
     * @Route("/{id}", name="admin_personal_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Personal entity.
     *
     * @Route("/{id}/edit", name="admin_personal_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $security = $this->get('security.context');
        $this->config['editType'] = new PersonalType($security);
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Creates a form to edit a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($config, $entity) {
        $form = $this->createForm($config['editType'], $entity, array(
            'action' => $this->generateUrl($config['update'], array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array('class' => 'form-horizontal')
        ));

        $form
            ->add(
                    'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success col-lg-2')
            ))
            ->add('saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.new.saveAndAdd',
                'attr' => array('class' => 'btn btn-primary')
            ))
        ;

        return $form;
    }

    /**
     * Edits an existing Personal entity.
     *
     * @Route("/{id}", name="admin_personal_update")
     * @Method("PUT")
     * @Template("SistemaRRHHBundle:Personal:edit.html.twig")
     */
    public function updateAction($id) {
        $security = $this->get('security.context');
        $this->config['editType'] = new PersonalType($security);

        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $current_pass = $entity->getUser()->getPassword(); //pass anterior
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //utilizo solo el checkbox enabled de user para personal
            $entity->setActivo($entity->getUser()->isEnabled());
            //si pass es nulo guardo al anterior
            if (is_null($entity->getUser()->getPassword())) {
                $entity->getUser()->setPassword($current_pass);
            } elseif ($current_pass != $entity->getUser()->getPassword()) {//si el password cambio lo codifico
                $this->setSecurePassword($entity->getUser());
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Personal entity.
     *
     * @Route("/{id}", name="admin_personal_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Personal entity.
     *
     * @Route("/autocomplete-forms/get-user", name="Personal_autocomplete_user")
     */
    public function getAutocompleteUser() {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Personal entity.
     *
     * @Route("/autocomplete-forms/get-gimnasio", name="Personal_autocomplete_gimnasio")
     */
    public function getAutocompleteGimnasio() {
        $options = array(
            'repository' => "SistemaGymBundle:Gimnasio",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Personal entity.
     *
     * @Route("/autocomplete-forms/get-telefonos", name="Personal_autocomplete_telefonos")
     */
    public function getAutocompleteTelefono() {
        $options = array(
            'repository' => "SistemaRRHHBundle:Telefono",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Personal entity.
     *
     * @Route("/autocomplete-forms/get-domicilios", name="Personal_autocomplete_domicilios")
     */
    public function getAutocompleteDomicilio() {
        $options = array(
            'repository' => "SistemaRRHHBundle:Domicilio",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    private function setSecurePassword($entity) {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_personal_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
