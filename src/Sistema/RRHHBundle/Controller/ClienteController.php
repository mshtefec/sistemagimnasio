<?php

namespace Sistema\RRHHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\RRHHBundle\Entity\Cliente;
use Sistema\GymBundle\Entity\Asistencia;
use Sistema\RRHHBundle\Form\ClienteType;
use Sistema\RRHHBundle\Form\ClienteFilterType;
use Sistema\GymBundle\Entity\ActividadCobro;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Cliente controller.
 * @author TECSPRO <contacto@tecspro.com.ar>
 *
 * @Route("/admin/cliente")
 */
class ClienteController extends Controller {

    //por defecto busca clientes activos en el createQuery
    private $clientesActivos = true;

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/RRHHBundle/Resources/config/Cliente.yml',
    );

    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository) {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
            ->createQueryBuilder('a')
            ->select('a', 'g', 'u')
            ->join('a.gimnasio', 'g')
            ->join('g.user', 'u')
            ->where('a.activo = :clienteActivo')
            ->setParameter('clienteActivo', $this->clientesActivos)
        ;
        //si no es ROLE_SUPER_ADMIN entro y filtro segun usuario
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_SUPER_ADMIN')) {
            $session = $this->getRequest()->getSession();
            $idGymSession = $session->get('_idGimnasio');
            if ($idGymSession > 0) {
                $queryBuilder
                    ->andWhere('g.id = :idGym')
                    ->setParameter('idGym', $idGymSession)
                ;
            }
        } else {
            if ($securityContext->isGranted('ROLE_ADMIN')) {
                $session = $this->getRequest()->getSession();
                $idGymSession = $session->get('_idGimnasio');
            } else if ($securityContext->isGranted('ROLE_EMPLEADO')) {
                $user = $this->get('security.context')->getToken()->getUser();
                $idGymSession = $user->getPersonal()->getGimnasio()->getId();
            }
            if (!is_null($idGymSession)) {
                $queryBuilder
                    ->andWhere('g.id = :idGym')
                    ->setParameter('idGym', $idGymSession)
                ;
            }
        }
        $queryBuilder->orderby('a.apellido', 'ASC');

        return $queryBuilder;
    }

    /**
     * Lists all Cliente entities.
     *
     * @Route("/", name="admin_cliente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ClienteFilterType();
        $config = $this->getConfig();

        list($filterForm, $queryBuilder) = $this->filter($config);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );
        //remove the form to return to the view
        unset($config['filterType']);

        return array(
            'config' => $config,
            'entities' => $pagination,
            'estado' => 'Activo',
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Lists all Cliente entities.
     *
     * @Route("/inactivos", name="admin_cliente_inactivo")
     * @Method("GET")
     * @Template("SistemaRRHHBundle:Cliente:index.html.twig")
     */
    public function indexInactivosAction() {
        $this->config['filterType'] = new ClienteFilterType();
        $config = $this->getConfig();

        $this->clientesActivos = false;

        list($filterForm, $queryBuilder) = $this->filter($config);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $queryBuilder, $this->get('request')->query->get('page', 1), ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range') : 10
        );
        //remove the form to return to the view
        unset($config['filterType']);

        return array(
            'config' => $config,
            'entities' => $pagination,
            'estado' => 'Inactivo',
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Creates a new Cliente entity.
     *
     * @Route("/", name="admin_cliente_create")
     * @Method("POST")
     * @Template("SistemaRRHHBundle:Cliente:new.html.twig")
     */
    public function createAction() {
        $security = $this->get('security.context');
        $config = $this->getConfig();
        $request = $this->getRequest();
        $session = $request->getSession();

        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $config['newType'] = new ClienteType($security->isGranted('ROLE_SUPER_ADMIN'), $idGymSession);

        $entity = new $config['entity']();
        $actividadCobro = new ActividadCobro();

        $entity->addActividadCobro($actividadCobro);

        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if($idGymSession == 0){

            $this->get('session')->getFlashBag()->add('danger', 'Ud. no ha creado ningun gimnasio para registrar clientes.');

            unset($config['newType']);

            return array(
                'config' => $config,
                'entity' => $entity,
                'form' => $form->createView(),
            );
        }

        if ($form->isValid()) {
            // Settear actividadCobro.
            $actividadCobro->setGimnasio($entity->getGimnasio());
            $actividadCobro->setFecha($entity->getFechaInscripcion());
            $actividadCobro->setCliente($entity);
            // Generar asistencia nueva. GymBundle/Services/Asistencia
            $asistencia = $this->get('gym_asistencia')->crearAsistencia(
                $entity->getFechaInscripcion()->format('d/m/Y'), 'd/m/Y', $entity
            );
            // Genero cuota segun asistencia GymBundle/Services/Cuota
            $cuota = $this->get('gym_cuota')
                ->generarCuotaMes(
                    $asistencia->getActividadCobro(), 
                    $asistencia->getMes(), 
                    $asistencia->getAnio()
                );

            $entity->addAsistencia($asistencia);
            // Fin Generar asistencia nueva.
            $em = $this->getDoctrine()->getManager();
            $img = $form->get('fotoPerfil')->getData();
            if (!empty($img)) {
                $pos = strpos($img, ',');
                $img = substr($img, $pos + 1);
                $data = base64_decode($img);
                $entity->setFile($data);
            }

            $em->persist($entity);
            $em->flush();
            //Seteo ACL
            $this->useACL($entity, 'create');
            $this->useACL($asistencia, 'create');
            $this->useACL($actividadCobro, 'create');
            $this->useACL($cuota, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createCreateForm($config, $entity) {
        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
            'attr' => array('class' => 'form-horizontal')
        ));

        $form
                ->add('save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                        )
                )
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                        )
                )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Cliente entity.
     *
     * @Route("/new", name="admin_cliente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {

        $security = $this->get('security.context');

        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $this->config['newType'] = new ClienteType($security->isGranted('ROLE_SUPER_ADMIN'), $idGymSession);
        $config = $this->getConfig();

        $entity = new $config['entity']();
        $actividadCobro = new ActividadCobro();

        $entity->addActividadCobro($actividadCobro);

        $form = $this->createCreateForm($config, $entity);
        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Cliente entity.
     *
     * @Route("/{id}", name="admin_cliente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Cliente entity.
     *
     * @Route("/{id}/edit", name="admin_cliente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $security = $this->get('security.context');

        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $this->config['editType'] = new ClienteType($security->isGranted('ROLE_SUPER_ADMIN'), $idGymSession);
        $config = $this->getConfig();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a Post entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm($config, $id) {
        $mensaje = $this->get('translator')->trans('views.recordactions.disableConfirm', array(), 'MWSimpleAdminCrudBundle');
        $onclick = 'return confirm("' . $mensaje . '");';
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($config['delete'], array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array(
                            'translation_domain' => 'MWSimpleAdminCrudBundle',
                            'label' => 'views.recordactions.disable',
                            'attr' => array(
                                'class' => 'btn btn-danger col-lg-11',
                                'onclick' => $onclick,
                            )
                        ))
                        ->getForm()
        ;
    }

    /**
     * Creates a form to edit a entity.
     * @param array $config
     * @param $entity The entity
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createEditForm($config, $entity) {
        $form = $this->createForm($config['editType'], $entity, array(
            'action' => $this->generateUrl($config['update'], array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr' => array('class' => 'form-horizontal')
        ));

        $form
                ->add(
                        'save', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.save',
                    'attr' => array('class' => 'btn btn-success col-lg-2')
                ))
                ->add('saveAndAdd', 'submit', array(
                    'translation_domain' => 'MWSimpleAdminCrudBundle',
                    'label' => 'views.new.saveAndAdd',
                    'attr' => array('class' => 'btn btn-primary col-lg-2 col-lg-offset-1')
                ))
        ;

        return $form;
    }

    /**
     * Edits an existing Cliente entity.
     *
     * @Route("/{id}", name="admin_cliente_update")
     * @Method("PUT")
     * @Template("SistemaRRHHBundle:Cliente:edit.html.twig")
     */
    public function updateAction($id) {

        $security = $this->get('security.context');

        $session = $this->getRequest()->getSession();
        $idGymSession = $session->get('_idGimnasio');

        $this->config['editType'] = new ClienteType($security->isGranted('ROLE_SUPER_ADMIN'), $idGymSession);
        $config = $this->getConfig();
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $img = $editForm->get('fotoPerfil')->getData();
            if (!empty($img)) {
                $pos = strpos($img, ',');
                $img = substr($img, $pos + 1);
                $data = base64_decode($img);
                $entity->setFile($data);
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');
            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $response;
    }

    /**
     * Deletes a Cliente entity.
     *
     * @Route("/{id}", name="admin_cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $form = $this->createDeleteForm($config, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($config['repository'])->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }

            $entity->setActivo(false);
            //$em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Cliente deshabilitado');
        }

        return $this->redirect($this->generateUrl($config['index']));
    }

    /**
     * Autocomplete a Cliente entity.
     *
     * @Route("/autocomplete-forms/get-cuentaCorriente", name="Cliente_autocomplete_cuentaCorriente")
     */
    public function getAutocompleteCuentaCorriente() {
        $options = array(
            'repository' => "SistemaFACTURACIONBundle:CuentaCorriente"
            ,
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Cliente entity.
     *
     * @Route("/autocomplete-forms/get-actividadCobros", name="Cliente_autocomplete_actividadCobros")
     */
    public function getAutocompleteActividadCobro() {
        $options = array(
            'repository' =>
            "SistemaGymBundle:ActividadCobro",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Cliente entity.
     *
     * @Route("/autocomplete-forms/get-telefonos", name="Cliente_autocomplete_telefonos")
     */
    public function getAutocompleteTelefono() {
        $options = array(
            'repository' =>
            "SistemaRRHHBundle:Telefono",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Cliente entity.
     *
     * @Route("/autocomplete-forms/get-domicilios", name="Cliente_autocomplete_domicilios")
     */
    public function getAutocompleteDomicilio() {
        $options = array(
            'repository' => "SistemaRRHHBundle:Domicilio",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * @Route("/get-cliente", name="find_cliente_by_id")
     * @Method("POST")
     */
    public function getClientesByIdAction() {

        $request = $this->getRequest();
        $id = $request->request->get('id', null);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaRRHHBundle:Cliente')->find($id);

        $array = array();

        $array = array(
            'id' => $entity->getId(),
            'text' => $entity->__toString(),
            'fotoPerfil' => $this->container->get('templating.helper.assets')->getUrl('uploads/' . $entity->getFilePath()),
            'email' => $entity->getEmail(),
            'dni' => $entity->getDni(),
            'gimnasio' => $entity->getGimnasio()->getNombre(),
            'fecha' => $entity->getFechanacimiento()->format("d/m/Y"),
            'fecha_inscripcion' => $entity->getFechaInscripcion()->format("d/m/Y"),
            'activo' => $entity->getActivo(),
            'observacion' => $entity->getObservaciones()
        );
        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-clientes-all", name="Cliente_autocomplete_cliente")
     */
    public function getClientesAction() {
        
        $em           = $this->getDoctrine()->getManager();
        $term         = $this->getRequest()->query->get('q', null);        
        $session      = $this->getRequest()->getSession();
        $security     = $this->get('security.context');
        $idGymSession = $session->get('_idGimnasio');
        
        if(is_null($idGymSession) && !$security->isGranted('ROLE_SUPER_ADMIN')){

            $idGymSession = 0;
        }

        // Trae usuarios del gimnasio y que esten activos.
        $entities = $em->getRepository("SistemaRRHHBundle:Cliente")
                       ->likeNombreGim($term, null, true, $idGymSession);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id'         => $entity->getId(),
                'text'       => $entity->__toString(),
                'fotoPerfil' => $this->container->get('templating.helper.assets')
                                                ->getUrl('uploads/' . $entity->getFilePath())
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Exporter.
     *
     * @Route("/exporter/{format}", name="admin_cliente_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

}
