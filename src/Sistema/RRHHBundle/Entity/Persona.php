<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Sistema\MWSCONFBundle\Entity\MWSgedmo;

/**
 * Persona
 *
 * @ORM\Table(name="persona")
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\PersonaRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminador", type="string")
 * @ORM\DiscriminatorMap({
 *  "persona" = "Persona",
 *  "personal" = "Personal",
 *  "cliente" = "Cliente"
 * })
 *
 */
class Persona extends MWSgedmo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=100, nullable=true)
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "El Apellido debe tener al menos {{ limit }} letras",
     *      maxMessage = "El Apellido no debe tener mas de {{ limit }} letras"
     * )
     */
    protected $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = "1",
     *      max = "100",
     *      minMessage = "El Nombre debe tener al menos {{ limit }} 2 letras",
     *      maxMessage = "El Nombre no debe tener mas de {{ limit }} letras"
     * )
     */
    protected $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=20, nullable=true)
     */
    protected $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="cuitcuil", type="string", length=30, nullable=true)
     */
    protected $cuitcuil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechanacimiento", type="date", nullable=true)
     */
    protected $fechanacimiento;

    /**
     * @var boolean $activo
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Type(type="boolean", message="El valor {{ value }} no es un {{ type }} valido.")
     */
    protected $activo;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\RRHHBundle\Entity\Telefono", mappedBy="persona", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $telefonos;

    /**
     *
     * @ORM\OneToMany(targetEntity="Sistema\RRHHBundle\Entity\Domicilio", mappedBy="persona", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $domicilios;

    /**
     * Constructor
     */
    public function __construct() {
        $this->activo = true;
        $this->telefonos  = new ArrayCollection();
        $this->domicilios = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set apellido
     *
     * @param  string  $apellido
     * @return Persona
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido() {
        return $this->apellido;
    }

    /**
     * Set nombre
     *
     * @param  string  $nombre
     * @return Persona
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set dni
     *
     * @param  string  $dni
     * @return Persona
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * Set cuitcuil
     *
     * @param  string  $cuitcuil
     * @return Persona
     */
    public function setCuitcuil($cuitcuil) {
        $this->cuitcuil = $cuitcuil;

        return $this;
    }

    /**
     * Get cuitcuil
     *
     * @return string
     */
    public function getCuitcuil() {
        return $this->cuitcuil;
    }

    /**
     * Set fechanacimiento
     *
     * @param  \DateTime $fechanacimiento
     * @return Persona
     */
    public function setFechanacimiento($fechanacimiento) {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return \DateTime
     */
    public function getFechanacimiento() {
        return $this->fechanacimiento;
    }

    /**
     * Set activo
     *
     * @param  boolean $activo
     * @return Persona
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Add telefonos
     *
     * @param  \Sistema\RRHHBundle\Entity\Telefono $telefonos
     * @return Persona
     */
    public function addTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $telefonos->setPersona($this);
        $this->telefonos[] = $telefonos;

        return $this;
    }

    /**
     * Remove telefonos
     *
     * @param \Sistema\RRHHBundle\Entity\Telefono $telefonos
     */
    public function removeTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $this->telefonos->removeElement($telefonos);
    }

    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelefonos() {
        return $this->telefonos;
    }

    /**
     * Add domicilios
     *
     * @param  \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     * @return Persona
     */
    public function addDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $domicilios->setPersona($this);
        $this->domicilios[] = $domicilios;

        return $this;
    }

    /**
     * Remove domicilios
     *
     * @param \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     */
    public function removeDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $this->domicilios->removeElement($domicilios);
    }

    /**
     * Get domicilios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDomicilios() {
        return $this->domicilios;
    }

}
