<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;
use Sistema\MWSCONFBundle\Controller\Slug;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\ClienteRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Cliente extends Persona {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @var string
     * @ORM\Column(name="fechaInscripcion", type="date")
     */
    protected $fechaInscripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=true)
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es un email Valido."
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=100, nullable=true)
     */
    protected $empresa;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\FACTURACIONBundle\Entity\CuentaCorriente", mappedBy="cliente", cascade={"persist", "remove"})
     */
    protected $cuentaCorriente;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\ActividadCobro", mappedBy="cliente", cascade={"persist"})
     */
    private $actividadCobros;    
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @var string
     */
    private $temp;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Gimnasio", inversedBy="clientes")
     * @ORM\JoinColumn(name="gimnasio_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $gimnasio;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     */
    protected $observaciones;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\RutinaCliente", mappedBy="cliente")
     */
    private $rutinasClientes;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Grupo", inversedBy="clientes")
     * @ORM\JoinColumn(name="grupo_id", referencedColumnName="id")
     * */
    private $grupo;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Asistencia", mappedBy="cliente", cascade={"all"})
     */
    private $asistencias;

    /**
     * @ORM\OneToMany(targetEntity="Sistema\GymBundle\Entity\Pago", mappedBy="cliente")
     */
    private $pagos;

    /**
     * Constructor
     */
    public function __construct() {
        $this->asistencias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actividadCobros = new ArrayCollection();
        $this->rutinasClientes = new ArrayCollection();
        $this->telefonos = new ArrayCollection();
        $this->domicilios = new ArrayCollection();
        $this->pagos = new ArrayCollection();
        $this->setActivo(true);
    }

    public function __toString() {
        return $this->getApellido() . ' ' . $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Cliente
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set empresa
     *
     * @param string $empresa
     * @return Cliente
     */
    public function setEmpresa($empresa) {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string 
     */
    public function getEmpresa() {
        return $this->empresa;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Cliente
     */
    public function setApellido($apellido) {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido() {
        return $this->apellido;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cliente
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Cliente
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni() {
        return $this->dni;
    }

    /**
     * Set cuitcuil
     *
     * @param string $cuitcuil
     * @return Cliente
     */
    public function setCuitcuil($cuitcuil) {
        $this->cuitcuil = $cuitcuil;

        return $this;
    }

    /**
     * Get cuitcuil
     *
     * @return string 
     */
    public function getCuitcuil() {
        return $this->cuitcuil;
    }

    /**
     * Set fechanacimiento
     *
     * @param \DateTime $fechanacimiento
     * @return Cliente
     */
    public function setFechanacimiento($fechanacimiento) {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return \DateTime 
     */
    public function getFechanacimiento() {
        return $this->fechanacimiento;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Cliente
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Set cuentaCorriente
     *
     * @param \Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente
     * @return Cliente
     */
    public function setCuentaCorriente(\Sistema\FACTURACIONBundle\Entity\CuentaCorriente $cuentaCorriente = null) {
        $this->cuentaCorriente = $cuentaCorriente;

        return $this;
    }

    /**
     * Get cuentaCorriente
     *
     * @return \Sistema\FACTURACIONBundle\Entity\CuentaCorriente 
     */
    public function getCuentaCorriente() {
        return $this->cuentaCorriente;
    }

    /**
     * Add actividadCobros
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobros
     * @return Cliente
     */
    public function addActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobros) {
        $actividadCobros->setCliente($this);
        $this->actividadCobros[] = $actividadCobros;

        return $this;
    }

    /**
     * Remove actividadCobros
     *
     * @param \Sistema\GymBundle\Entity\ActividadCobro $actividadCobros
     */
    public function removeActividadCobro(\Sistema\GymBundle\Entity\ActividadCobro $actividadCobros) {
        $this->actividadCobros->removeElement($actividadCobros);
    }

    /**
     * Get actividadCobros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActividadCobros() {
        return $this->actividadCobros;
    }

    /**
     * Add telefonos
     *
     * @param \Sistema\RRHHBundle\Entity\Telefono $telefonos
     * @return Cliente
     */
    public function addTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $telefonos->setPersona($this);
        $this->telefonos[] = $telefonos;

        return $this;
    }

    /**
     * Remove telefonos
     *
     * @param \Sistema\RRHHBundle\Entity\Telefono $telefonos
     */
    public function removeTelefono(\Sistema\RRHHBundle\Entity\Telefono $telefonos) {
        $this->telefonos->removeElement($telefonos);
    }

    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTelefonos() {
        return $this->telefonos;
    }

    /**
     * Add domicilios
     *
     * @param \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     * @return Cliente
     */
    public function addDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $domicilios->setPersona($this);
        $this->domicilios[] = $domicilios;

        return $this;
    }

    /**
     * Remove domicilios
     *
     * @param \Sistema\RRHHBundle\Entity\Domicilio $domicilios
     */
    public function removeDomicilio(\Sistema\RRHHBundle\Entity\Domicilio $domicilios) {
        $this->domicilios->removeElement($domicilios);
    }

    /**
     * Get domicilios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDomicilios() {
        return $this->domicilios;
    }

    /**
     * Set gimnasio
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasio
     * @return Cliente
     */
    public function setGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasio = null) {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get gimnasio
     *
     * @return \Sistema\GymBundle\Entity\Gimnasio 
     */
    public function getGimnasio() {
        return $this->gimnasio;
    }

    /**
     * Set filePath
     *
     * @param  string $filePath
     * @return File
     */
    public function setFilePath($filePath) {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get FilePath
     *
     * @return string
     */
    public function getFilePath() {
        return $this->filePath;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Cliente
     */
    public function setFile($file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        }
        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile() {
        return $this->file;
    }

    public function getAbsolutePath() {
        return is_null($this->filePath) ? null : $this->getUploadRootDir() . '/' . $this->filePath
        ;
    }

    public function getWebPath() {
        return is_null($this->filePath) ? null : $this->getUploadDir() . '/' . $this->filePath
        ;
    }

    protected function getUploadRootDir() {
        if (!$this->getUploadDir()) {
            $uploadDir = 'uploads';
        } else {
            $uploadDir = $this->getUploadDir();
        }
        $path = __DIR__ . '/../../../../web/uploads/' . $this->getUploadDir();

        if (!file_exists($path)) {
            mkdir($path, 0755);
        }

        return $path;
    }

    public function setUploadDir($uploadDir) {
        $this->uploadDir = $uploadDir;
    }

    public function getUploadDir() {
        return $this->uploadDir;
    }

    public function getFixturesPath() {
        return $this->getAbsolutePath() . 'web/filefixture/';
    }

    /**
     * @ORM\PreFlush()
     */
    public function preUpload() {
        if (!is_null($this->getFile())) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $slugApellidoNombre = Slug::slugify($this->getApellido() . $this->getNombre());
            $this->filePath = $filename . $slugApellidoNombre . ".png";
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {

        if (is_null($this->getFile())) {
            return null;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        // $this->getFile()->move($this->getUploadRootDir(), $this->filePath);
        // $file = __DIR__ . '/../../../../web/uploads/snapshot' . rand(1000, 9999) . '.png';
        //mueve el archivo canvas al directorio donde se decea guardar la imagen    
        file_put_contents($this->getUploadRootDir() . $this->filePath, $this->getFile());
        // check if we have an old image
        if (isset($this->temp)) {
            if (file_exists($this->temp)) {
                // delete the old image
                unlink($this->temp);
            }
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove() {
        $this->temp = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if (isset($this->temp)) {
            if (file_exists($this->temp)) {
                unlink($this->temp);
            }
        }
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return Cliente
     */
    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones() {
        return $this->observaciones;
    }

    /**
     * Set grupo
     *
     * @param \Sistema\GymBundle\Entity\Grupo $grupo
     * @return Cliente
     */
    public function setGrupo(\Sistema\GymBundle\Entity\Grupo $grupo = null) {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \Sistema\GymBundle\Entity\Grupo 
     */
    public function getGrupo() {
        return $this->grupo;
    }

    /**
     * Add rutinasClientes
     *
     * @param \Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes
     * @return Cliente
     */
    public function addRutinasCliente(\Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes) {

        $this->rutinasClientes[] = $rutinasClientes;

        return $this;
    }

    /**
     * Remove rutinasClientes
     *
     * @param \Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes
     */
    public function removeRutinasCliente(\Sistema\GymBundle\Entity\RutinaCliente $rutinasClientes) {
        $this->rutinasClientes->removeElement($rutinasClientes);
    }

    /**
     * Get rutinasClientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRutinasClientes() {
        return $this->rutinasClientes;
    }

    /**
     * Add asistencias
     *
     * @param \Sistema\GymBundle\Entity\Asistencia $asistencias
     * @return Cliente
     */
    public function addAsistencia(\Sistema\GymBundle\Entity\Asistencia $asistencias) {
        $asistencias->setCliente($this);
        $this->asistencias[] = $asistencias;

        return $this;
    }

    /**
     * Remove asistencias
     *
     * @param \Sistema\GymBundle\Entity\Asistencia $asistencias
     */
    public function removeAsistencia(\Sistema\GymBundle\Entity\Asistencia $asistencias) {
        $this->asistencias->removeElement($asistencias);
    }

    /**
     * Get asistencias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsistencias() {
        return $this->asistencias;
    }

    /**
     * Add pagos
     *
     * @param \Sistema\GymBundle\Entity\Pago $pagos
     * @return Cliente
     */
    public function addPago(\Sistema\GymBundle\Entity\Pago $pagos) {
        $pagos->setCliente($this);
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \Sistema\GymBundle\Entity\Pago $pagos
     */
    public function removePago(\Sistema\GymBundle\Entity\Pago $pagos) {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos() {
        return $this->pagos;
    }


    /**
     * Set fechaInscripcion
     *
     * @param \Date $fechaInscripcion
     * @return Cliente
     */
    public function setFechaInscripcion($fechaInscripcion)
    {
        $this->fechaInscripcion = $fechaInscripcion;

        return $this;
    }

    /**
     * Get fechaInscripcion
     *
     * @return \Date
     */
    public function getFechaInscripcion()
    {
        return $this->fechaInscripcion;
    }
}
