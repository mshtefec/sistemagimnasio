<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Domicilio
 *
 * @ORM\Table(name="domicilio")
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\DomicilioRepository")
 */
class Domicilio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="localidad", type="string", length=255)
     */
    private $localidad;

    /**
     * @var string
     *
     * @ORM\Column(name="calle", type="string", length=255)
     */
    private $calle;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="departamento", type="string", length=255, nullable=true)
     */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\Column(name="edificio", type="string", length=255, nullable=true)
     */
    private $edificio;

    /**
     * @var string
     *
     * @ORM\Column(name="manzana", type="string", length=100, nullable=true)
     */
    private $manzana;

    /**
     * @var string
     *
     * @ORM\Column(name="entreCalles", type="string", length=255, nullable=true)
     */
    private $entreCalles;

    /**
     * @var string
     *
     * @ORM\Column(name="tira", type="string", length=255, nullable=true)
     */
    private $tira;

    /**
     * @var string
     *
     * @ORM\Column(name="parcela", type="string", length=255, nullable=true)
     */
    private $parcela;

    /**
    *
    * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Persona", inversedBy="domicilios")
    * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
    *
    */
    private $persona;

    public function __toString()
    {
        return $this->getCalle().' '.$this->getNumero().' - '.$this->getLocalidad();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set localidad
     *
     * @param  string    $localidad
     * @return Domicilio
     */
    public function setLocalidad($localidad)
    {
        $this->localidad = $localidad;

        return $this;
    }

    /**
     * Get localidad
     *
     * @return string
     */
    public function getLocalidad()
    {
        return $this->localidad;
    }

    /**
     * Set calle
     *
     * @param  string    $calle
     * @return Domicilio
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set numero
     *
     * @param  string    $numero
     * @return Domicilio
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set departamento
     *
     * @param  string    $departamento
     * @return Domicilio
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return string
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set edificio
     *
     * @param  string    $edificio
     * @return Domicilio
     */
    public function setEdificio($edificio)
    {
        $this->edificio = $edificio;

        return $this;
    }

    /**
     * Get edificio
     *
     * @return string
     */
    public function getEdificio()
    {
        return $this->edificio;
    }

    /**
     * Set manzana
     *
     * @param  string    $manzana
     * @return Domicilio
     */
    public function setManzana($manzana)
    {
        $this->manzana = $manzana;

        return $this;
    }

    /**
     * Get manzana
     *
     * @return string
     */
    public function getManzana()
    {
        return $this->manzana;
    }

    /**
     * Set entreCalles
     *
     * @param  string    $entreCalles
     * @return Domicilio
     */
    public function setEntreCalles($entreCalles)
    {
        $this->entreCalles = $entreCalles;

        return $this;
    }

    /**
     * Get entreCalles
     *
     * @return string
     */
    public function getEntreCalles()
    {
        return $this->entreCalles;
    }

    /**
     * Set tira
     *
     * @param  string    $tira
     * @return Domicilio
     */
    public function setTira($tira)
    {
        $this->tira = $tira;

        return $this;
    }

    /**
     * Get tira
     *
     * @return string
     */
    public function getTira()
    {
        return $this->tira;
    }

    /**
     * Set parcela
     *
     * @param  string    $parcela
     * @return Domicilio
     */
    public function setParcela($parcela)
    {
        $this->parcela = $parcela;

        return $this;
    }

    /**
     * Get parcela
     *
     * @return string
     */
    public function getParcela()
    {
        return $this->parcela;
    }

    /**
     * Set persona
     *
     * @param  \Sistema\RRHHBundle\Entity\Persona $persona
     * @return Domicilio
     */
    public function setPersona(\Sistema\RRHHBundle\Entity\Persona $persona = null)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \Sistema\RRHHBundle\Entity\Persona
     */
    public function getPersona()
    {
        return $this->persona;
    }
}
