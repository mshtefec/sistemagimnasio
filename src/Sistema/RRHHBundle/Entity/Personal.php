<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Personal
 *
 * @ORM\Table(name="personal")
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\PersonalRepository")
 */
class Personal extends Persona
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\UserBundle\Entity\User", cascade={"persist", "remove"}, inversedBy="personal", orphanRemoval=true)
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @Assert\Valid
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\GymBundle\Entity\Gimnasio", inversedBy="clientes")
     * @ORM\JoinColumn(name="gimnasio_id", referencedColumnName="id", nullable=false)
     */
    private $gimnasio;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->telefonos  = new ArrayCollection();
        $this->domicilios = new ArrayCollection();
    }

    public function __toString() {
        return $this->getApellido().' '.$this->getNombre();
    }

    /**
     * Get Enabled de User
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->user->isEnabled();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Sistema\UserBundle\Entity\User $user
     * @return Personal
     */
    public function setUser(\Sistema\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set gimnasio
     *
     * @param \Sistema\GymBundle\Entity\Gimnasio $gimnasio
     * @return Personal
     */
    public function setGimnasio(\Sistema\GymBundle\Entity\Gimnasio $gimnasio)
    {
        $this->gimnasio = $gimnasio;

        return $this;
    }

    /**
     * Get gimnasio
     *
     * @return \Sistema\GymBundle\Entity\Gimnasio 
     */
    public function getGimnasio()
    {
        return $this->gimnasio;
    }

}
