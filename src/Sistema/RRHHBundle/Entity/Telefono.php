<?php

namespace Sistema\RRHHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;

/**
 * Telefono
 *
 * @ORM\Table(name="telefono")
 * @ORM\Entity(repositoryClass="Sistema\RRHHBundle\Entity\TelefonoRepository")
 */
class Telefono {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="tipo", type="string", length=100, nullable=false)
     * @Assert\NotNull()
     */
    private $tipo;

    /**
     * @var string
     * @ORM\Column(name="numero", type="phone_number", length=50, nullable=false)
     * @Assert\NotNull()
     * @AssertPhoneNumber(defaultRegion="AR")
     */
    private $numero;

    /**
     * @ORM\ManyToOne(targetEntity="Sistema\RRHHBundle\Entity\Persona", inversedBy="telefonos")
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     */
    private $persona;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param  string   $tipo
     * @return Telefono
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set numero
     *
     * @param  string   $numero
     * @return Telefono
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set persona
     *
     * @param  \Sistema\RRHHBundle\Entity\Persona $persona
     * @return Telefono
     */
    public function setPersona(\Sistema\RRHHBundle\Entity\Persona $persona = null) {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return \Sistema\RRHHBundle\Entity\Persona
     */
    public function getPersona() {
        return $this->persona;
    }

    function __toString() {
        return $this->getNumero() . ' - ' . $this->getTipo();
    }

}
