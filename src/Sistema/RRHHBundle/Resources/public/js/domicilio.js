// Get the ul that holds the collection
var $collectionDomicilio;
// setup an "add a tag" link
var $addDomicilioLink = $('<hr><a href="#" class="add_domicilio_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newDomicilioLinkLi = $('<div></div>');

jQuery(document).ready(function () {
    // Get the ul that holds the collection of tags
    $collectionDomicilio = $('.domicilio');
    // add the "add a tag" anchor and li to the tags ul

    $collectionDomicilio.append($newDomicilioLinkLi);
    $collectionDomicilio.append($addDomicilioLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionDomicilio.data('index', $collectionDomicilio.find(':input').length);
    $("#nuevoDomicilio").on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionDomicilio, $newDomicilioLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newDomicilioLinkLi).offset().top
        }, 1000);
    });
    $addDomicilioLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionDomicilio, $newDomicilioLinkLi);
    });
    $collectionDomicilio.delegate('.delete_domicilio', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });
});