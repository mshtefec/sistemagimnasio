// Get the ul that holds the collection
var $collectionTelefono;
// setup an "add a tag" link
var $addTelefonoLink = $('<hr><a href="#" class="add_telefonos_link btn btn-primary"><i class="glyphicon glyphicon-plus"></i><i class="glyphicon glyphicon-folder-open "></i></a>');
var $newTelefonoLinkLi = $('<div class="row"></div>');

jQuery(document).ready(function () {
    // Get the ul that holds the collection of tags
    $collectionTelefono = $('.telefono');
    // add the "add a tag" anchor and li to the tags ul

    $collectionTelefono.append($newTelefonoLinkLi);
    $collectionTelefono.append($addTelefonoLink);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionTelefono.data('index', $collectionTelefono.find(':input').length);
    $("#nuevoTelefono").on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionTelefono, $newTelefonoLinkLi);

        $('html, body').stop().animate({
            scrollTop: $($newTelefonoLinkLi).offset().top
        }, 1000);
    });
    $addTelefonoLink.on('click', function (e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionTelefono, $newTelefonoLinkLi);
    });
    $collectionTelefono.delegate('.delete_telefono', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        removeForm(jQuery(this).closest('.rowremove'));
    });
});